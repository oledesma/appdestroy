/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

//base color #7d5fff

import React from 'react';
import {StyleSheet, View} from 'react-native';
import LoginScreen from './src/Views/LoginScreen';
import SplashScreen from './src/Views/SplashScreen';
import RegisterScreen from './src/Views/RegisterScreen';
import RegistrationSuccessScreen from './src/Views/RegistrationSuccessScreen';
import MainScreen from './src/Views/MainScreen';
import Home from './src/Views/MainScreenComponents/Home';
import DetailsCarta from './src/Views/MainScreenComponents/DetailsCarta';
import NewCarta from './src/Views/MainScreenComponents/NewCarta';
import Description from './src/Views/MainScreenComponents/NewCartaStepsOld/Description';
import MediaFiles from './src/Views/MainScreenComponents/NewCartaStepsOld/MediaFiles';
import Ubication from './src/Views/MainScreenComponents/NewCartaStepsOld/Ubication';
import LastStep from './src/Views/MainScreenComponents/NewCartaStepsOld/LastStep';
import CreatedSuccess from './src/Views/MainScreenComponents/NewCartaStepsOld/CreatedSuccess';
import NewCartaContainer from './src/Views/MainScreenComponents/NewCartaContainer';
import CardDestroy from './src/Views/CustomComponents/CardDestroy';
import CardDetails from './src/Views/CustomComponents/CardDetails';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import CodeChecker from './src/Animations/CodeChecker';
import SelectCardType from './src/Views/MainScreenComponents/SelectCardType';
import FormCard from './src/Views/MainScreenComponents/NewCard/FormCard';
import FormCode from './src/Views/MainScreenComponents/NewCard/FormCode';
import FormPhotos from './src/Views/MainScreenComponents/NewCard/FormPhotos';
import FormUbications from './src/Views/MainScreenComponents/NewCard/FormUbications';
import CardCreator from './src/Views/MainScreenComponents/NewCard/CardCreator';
import Map from './src/Views/MainScreenComponents/NewCard/Map';
import OptionSelector from './src/Views/MainScreenComponents/OptionSelector';
import FirstScreen from './src/Views/FirstScreen';

const Stack = createStackNavigator();

const App: () => React$Node = () => {
    return (
        <NavigationContainer>
            <MyStack />
        </NavigationContainer>
    );
};

function MyStack() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name="SplashScreen" component={SplashScreen} />
            <Stack.Screen name="FirstScreen" component={FirstScreen} />
            <Stack.Screen name="LoginScreen" component={LoginScreen} />
            <Stack.Screen name="MainScreen" component={MainScreen} />
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
            <Stack.Screen name="RegistrationSuccessScreen" component={RegistrationSuccessScreen} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="CardDetails" component={CardDetails} />
            <Stack.Screen name="CardDestroy" component={CardDestroy} />
            <Stack.Screen name="CodeChecker" component={CodeChecker} />
            <Stack.Screen name="OptionSelector" component={OptionSelector} />
            <Stack.Screen name="SelectCardType" component={SelectCardType} />
            <Stack.Screen name="FormCard" component={FormCard} />
            <Stack.Screen name="FormCode" component={FormCode} />
            <Stack.Screen name="FormPhotos" component={FormPhotos} />
            <Stack.Screen name="FormUbications" component={FormUbications} />
            <Stack.Screen name="Map" component={Map} />
            <Stack.Screen name="CardCreator" component={CardCreator} />
            <Stack.Screen name="Description" component={Description} />
            <Stack.Screen name="MediaFiles" component={MediaFiles} />
            <Stack.Screen name="Ubication" component={Ubication} />
            <Stack.Screen name="ConfirmCreation" component={LastStep} />
            <Stack.Screen name="CreatedSuccess" component={CreatedSuccess} />
            <Stack.Screen name="NewCartaContainer" component={NewCartaContainer} />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
});

export default App;
