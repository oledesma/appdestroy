import React, {Component} from 'react';
import {StyleSheet, View,Animated, Dimensions, Image} from 'react-native';
import LottieView from 'lottie-react-native';

const screenHeight = Dimensions.get("window").height;

export default class Loading extends Component{
    state={
        top: new Animated.Value(0),
        opacity: new Animated.Value(1)
    }

    componentDidMount() {
        console.log(this.props)
    }

    componentDidUpdate() {

    }

    render() {
        return (
            <Animated.View style={{
                top: this.state.top,
                opacity: this.state.opacity,
                width: "100%",
                height: "100%",
                backgroundColor: "rgba(255,255,255,0.9)",
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                left: 0,
            }}>
                <Image
                    source={require("../AnimationSources/loadingShip.gif")}
                    style={{width: 300, height: 300}}
                />
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        backgroundColor: "rgba(255,255,255,0.9)",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        zIndex: 12
    }
});
