import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    BackHandler
} from 'react-native';
import {Title, Input, Textarea, Button, Subtitle, Spinner} from 'native-base';
import DataSource from '../datasource/DataSource';

const DS = new DataSource();

export default class CodeChecker extends Component {

    state={
        code: "",
        errorCode: false,
        errorMessage: "",
        disabled: false
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=>{
            this.resetState();
            this.props.navigation.goBack();
            return true;
        })
    }

    renderError(){
        if(this.state.errorCode){
            return(
                <Subtitle style={{color: "red"}}>
                    {this.state.errorMessage}
                </Subtitle>
            )
        }
    }

    resetState(){
        this.setState({disabled: false, errorCode: false, errorMessage: ""});
    }

    async checkCode(){
        if(this.state.code !== ""){
            this.setState({disabled: true})
            let check = await DS.checkIfCodeExist(this.state.code);
            if(check){
                if(check.estado === "en uso" || check.estado === "foto requerida"){
                    this.setState({errorCode: true, errorMessage: "Parece que ya se ha utilizado tu código.", disabled: false})
                }else{
                    this.props.navigation.push("CardDestroy", check)
                    this.resetState();
                }
            }else {
                this.setState({errorCode: true, errorMessage: "No hemos encontrado tu carta :(", disabled: false})
            }
        }else {
            this.setState({errorCode: true, errorMessage: "No puede estar vacío el código.", disabled: false})
        }
    }

    renderSpinner(){
        if(this.state.disabled){
            return(
                <Spinner color="red"/>
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView behavior="height" style={{width: "90%", height: "100%", justifyContent: "center"}}>
                    <Title style={{color: "black", height: 60}}>Introduce el código</Title>
                    <Input
                        value={this.state.code}
                        onChangeText={code => this.setState({code})}
                        placeholder="Introduce aquí el código..."
                        disabled={this.state.disabled}
                        style={{maxHeight: 60, borderBottomWidth: 1, borderBottomColor: "grey", borderTopWidth: 1, borderTopColor: "red"}}
                        onEndEditing={()=>this.checkCode()}
                        autoCapitalize="none"
                    />
                    {this.renderError()}
                    <Button disabled={this.state.disabled} onPress={()=> this.checkCode()} style={{backgroundColor: (this.state.disabled) ? "grey" : "green", padding: 10, borderRadius: 10, height: 60, alignItems: "center", width: "100%", justifyContent: "center", marginTop: 100}} >
                        <Text style={{color: "#FFFFFF"}}>Comprobar   </Text>{this.renderSpinner()}
                    </Button>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        backgroundColor: "#FFFFFF",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
    }
});

