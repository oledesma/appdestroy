import React, {Component} from 'react';
import {StyleSheet, View, Animated, Dimensions, Text} from 'react-native';
import LottieView from 'lottie-react-native';
const screenHeight = Dimensions.get("window").height;

export default class Explosion extends Component{
    state={
        top: new Animated.Value(1000),
        opacity: new Animated.Value(0),
        animateX: new Animated.Value(1000)
    }

    componentDidMount() {}

    componentDidUpdate() {
        if(this.props.isActive){
            this.animation.play();
            setTimeout(()=>{
                Animated.timing(this.state.animateX, {
                    toValue: 0,
                    duration: 1000,
                    useNativeDriver: false
                }).start();
            }, 2000)
        }else {
            this.animation.loop = false;
        }
    }

    render() {
        return (
            <Animated.View style={{
                width: "100%",
                height: "100%",
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                backgroundColor: "black",
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
            }}>
                <LottieView
                    source={require("../AnimationSources/boom.json")}
                    autoPlay={false}
                    loop={false}
                    ref={animation =>{
                        this.animation=animation
                    }}
                />
                <Animated.Text
                    style={{
                        color: "#FFFFFF",
                        fontSize: 20,
                        transform:
                            [
                                {translateX: this.state.animateX}
                            ]
                    }}
                >
                    Tu mensaje se ha autodestruido.
                </Animated.Text>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        backgroundColor: "rgba(255,255,255,0.9)",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        zIndex: 12
    }
});
