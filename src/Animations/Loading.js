import React, {Component} from 'react';
import {StyleSheet, View,Animated, Dimensions} from 'react-native';
import LottieView from 'lottie-react-native';

const screenHeight = Dimensions.get("window").height;

export default class Loading extends Component{
    state={
        top: new Animated.Value(1000),
        opacity: new Animated.Value(0)
    }

    componentDidMount() {

    }

    componentDidUpdate() {
        if(this.props.isActive){
            Animated.timing(this.state.top, {toValue: 0, duration: 100, useNativeDriver: false}).start();
            Animated.timing(this.state.opacity, {toValue: 1, useNativeDriver: false}).start();

            this.animation.play();
        }else {
            Animated.timing(this.state.top, {toValue: screenHeight, duration: 100, useNativeDriver: false}).start();
            Animated.timing(this.state.opacity, {toValue: 0, useNativeDriver: false}).start();

            this.animation.loop = false;
        }
    }

    render() {
        return (
            <Animated.View style={{
                top: this.state.top,
                opacity: this.state.opacity,
                width: "100%",
                height: "100%",
                backgroundColor: "rgba(255,255,255,0.7)",
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                left: 0,
            }}>
                <LottieView
                    source={require("../AnimationSources/shipLoading.json")}
                    autoPlay={false}
                    ref={animation =>{
                        this.animation=animation
                    }}
                />
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        backgroundColor: "rgba(255,255,255,0.9)",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        zIndex: 12
    }
});
