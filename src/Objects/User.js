export default class User{
    constructor(username, email, password, uid){
        this._username = username;
        this._email = email;
        this._password = password;
        this._uid = uid;
    }


    get username() {
        return this._username;
    }

    set username(value) {
        this._username = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }

    get password() {
        return this._password;
    }

    set password(value) {
        this._password = value;
    }

    get uid() {
        return this._uid;
    }

    set uid(value) {
        this._uid = value;
    }
}
