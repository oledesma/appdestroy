import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableHighlight, TouchableOpacity, Animated, TouchableWithoutFeedback} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

export default class AddButton extends Component{
    animated = new Animated.Value(0)

    toggleMenu= ()=> {
        const toValue = this.open ? 0 : 1;

        Animated.spring(this.animated, {
            toValue,
            friction: 5,
            useNativeDriver: false
        }).start();

        this.open = !this.open;
    }
    handlePress(type){
        this.props.onPressButton(type)

    }
    render() {
        const cardStyle = {
            transform: [
                {scale: this.animated},
                {
                    translateY: this.animated.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, -140]
                    })
                }
            ]
        }

        const codeStyle = {
            transform: [
                {scale: this.animated},
                {
                    translateY: this.animated.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, -80]
                    })
                }
            ]
        }
        const rotation = {
            transform: [
                {
                    rotate: this.animated.interpolate({
                        inputRange: [0, 1],
                        outputRange: ["0deg", "45deg"]
                    })
                }
            ]
        }

        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={()=> this.handlePress("sd")}>
                    <Animated.View style={[styles.button, styles.menu, rotation]}>
                        <FontAwesomeIcon size={23} name="plus" color="#FFFFFF"/>
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        alignItems: "center",
        flex: 1
    },
    button:{
        position: "absolute",
        width: 50,
        height: 50,
        borderRadius: 360,
        alignItems: "center",
        justifyContent: "center",
        elevation: 10,
        borderWidth: 3,
        borderColor: "#FFFFFF",
    },
    menu:{
        backgroundColor: "#7d5fff",
    },
    secondary: {
        width: 48,
        height: 48,
        borderRadius: 360,
        backgroundColor: "#FFFFFF",
        borderColor: "#7d5fff",
    }
});
