import {View, StyleSheet, Text} from 'react-native';
import React from 'react';
import {IconButton} from 'react-native-paper';

export default function BottomSheet(props){
    return(
        <View style={styles.container}>
            <View style={styles.backdrop}/>
            <View style={styles.card}>
                <Text>ho.la</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        backgroundColor: "#FFFFFF",
        bottom: 0,
        left: 0,
        right:0,
        top: 0,
        borderTopRightRadius: 24,
        borderTopLeftRadius: 24,
        alignItems: "center",
        justifyContent: "center"
    },
    backdrop:{

    },
    card:{

    },
    indicator:{
        height: 3,
        width: 80,
        backgroundColor: "grey",
        borderRadius: 3,
    },
    buttonsHeader:{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%"
    }
})
