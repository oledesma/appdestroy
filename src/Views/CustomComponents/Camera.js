import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Slider,
    TouchableWithoutFeedback,
    Dimensions,
    Animated,
    Vibration
} from 'react-native';
// eslint-disable-next-line import/no-unresolved
import { RNCamera } from 'react-native-camera';
import {Button, Icon} from 'native-base'

const WIDTH_D = Dimensions.get("window").width;
const HEIGHT_D = Dimensions.get("window").height;

const flashModeOrder = {
    off: 'on',
    on: 'auto',
    auto: 'torch',
    torch: 'off',
};

const wbOrder = {
    auto: 'sunny',
    sunny: 'cloudy',
    cloudy: 'shadow',
    shadow: 'fluorescent',
    fluorescent: 'incandescent',
    incandescent: 'auto',
};

const landmarkSize = 2;

export default class Camera extends React.Component {
    state = {
        flash: 'off',
        zoom: 0,
        autoFocus: 'on',
        autoFocusPoint: {
            normalized: { x: 0.5, y: 0.5 }, // normalized values required for autoFocusPointOfInterest
            drawRectPosition: {
                x: Dimensions.get('window').width * 0.5 - 32,
                y: Dimensions.get('window').height * 0.5 - 32,
            },
        },
        depth: 0,
        type: 'back',
        whiteBalance: 'auto',
        ratio: '16:9',
        recordOptions: {
            mute: false,
            maxDuration: 5,
            quality: RNCamera.Constants.VideoQuality['288p'],
        },
        canDetectFaces: false,
        canDetectText: false,
        canDetectBarcode: false,
        faces: [],
        textBlocks: [],
        barcodes: [],
        //mios
        backgroundColor: "transparent",
        longPressActive: false,
        isRecording: false,
        flashStatus: "flash-off"
    };

    takePhoto(){
        if(this.state.isRecording){
            this.stopRecord();
        }else{
            this.setState({backgroundColor: "red"}, async ()=>{
                setTimeout(()=>{
                    this.setState({backgroundColor: "transparent"});
                }, 100);
                const data = await this.camera.takePictureAsync();
                this.props.newPhoto(data.uri);
            })
        }
    }

    async startRecord(){

        try {
            const promise = this.camera.recordAsync();
            if (promise) {
                Vibration.vibrate(200);
                this.setState({backgroundColor: "red", longPressActive: true, isRecording: true})
                const data = await promise;
                this.props.newVideo(data.uri);
            }
        } catch (e) {
            console.error(e);
        }
    }

    async stopRecord(){
        this.setState({backgroundColor: "transparent"});
        this.camera.stopRecording();
    }

    onLongPress(){

        this.startRecord();
    }

    changeCameraPosition(){
        console.log("PRESS")
        this.setState({
            type: (this.state.type === "back") ? "front" : "back"
        })
    }

    changeFlashStatus(){
        this.setState({
            flash: (this.state.flash === "off") ? "torch" : "off",
            flashStatus: (this.state.flash === "torch") ? "flash-off" : "flash"
        }, ()=> console.log(this.state.flash))
    }

    renderCamera() {
        return (
            <RNCamera
                ref={(ref)=> this.camera = ref}
                style={{
                    width: "100%",
                    height: "100%"
                }}
                flashMode={this.state.flash}
                autoFocus={this.state.autoFocus}
                type={this.state.type}
            >
                <Button
                    transparent
                    style={{
                        position:"absolute",
                        top: 10,
                        left: 10,
                    }}
                    onPress={()=> this.changeFlashStatus()}
                >
                    <Icon name={this.state.flashStatus} type="MaterialCommunityIcons" style={{color: "#FFFFFF"}}/>
                </Button>
                <View
                    style={{
                        position: "absolute",
                        left:0,
                        right: 0,
                        bottom: 0,
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center"
                    }}
                >
                    <Button transparent onPress={()=> this.changeCameraPosition()}>
                        <Icon name="camera-retake-outline" type="MaterialCommunityIcons" style={{color: "grey"}}/>
                    </Button>
                    <TouchableWithoutFeedback
                        onPress={()=>{
                            this.takePhoto();
                        }}

                        onLongPress={()=>{
                            if(this.props.enableVideo){
                                this.onLongPress();
                            }
                        }}
                    >
                        <View
                            style={{
                                borderRadius: 360,
                                borderWidth: 2,
                                borderColor: "#FFFFFF",
                                padding: 10,
                                width: 70,
                                height: 70,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <Animated.View
                                style={{
                                    backgroundColor: this.state.backgroundColor,
                                    borderRadius: 360,
                                    width: 60,
                                    height: 60
                                }}
                            />
                        </View>
                    </TouchableWithoutFeedback>
                    <Button transparent onPress={()=>this.props.cancel()}>
                        <Icon name="close" type="MaterialCommunityIcons" style={{color: "#FFFFFF"}}/>
                    </Button>
                </View>
            </RNCamera>
        );
    }

    render() {
        return <View style={styles.container}>{this.renderCamera()}</View>;
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height:"100%",
        backgroundColor: '#000',
    },
    flipButton: {
        flex: 0.3,
        height: 40,
        marginHorizontal: 2,
        marginBottom: 10,
        marginTop: 10,
        borderRadius: 8,
        borderColor: 'white',
        borderWidth: 1,
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    autoFocusBox: {
        position: 'absolute',
        height: 64,
        width: 64,
        borderRadius: 12,
        borderWidth: 2,
        borderColor: 'white',
        opacity: 0.4,
    },
    flipText: {
        color: 'white',
        fontSize: 15,
    },
    zoomText: {
        position: 'absolute',
        bottom: 70,
        zIndex: 2,
        left: 2,
    },
    picButton: {
        backgroundColor: 'darkseagreen',
    },
    facesContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        top: 0,
    },
    face: {
        padding: 10,
        borderWidth: 2,
        borderRadius: 2,
        position: 'absolute',
        borderColor: '#FFD700',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    landmark: {
        width: landmarkSize,
        height: landmarkSize,
        position: 'absolute',
        backgroundColor: 'red',
    },
    faceText: {
        color: '#FFD700',
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        backgroundColor: 'transparent',
    },
    text: {
        padding: 10,
        borderWidth: 2,
        borderRadius: 2,
        position: 'absolute',
        borderColor: '#F00',
        justifyContent: 'center',
    },
    textBlock: {
        color: '#F00',
        position: 'absolute',
        textAlign: 'center',
        backgroundColor: 'transparent',
    },
});
