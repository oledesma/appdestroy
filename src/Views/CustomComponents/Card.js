import React, {Component} from 'react';
import {View, StyleSheet,Text,  Dimensions, Image} from 'react-native';
import {Avatar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ProgressCircle from 'react-native-progress-circle';

const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;

export default class Cards extends Component {

    state={
        percent: 10
    }

    componentDidMount() {
        console.log("props: " +this.props.item.code);
        if(this.props.item.type === "cnormal" || this.props.item.type === "cfotos" || this.props.item.type === "ccode"){
            if(this.props.item.estado === "preparado") this.setState({percent: 0});
            else if(this.props.item.estado === "en uso") this.setState({percent: 50});
            else this.setState({percent: 100})
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.columnsCard}>
                    <View style={{flexDirection: "row"}}>
                        <Text style={{color: "grey"}}>
                            tipo:
                        </Text>
                        <Text style={{fontWeight: "bold", fontSize: 17}}>
                            {
                                this.props.item.type === "cnormal"
                                    ? " CARTA"
                                    : this.props.item.type === "cfotos"
                                    ? " FOTOS"
                                    : this.props.item.type ===  "ccode"
                                        ? " CÓDIGO" : " UBICACIÓN"
                            }
                        </Text>
                    </View>
                    <Image style={{width: 50, height: 50, marginTop: 20}} source={this.props.item.type === "cnormal" ? require("../../Images/carta.png") : this.props.item.type === "cfotos" ? require("../../Images/foto.png") : this.props.item.type === "ccode" ? require("../../Images/codigo.png") : require("../../Images/tesoroMarker.png")}/>
                </View>
                <View style={styles.columnsCard}>
                    <Text
                        style={
                            this.props.item.estado === "preparado"
                            ? {color: "green", fontWeight: "bold", fontSize: 18}
                            : this.props.item.estado === "foto requerida"
                            ? {color: "#ff7d4a", fontWeight: "bold", fontSize: 18}
                            : this.props.item.estado === "en uso"
                            ? {color: "#4a74ff", fontWeight: "bold", fontSize: 18}
                            : {color: "red"}
                        }
                    >
                        {this.props.item.estado.toUpperCase()}
                    </Text>
                    <ProgressCircle
                        percent={this.state.percent}
                        radius={50}
                        borderWidth={8}
                        color="#7d5fff"
                        shadowColor="#999"
                        bgColor="#fff"
                    >
                        <Text style={{ fontSize: 18 }}>{`${this.state.percent}%`}</Text>
                    </ProgressCircle>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: "row",
        margin: 5,
        borderRadius: 10,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        maxHeight: 150,
        elevation: 5,
        borderWidth: 1,
        borderColor: "#ededed",
        backgroundColor: "#ededed"
    },
    columnsCard:{
        flexDirection: "column",
        width: "50%",
        justifyContent: "flex-start",
        alignItems: "center",
        height: "100%"
    }
});
