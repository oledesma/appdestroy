import React, {Component, useRef} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    Animated,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    Alert,
    AppState,
    Dimensions,
     BackHandler
} from 'react-native';
import Swiper from 'react-native-swiper';
import {Title, Button, Subtitle, Icon, Spinner} from 'native-base';
import {translate} from 'react-native-redash';
import LottieView from "lottie-react-native";
import Video from 'react-native-video';
import Explosion from '../../Animations/Explosion';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'rn-fetch-blob'
import LoadingCardDestroy from '../../Animations/LoadingCardDestroy';
import CountdownCircle from 'react-native-countdown-circle';
import * as Progress from 'react-native-progress';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'
import Clipboard from '@react-native-community/clipboard';
import {set} from 'react-native-reanimated';
import Geolocation from 'react-native-geolocation-service';
import FlagSecure from 'react-native-flag-secure-android';
import ImageZoom from 'react-native-image-pan-zoom';
import {PinchGestureHandler} from 'react-native-gesture-handler';
//import getVideoDurationInSeconds from 'get-video-duration';
import DataSource from '../../datasource/DataSource';
import Camera from './Camera';
//import RNLocation from 'react-native-location';
//import GetLocation from 'react-native-get-location';
const DS = new DataSource();

const WIDTH_D = Dimensions.get("window").width;
const HEIGTH_D = Dimensions.get("window").height;

export default class CardDestroy extends Component {
    constructor(props) {
        super(props);

        this.state={
            card: this.props.route.params,
            animateTitle: new Animated.Value(1000),
            animateDescription: new  Animated.Value(1000),
            animateButtonStart: new  Animated.Value(1000),
            animateDescriptionCarta: new Animated.Value(1000),
            animateButtonAccept: new Animated.Value(1000),
            activeExplosion: false,
            loadingCard: true,
            readyToDisplayImages: false,
            currentIndexSwiperFotos: 0,
            copiedMessage: false,
            countDownCode: false,
            currentUbication: {},
            misco: "",
            blurApp: false,
            animatedCamera: new Animated.Value(1000),
            loadingUbication: false,
            countdownshow: false
        }

        this.animatedButton = new Animated.Value(1);
        this.animatedButtonStart = new Animated.Value(1);
        this.animateCarta = new Animated.Value(0);

        this.handlePressIn = this.handlePressIn.bind(this);
        this.handlePressOut = this.handlePressOut.bind(this);
    }

    UNSAFE_componentWillMount() {

        if(this.state.card.captura){
            console.log("CAPTURE ENABLED");
            FlagSecure.activate();
        }else{
            FlagSecure.deactivate();
        }

        if(this.state.card.type === "cfotos"){
            let tmp = this.state.card;
            tmp.multimedia = Object.values(tmp.multimedia);
            this.setState({card: tmp});
        }

        if(this.state.card.type === "ubications"){
            this.setNextUbication();
        }
        this.setCardOpened();
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress",()=>{
            return true;
        })
        AppState.addEventListener("blur", ()=> this.setState({blurApp: true}));
        AppState.addEventListener("focus", ()=> this.setState({blurApp: false}));
        console.log(this.state.card)
        setTimeout(()=>{
            this.setState({loadingCard: false}, ()=>{
                this.startCard();
            })
        }, 5000);
        /*if(this.state.card.type === "cfotos" || this.state.card.type === "cnofot"){
            console.log("ENTRADO EN FOTOS")
            this.checkPhotoPermissions();
        }
        if(this.state.card.type === "cnormal"){
            //
        }

        if(this.state.card.type === "codigo"){
            //
        }

        */

        if(this.state.card.type === "ubications"){
            DS.database.ref("codigos/"+ this.state.card.code).on('value', snapshot=>{
                console.log("CAMBIOS CARTA: ", snapshot.val());
                let dd = snapshot.val();
                if(dd.estado !== "terminado"){
                    this.setState({card: dd}, async ()=> {
                        console.log("ESTE ES EL NUEVO CAMBIO: ", this.state.card.ubications[this.state.currentUbication.index]);

                        if(this.state.card.ubications[this.state.currentUbication.index].status === "ready to show"){
                            let tmp = {...this.state.currentUbication};
                            tmp.status = "ready to show";
                            this.setState({currentUbication: tmp}, ()=> {

                                setTimeout(()=>{
                                    console.log("APROBADO.")
                                    switch (this.state.currentUbication.type) {
                                        case "cnormal":
                                            this._swiper.scrollBy(1)
                                            Animated.timing(this.animateCarta, {
                                                toValue: 1,
                                                duration: 2000,
                                                useNativeDriver: false,

                                            }).start();
                                            break;
                                        case "cfotos":
                                            this.createIntervalMultimediaUbications();
                                            break;
                                        case "ccode":
                                            this._swiper.scrollBy(1);
                                            this.setState({countDownCode: true}, async ()=> {
                                                await this.sleep(this.state.currentUbication.duration * 1000);
                                                this.finish();
                                            });
                                            break;
                                    }
                                }, 2000);

                            })

                        }
                        if(this.state.card.ubications[this.state.currentUbication.index].status === "not complete"){
                            let tmp = {...this.state.currentUbication};
                            tmp.status = "not complete";
                            this.setState({currentUbication: tmp});
                        }
                    });
                }
            })
        }
    }

    startCard(){
        Animated.timing(this.state.animateTitle, {
            toValue: -120,
            duration: 1500,
            useNativeDriver: false
        }).start(({finished})=>{
            Animated.timing(this.state.animateDescription, {
                toValue: 15,
                duration: 1500,
                useNativeDriver: false
            }).start(({finish})=>{
                Animated.timing(this.state.animateButtonAccept, {
                    toValue: 120,
                    duration: 2000,
                    useNativeDriver: false
                }).start();
            })
        })
    }

    handlePressIn(){
        Animated.spring(this.animatedButton, {
            toValue: 0.7,
            useNativeDriver: false
        }).start()
    }

    handlePressOut(){
        Animated.spring(this.animatedButton,{
            toValue:1,
            friction: 3,
            tension: 40,
            useNativeDriver: false
        }).start()


        if(this.state.card.type == "cnormal" || this.state.card.type == "cnofot"){
            this._swiper.scrollBy(1);
            Animated.timing(this.animateCarta, {
                toValue: 1,
                duration: 2000,
                useNativeDriver: false,

            }).start();
        }

        if(this.state.card.type === "cfotos"){
            this.createIntervalPhotoVideo();
        }

        if(this.state.card.type === "ccode"){
            this._swiper.scrollBy(1);
            this.setState({countDownCode: true}, async ()=>{
                await this.sleep(this.state.card.duration * 1000);
                this.finish();
            });

        }

        if(this.state.card.type === "ubications"){
            this._swiper.scrollBy(1);
            this.controlUbication();
        }
        /*
        * Animated.timing(this.state.animateDescriptionCarta, {
            toValue: -120,
            duration: 1500,
            useNativeDriver: false
        }).start(()=> {
            Animated.timing(this.state.animateButtonStart, {
                toValue: 120,
                duration: 2000,
                useNativeDriver: false
            }).start();
        })
        * */
    }

    async controlUbication(){
        let permission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if(!permission){
            while (!permission){
                this.requestLocationPermission();
                await this.sleep(1500);
                permission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
            }
            this.controlUbication();
        }else{

            this.watchID = Geolocation.watchPosition(
                async (position)=>{
                    let distance = this.getDistanceFromLatLonInMeters(position.coords.latitude, position.coords.longitude, this.state.currentUbication.latLng.lat, this.state.currentUbication.latLng.lng);

                    if(distance <= 100 && this.state.currentUbication.status === "not complete"){
                        let tmp = {...this.state.currentUbication};
                        tmp.status = "pending approval";
                        this.setState({currentUbication: tmp}, ()=>{
                            this.displayAlert();
                        })
                        /*await this.setUbicationChange(tmp, "photo required");
                        */
                    }else{
                        if(this.state.currentUbication.status === "not complete"){
                            this.setState({coords: `Estás a ${distance} metros`});
                        }
                    }
                },
                (error)=>{

                },
                {
                    enableHighAccuracy: true,
                    distanceFilter: 5,
                    interval: 5000,
                    fastestInterval: 2000,
                }
            )

            /*
            this.watchID= Geolocation.watchPosition(position => {
                const {longitude, latitude} = position.coords;
                this.setState({coords: `${latitude} - ${longitude}`});


            })*/
        }
    }

    getDistanceFromLatLonInMeters(lat1,lon1,lat2,lon2) {
        let R = 6371e3; // Radius of the earth in km
        let dLat = this.deg2rad(lat2-lat1);  // deg2rad below
        let dLon = this.deg2rad(lon2-lon1);
        let a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        let d = R * c; // Distance in km
        return d;
    }

    deg2rad(deg) {
        return deg * (Math.PI/180)
    }

    componentWillUnmount() {
        this.watchID != null && Geolocation.clearWatch(this.watchID);
        AppState.removeEventListener("blur");
        AppState.removeEventListener("focus");
        BackHandler.removeEventListener("hardwareBackPress");
    }

    async requestLocationPermission(){
        try{
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                    'title': "Acceso a su ubicación requerida ",
                    'message': "La aplicación necesita permisos para acceder a su ubicación"
                }
            );

            console.log(granted === PermissionsAndroid.RESULTS.GRANTED);
        }catch (e) {

        }
    }

    setNextUbication(){
        console.log("HA ENTRADO A SET UBICATIONS");

        const card = {...this.state.card};
        const ubications = card.ubications;

        for(let i = 0; i < ubications.length; i++){
            if(ubications[i].status === "not complete"){
                if(ubications[i].type === "cnormal"){
                    console.log("ES CNORMAL");
                    this.setState({
                        currentUbication: {
                            index: i,
                            direction: ubications[i].direction,
                            latLng: ubications[i].point,
                            status: ubications[i].status,
                            type: ubications[i].type,
                            carta: ubications[i].carta
                        }
                    })
                    break;
                }else if(ubications[i].type === "cfotos"){
                    this.setState({
                        currentUbication: {
                            index: i,
                            direction: ubications[i].direction,
                            latLng: ubications[i].point,
                            status: ubications[i].status,
                            type: ubications[i].type,
                            duration: ubications[i].duration,
                            multimedia: ubications[i].multimedia,
                            captura: ubications[i].captura
                        }
                    })
                    break;
                }else {
                    this.setState({
                        currentUbication: {
                            index: i,
                            direction: ubications[i].direction,
                            latLng: ubications[i].point,
                            status: ubications[i].status,
                            type: ubications[i].type,
                            duration: ubications[i].duration,
                            codigo: ubications[i].codigo
                        }
                    });
                    break;
                }
            }
        }
    }

    async createIntervalPhotoVideo(){
        let temp = (this.state.card.type === "ubications") ? {...this.state.currentUbication} : {...this.state.card};
        let multimedia = (this.state.card.type=== "ubications") ? [...this.state.currentUbication.multimedia] : [...this.state.card.multimedia];
        this._swiper.scrollBy(1);
        let res = null;

        let i = 0;
        console.log("TEMP INTERVAL: ", temp);
        console.log("MULTIMEDIA INTERVAL: ", multimedia);

        for(let i = 0; i < multimedia.length; i++){
            if(multimedia[i].type === "image"){
                multimedia[i].isPlaying = !multimedia[i].isPlaying;
                temp.multimedia = multimedia;
                this.setState((this.state.card.type === "ubications") ? {currentUbication: temp} : {card: temp})
                await this.sleep(((this.state.card.type === "ubications" ) ? this.state.currentUbication.duration : this.state.card.duration) * 1000);
                this._swiperFotos.scrollBy(1);
            }if(multimedia[i].type=== "video"){
                //
                if(!multimedia[i].end){
                    if(multimedia[i].paused){
                        multimedia[i].paused = !multimedia[i].paused;
                        temp.multimedia = multimedia;
                        this.setState((this.state.card.type === "ubications") ? {currentUbication: temp} : {card: temp});
                    }
                    i--;
                }else{
                    this._swiperFotos.scrollBy(1);
                }
                await this.sleep(1000);
            }
        }
        this.finish();
    }

    countdown(seconds) {
        let timer = 0;
        let partes = seconds / 10;
        let currentPart = partes;
        let currenIndex = 1;

        this.interval = setInterval( ()=> {
            seconds--;
            this.setState({time: seconds.toString().padStart(2, "0")});
            if(seconds === 0){
                this.setState({time: "00", countdownshow: false});
                clearInterval(this.interval);
            }
        }, 1000);
    }

    async createIntervalMultimediaUbications(){
        this._swiper.scrollBy(1);
        let currentUbic = {...this.state.currentUbication};

        for(let i = 0; i < currentUbic.multimedia.length; i++){
            if(currentUbic.multimedia[i].type === "image"){
                this.setState({time: this.state.currentUbication.duration.toString().padStart(2, "0"), countdownshow: true});
                this.countdown(this.state.currentUbication.duration);
                await this.sleep((this.state.currentUbication.duration * 1000) + 2000);
                this._swiperFotos.scrollBy(1);
            }else{
                if(!currentUbic.multimedia[i].end){
                    if(currentUbic.multimedia[i].paused){
                        currentUbic.multimedia[i].paused = !currentUbic.multimedia[i].paused;
                        this.setState({currentUbication: currentUbic});
                    }
                    i--;
                }else{
                    this._swiperFotos.scrollBy(1);
                }
                await this.sleep(1000);
            }
        }
        currentUbic = null;
        this.finish();
    }

    sleep(time){
        return new Promise((resolve)=>{
            setTimeout(()=>{
                resolve(true)
            },time )
        })
    }

    async setCardOpened(){
        let set = await DS.getUserCards(this.state.card.code);
        if(set){
            set["estado"] = "en uso";
            let pushData = await DS.settDataUser(set, "codigos/" + this.state.card.code + "/");
            if(pushData){
                console.log("modificado con éxito set opened card.")
            }else{
                console.log("fallo al modificar set opened card")
            }

        }else{
            console.log("no hay carta set opened card");
        }
    }

    setUbicationChange(currentUbication, status){
        return new Promise(async (resolve)=>{
            let set = await DS.getUserCards(this.state.card.code);
            if(set){
                switch (currentUbication.type) {
                    case "cnormal":
                        set["ubications"][currentUbication.index] = {
                            carta: currentUbication.carta,
                            direction: currentUbication.direction,
                            point: currentUbication.latLng,
                            status: status,
                            type: "cnormal"
                        }
                        break;
                    case "cfotos":
                        set["ubications"][currentUbication.index] = {
                            captura: false,
                            direction: currentUbication.direction,
                            point: currentUbication.latLng,
                            status: status,
                            type: "cfotos",
                            duration: currentUbication.duration,
                            multimedia: currentUbication.multimedia
                        }
                        break;
                    case "ccode":
                        set["ubications"][currentUbication.index] = {
                            codigo: currentUbication.codigo,
                            direction: currentUbication.direction,
                            point: currentUbication.latLng,
                            status: status,
                            type: "ccode",
                            duration: currentUbication.duration
                        }
                        break;
                }

                let pushData = await DS.settDataUser(set, "codigos/" + this.state.card.code + "/");
                if(pushData){
                    console.log("modificado estado de ubicacion.")
                }else{
                    console.log("fallo al modificar estado de ubicacion")
                }


            }else{
                console.log("no hay carta estado de ubicacion");
            }
        })
    }

    async setMultimediaPosition(){
        //
    }

    async setLocalStorageStateOfCard(){
        //
    }

    find_dimesions(width,height) {
        const deviceHeight = Dimensions.get("window").height;
        const deviceWidth = Dimensions.get("window").width;
        console.log(" view width:" + width + "  " + "view height:" + height);
        console.log(
            "device width:" + deviceWidth + "  " + " device height:" + deviceHeight
        );

        if(height < deviceHeight) {
            this.setState({scrolledFin: true})
        }
    }

    isCloseToBottom({layoutMeasurement, contentOffset, contentSize}){
        const paddingToBottom= 20;
        if(layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom){
            this.setState({scrolledFin: true})
        }else{
            console.log(layoutMeasurement.height + contentOffset.y)
            console.log(contentSize.height - paddingToBottom)
        }
    }

    onVideoEnd(item, index){
        if(this.state.card.type === "ubications"){
            let tmp = {...this.state.currentUbication};
            tmp.multimedia[index].end = true;
            this.setState({currentUbication: tmp})
        }else{
            let tmp = this.state.card;
            tmp.multimedia[index].end = true;
            this.setState({card: tmp})
        }
    }

    renderMessageCopied(){
        if(this.state.copiedMessage){
            setTimeout(()=>{
                this.setState({copiedMessage: false})
            }, 6000);
            return(
                <Subtitle>¡Código copiado!</Subtitle>
            )
        }
    }

    renderContentUbications(){
        if(this.state.card.type=== "ubications"){
            if(this.state.currentUbication !== null){
                if(this.state.currentUbication.status === "ready to show"){
                    switch (this.state.currentUbication.type) {
                        case "cnormal":
                            return this.renderNormal();
                        case "cfotos":
                            return this.renderFotos();
                        case "ccode":
                            return this.renderCcode();
                    }
                }
            }
        }
    }

    cancelPhoto(){
        Animated.timing(this.state.animatedCamera, {
            toValue: 1000,
            useNativeDriver: false
        }).start(()=>{
            setTimeout(()=>{
                this.displayAlert();
            }, 800);
        })
    }

    async sendPhoto(uri){
        Animated.timing(this.state.animatedCamera,{
            toValue: 1000,
            useNativeDriver: false
        }).start(async ()=>{
            this.setState({loadingUbication: true, coords: "PENDIENTE DE APROBACIÓN."});
            let storage = await DS.createStorageCarta([{type: "image", path: uri}], "", this.state.card.code);
            if(storage){
                let uri = await DS.getFilesUri(this.state.card.code);
                console.log("FILES URI SEND PHOTO: ", uri);
                if(uri){
                    let cardData = await DS.getUserCards(this.state.card.code);
                    if(cardData){
                        console.log(cardData);
                        cardData["photoComprobation"] = {
                            url: uri[0].url
                        }
                        cardData["ubications"][this.state.currentUbication.index].status = "pending approval";
                        let dataCardPush = await DS.settDataUser(cardData, "codigos/" + this.state.card.code);
                        if(dataCardPush){
                            this.setState({loadingUbication: false});
                        }
                    }
                }
            }
        });

    }

    displayAlert(){
        Alert.alert(
            "Comprobación por foto",
            "Tendrá que hacerse una foto para poder validar su posición y esperar que el creador lo acepte.",
            [
                {
                    text: "Hacer foto", onPress: () => {
                        Animated.timing(this.state.animatedCamera,{
                            toValue: 0,
                            useNativeDriver: false
                        }).start();
                    }
                }
            ],
        );
    }

    renderUbications(){
        /*
        * <Animated.View style={{
                        top:0,
                        right: 0,
                        position: "absolute",
                        left: 0,
                        bottom: 0,
                        zIndex: 12
                    }}>
                        <LottieView
                            source={require("../../AnimationSources/locationFinding.json")}
                            autoPlay={true}
                            ref={animation =>{
                                this.animation=animation
                            }}
                        />
                    </Animated.View>
        * */
        console.log("renderizando ubications...")
        if(this.state.currentUbication !== null){
            return(
                <View style={{width: "100%", height: "100%", justifyContent: "center", alignItems: "center", backgroundColor: "blue"}}>
                    <Animated.View
                        style={{
                            position: "absolute",
                            width: WIDTH_D,
                            height: "100%",
                            zIndex: 50,
                            transform:[
                                {translateY: this.state.animatedCamera}
                            ]
                        }}
                    >
                        <Camera enableVideo={false} cancel={()=>this.cancelPhoto()} newPhoto={res=> this.sendPhoto(res)}/>
                    </Animated.View>
                    {
                        this.state.loadingUbication ?
                            <View
                                style={{
                                    position: "absolute",
                                    backgroundColor: "black",
                                    width: "100%",
                                    height: "100%",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    zIndex: 51
                                }}
                            >
                                <Spinner color='#7d5fff' />
                            </View>:
                            null
                    }
                    <Animated.Text
                        style={{
                            fontSize: 25,
                            width: "90%",
                            textAlign: "center",
                            color: "#FFFFFF",
                            position: "absolute",
                            top: 10,
                            left: 10,
                            right: 10
                        }}
                        numberOfLines={2}
                    >
                        Pista #{this.state.currentUbication.index}
                    </Animated.Text>
                    <Animated.Text
                        style={{
                            fontSize: 20,
                            width: "95%",
                            textAlign: "center",
                            color: "#FFFFFF",
                            position: "absolute",
                            top: 60,
                            left: 0,
                            right: 0
                        }}
                        numberOfLines={7}
                    >
                        Debes ir a {this.state.currentUbication.direction} para obtener
                        su pista.
                    </Animated.Text>
                    <Animated.Text
                        style={{
                            fontSize: 18,
                            width: "95%",
                            textAlign: "center",
                            color: "#FFFFFF",
                            position: "absolute",
                            top: 160,
                            left: 0,
                            right: 0
                        }}
                        numberOfLines={7}
                    >
                        {this.state.coords}
                    </Animated.Text>

                </View>
            );
        }
    }

    renderNormal(){
        let content = (this.state.card.type === "ubications") ? this.state.currentUbication : this.state.card;
        if(content !== null){
            return(
                <View style={{width: "100%", height: "100%"}}>
                    <ScrollView
                        style={{backgroundColor: "purple"}}
                        onScroll={({nativeEvent}) =>{
                            this.isCloseToBottom(nativeEvent);
                        }}
                        onContentSizeChange={(width, height) => {
                            this.find_dimesions(width,height)
                        }}
                        scrollEventThrottle={400}
                    >
                        <Animated.Text
                            style={{
                                fontSize: 20,
                                width: "95%",
                                textAlign: "center",
                                color: "#FFFFFF",
                                padding: 10,
                                opacity: this.animateCarta

                            }}
                            numberOfLines={300}
                        >
                            {content.carta}
                        </Animated.Text>
                    </ScrollView>
                    {this.state.scrolledFin ?
                        <Button style={{borderRadius: 10, position: "absolute", bottom: 10, right: 10, padding: 10}} onPress={()=> this.finish()}>
                            <Text style={{color: "#FFFFFF", fontSize: 23}}>Terminar</Text>
                        </Button>
                        :
                        null
                    }
                </View>
            )
        }
    }

    renderFotos(){
        let multimedia = (this.state.card.type === "ubications")  ? [...this.state.currentUbication.multimedia] : [...this.state.card.multimedia];
        if(multimedia !== null && multimedia !== undefined){
            return(
                <Swiper
                    ref={(ref)=> this._swiperFotos = ref}
                    index={0}
                    horizontal={false}
                    loop={false}
                    scrollEnabled={false}
                    showsPagination={false}
                    onIndexChanged={(index)=> this.setState({currentIndexSwiperFotos: index})}
                >
                    {multimedia.map((item, index)=>{
                        if(item.type == "image"){
                            return(
                                <View style={{width: "100%", height: "100%", backgroundColor: "black"}}>
                                    <ImageZoom cropWidth={Dimensions.get('window').width}
                                               cropHeight={Dimensions.get('window').height}
                                               imageWidth={WIDTH_D}
                                               imageHeight={HEIGTH_D}>
                                        <Image style={{width: "100%", height: "100%", resizeMode: "contain"}}
                                               source={{uri:item.url}}/>
                                    </ImageZoom>
                                </View>
                            )

                        }else{
                            return(
                                <View style={{width: "100%", height: "100%", backgroundColor: "black"}}>
                                    <Video
                                        source={{uri: item.url}}
                                        style={{width: "100%", height: "100%"}}
                                        resizeMode="contain"
                                        paused={item.paused}
                                        repeat={false}
                                        onEnd={()=> this.onVideoEnd(item, index)}
                                    />
                                </View>
                            )
                        }
                    })}
                </Swiper>
            );
        }
    }

    renderCcode(){
        let code = (this.state.card.type === "ubications") ? this.state.currentUbication : this.state.card;
        if(code !== null){
            return (
                <View style={{width: "100%", height: "100%", justifyContent: "center", alignItems: "center", backgroundColor: "#f7a305"}}>
                    <View style={{width: "95%", backgroundColor: "#fadca5", flexDirection: "row"}}>
                        <View style={{width: "83%", backgroundColor: "#FFFFFF", justifyContent: "center", alignItems: "center"}}>
                            <Text style={{textAlign: "center"}}>
                                {code.codigo}
                            </Text>
                        </View>
                        <Button
                            transparent
                            onPress={()=>{
                                Clipboard.setString(code.codigo);
                                this.setState({copiedMessage:true})
                            }}
                            style={{width: "18%"}}
                            contentStyle={{width: "100%", justifyContent: "center", alignItems: "center"}}
                        >
                            <Icon type="MaterialCommunityIcons" name='clipboard-outline' />
                        </Button>
                    </View>
                    {this.renderMessageCopied()}
                    <View style={{position: "absolute", bottom: 15, right: 15, zIndex: 12}}>
                        <CountdownCircleTimer
                            isPlaying={this.state.countDownCode}
                            duration={code.duration}
                            colors={[['#004777', 0.33], ['#F7B801', 0.33], ['#A30000']]}
                            size={100}
                        >
                            {({ remainingTime, animatedColor }) => (
                                <Animated.Text style={{ color: animatedColor }}>
                                    {remainingTime}
                                </Animated.Text>
                            )}
                        </CountdownCircleTimer>
                    </View>
                </View>
            )
        }
    }

    renderContent(){
        switch (this.state.card.type) {
            case 'cnormal':
                return this.renderNormal();
            case 'cfotos':
                return this.renderFotos();
            case 'ccode':
                return this.renderCcode();
            case 'ubications':
                return this.renderUbications();
        }
    }

    async setFinishCard(){
        let finish = await DS.getUserCards(this.state.card.code);
        if(finish){
            finish["estado"] = "terminado";
            let pushData = await DS.settDataUser(finish, "codigos/" + this.state.card.code + "/");
            if(pushData){
                this.props.navigation.navigate("MainScreen");
                console.log("modificado con éxito set finish card.")
            }else{
                console.log("fallo al modificar set finish card")
            }

        }else{
            console.log("no hay carta set finish card");
        }
    }

    async finish(){

        if(this.state.card.type === "ubications"){
            if(this.state.currentUbication.index === this.state.card.ubications.length - 1){
                console.log("FINISHED")
                this._swiper.scrollBy(1);
                setTimeout(()=>{
                    this.setState({activeExplosion: true}, ()=>{
                        let tmp = this.state.card;

                        setTimeout(()=>{
                            DS.database.ref("codigos/"+ this.state.card.code).off('value');
                            this.setFinishCard();
                        }, 5000)
                    });
                }, 1000);
            }else{
                let tmp = {...this.state.card};
                tmp.ubications[this.state.currentUbication.index].status = "complete";
                await DS.settDataUser(tmp, "codigos/" + tmp.code);
                this.setState({card: tmp});
                this.setNextUbication();
                //NOW MODIFY IN FIREBASE
                this._swiper.scrollBy(-1);
            }
        }else{
            console.log("finish normal")
            this._swiper.scrollBy(1);
            setTimeout(()=>{
                this.setState({activeExplosion: true}, ()=>{
                    setTimeout(()=>{
                        this.setFinishCard();
                    }, 5000)
                });
            }, 1000);
        }
    }

    render() {
        //<LoadingCardDestroy isActive={this.state.loadingCard} />
        return (
            <View style={styles.container}>
                {
                    this.state.countdownshow ?
                        <View style={{position: "absolute", top:10, right: 10, zIndex: 12}}>
                            <View
                                style={{
                                    backgroundColor: "#7d5fff",
                                    borderRadius: 7,
                                    padding: 10,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}
                            >
                                <Text style={{color: "white"}}>{this.state.time}</Text>
                            </View>
                        </View>:
                        null
                }
                {
                    this.state.blurApp ?
                        <View style={{position: "absolute", top: 0, left: 0, right: 0, bottom: 0, justifyContent: "center", alignItems: "center", zIndex: 50, backgroundColor: "black"}}>
                            <Text style={{color: "#FFFFFF", fontSize: 30, textAlign: "center"}}>NO INTENTES CAPTURAS ;)</Text>
                        </View>
                        :
                        null
                }
                {
                    this.state.loadingCard ?
                        <View
                            style={{
                                position: "absolute",
                                backgroundColor: "black",
                                width: "100%",
                                height: "100%",
                                justifyContent: "center",
                                alignItems: "center",
                                zIndex: 51
                            }}
                        >
                            <Spinner color='#7d5fff' />
                        </View>:
                        null
                }

                <Swiper
                    ref={(ref)=> this._swiper= ref}
                    index={0}
                    loop={false}
                    scrollEnabled={false}
                    showsPagination={false}
                >
                    <View style={{width: "100%", height: "100%", justifyContent: "center", alignItems: "center", backgroundColor: "green"}}>
                        <Animated.Text
                            style={{
                                fontSize: 25,
                                width: "90%",
                                textAlign: "center",
                                color: "#FFFFFF",
                                position: "absolute",
                                transform: [
                                    {translateY: this.state.animateTitle}
                                ]
                            }}
                            numberOfLines={2}
                        >
                            ¡Bienvenido a tu carta autodestructiva!
                        </Animated.Text>
                        <Animated.Text
                            style={{
                                fontSize: 20,
                                width: "95%",
                                textAlign: "center",
                                color: "#FFFFFF",
                                position: "absolute",
                                transform: [
                                    {translateY: this.state.animateDescription}
                                ]
                            }}
                            numberOfLines={7}
                        >
                            Intentar capturar la pantalla hará que tu carta se autodestruya
                            automáticamente. Solo podrás cambiar de pantalla si tienes pistas
                            a seguir o de lo contrario también se autrodestruirá sin que puedas verlo.
                            No intentes hacer trampas ;)
                        </Animated.Text>
                        <TouchableWithoutFeedback
                            onPressIn={this.handlePressIn}
                            onPressOut={this.handlePressOut}
                        >
                            <Animated.View
                                style={{
                                    padding: 15,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    borderRadius: 8,
                                    marginTop: 70,
                                    height: 60,
                                    transform: [
                                        {translateY: this.state.animateButtonAccept},
                                        {scale: this.animatedButton}
                                    ],
                                    backgroundColor: "red"
                                }}
                            >
                                <Text style={{color: "#FFFFFF", fontSize: 25}}>Aceptar</Text>
                            </Animated.View>
                        </TouchableWithoutFeedback>
                    </View>
                    {this.renderContent()}
                    {this.renderContentUbications()}
                    <View style={{width: "100%", height: "100%"}}>
                        <Explosion isActive={this.state.activeExplosion}/>
                    </View>
                </Swiper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    }
});
