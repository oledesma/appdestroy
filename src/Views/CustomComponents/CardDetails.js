import React, {Component, createRef} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    TextInput,
    KeyboardAvoidingView, Dimensions, Animated, PanResponder, FlatList, BackHandler, TouchableWithoutFeedback, Alert,
} from 'react-native';
import {IconButton} from 'react-native-paper';
import Swiper from 'react-native-swiper';
import {Form, Item,Label, Input, Content, Icon, Button,Textarea, Title, Subtitle} from 'native-base';
import Video from 'react-native-video';
import ActionSheet from 'react-native-actions-sheet';
import MapView, {Marker} from 'react-native-maps';
import {ParallaxSwiper, ParallaxSwiperPage} from 'react-native-parallax-swiper';

import DataSource from '../../datasource/DataSource';
import {SafeAreaView} from 'react-native-safe-area-context';
import LoadingContent from '../../Animations/LoadingContent';

const DS = new DataSource();
const WIDTH_D= Dimensions.get("window").width;
const HEIGHT_D= Dimensions.get("window").height;
const actionSheetRef = createRef();

const InputTextField = (editable,) =>{
    return(
        <View style={styles.textFieldContainer}>

        </View>
    );
}

export default class CardDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: this.props.route.params.item.title,
            multimedia: [1,2],
            ubications: (this.props.route.params.item.ubications) ? this.props.route.params.item.ubications : [],
            isEditing: false,
            currentIconEdit: "pencil",
            currentIndexSwiper: 0,
            renderContent: false,
            loadingContent: true,
            userRef: this.props.route.params.userRef
        }

        this.getMultimedia(this.state.title);

    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', ()=> this.handlerBack());
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", ()=> this.handlerBack);
    }

    handlerBack(){
        console.log(this.state.currentIndexSwiper)
        if(this.state.currentIndexSwiper === 0){
            this.props.navigation.goBack();
            return true;
        }
        if(this.state.currentIndexSwiper === 1){
            let tmp = this.state.multimedia;
            for(let i = 0; i < tmp.length; i++){
                if(tmp[i].type === "video"){
                    tmp[i].paused = true;
                }
            }
            this.setState({multimedia: tmp})
            this._swiper.scrollBy(-1);
            return true;
        }
    }

    async getMultimedia(title){
        const response = await DS.getFilesUri(title);
        this.setState({multimedia: response}, ()=>{
            setTimeout(()=>{
                this.setState({renderContent: true, loadingContent: false})
            }, 2000)
        });
    }

    playVideo(item, index){
        let temp = this.state.multimedia;

        temp[index].paused = !temp[index].paused;

        this.setState({multimedia: temp});
    }

    renderHeader(){
        if(this.state.ubications.length > 0){
            if(this.props.route.params.item.status === "notStarted"){
                return(
                    <View style={{margin: 10, backgroundColor: "blue", padding: 10, justifyContent: "flex-start", alignItems: "center", maxHeight: "80%", flexDirection: "row", borderRadius: 10}}>
                        <Image source={require("../../Images/cofre.png")} style={{width: "20%", height: 80, resizeMode: "contain"}}/>
                        <View style={{flexDirection: "column", marginLeft: 15, width: "70%"}}>
                            <Title>Aún sin abrirse.</Title>
                            <Subtitle>No olvides compartir tu carta.</Subtitle>
                        </View>
                    </View>
                );
            }

            if(this.props.route.params.item.status === "started"){
                console.log("entrado aqui")
                let tmp = "";
                for(let i = 0; i < this.state.ubications.length; i++){
                    if(this.state.ubications[i].state === "current"){
                        tmp =
                            <View style={{margin: 10, backgroundColor: "blue", padding: 10, justifyContent: "flex-start", alignItems: "center", maxHeight: "80%", flexDirection: "row", borderRadius: 10}}>
                                <Image source={require("../../Images/tesoroMarker.png")} style={{width: "20%", height: 80, resizeMode: "contain"}}/>
                                <View style={{flexDirection: "column", marginLeft: 15, width: "70%"}}>
                                    <Title>{this.state.ubications[i].title}</Title>
                                    <Subtitle>{this.state.ubications[i].description}</Subtitle>
                                </View>
                            </View>;
                        break;
                    }
                }
                return tmp;
            }

            if(this.props.route.params.item.status === "finished"){
                return (
                    <View style={{margin: 10, backgroundColor: "blue", padding: 10, justifyContent: "flex-start", alignItems: "center", maxHeight: "80%", flexDirection: "row", borderRadius: 10}}>
                        <Image source={require("../../Images/explosion.png")} style={{width: "20%", height: 80, resizeMode: "contain"}}/>
                        <View style={{flexDirection: "column", marginLeft: 15, width: "70%"}}>
                            <Title>Terminado</Title>
                            <Subtitle>Tu mensaje se ha autodestruido.</Subtitle>
                        </View>
                    </View>
                )
            }
        }else{
            return(
                <View style={{margin: 10, backgroundColor: "blue", padding: 10, justifyContent: "flex-start", alignItems: "center", maxHeight: "80%", flexDirection: "row", borderRadius: 10}}>
                    <Image source={(this.props.route.params.item.status === "notStarted") ? require("../../Images/cofre.png") : (this.props.route.params.item.status  === "started") ? require("../../Images/bomba.png") : require("../../Images/explosion.png")} style={{width: "20%", height: 80, resizeMode: "contain"}}/>
                    <View style={{flexDirection: "column", marginLeft: 15, width: "70%"}}>
                        <Title>{(this.props.route.params.item.status === "notStarted") ? "Aún sin abrirse." : (this.props.route.params.item.status  === "started") ? "Abierto" : "Terminado"}</Title>
                        <Subtitle>{(this.props.route.params.item.status === "notStarted") ? "No olvides compartir tu carta." : (this.props.route.params.item.status  === "started") ? "Corre el tiempo" : "Tu mensaje se ha autodestruido."}</Subtitle>
                    </View>
                </View>
            );
        }
    }

    displayAlertConfirmation(){
        Alert.alert(
            "¿Está seguro que desea borrar la carta?",
            "Una vez se borre, tendrá que volver a crearlo.",
            [
                {
                    text: "Cancelar",
                    onPress: ()=> console.log("CANCELAR PERRO"),
                    style: "cancel"
                },
                {
                    text: "Borrar",
                    onPress: ()=> this.deleteCard(),
                }
            ],
            {cancelable: true}
        );
    }

    async deleteCard(){
        let dataUser = await DS.getUserData(this.state.userRef);
        if(dataUser){
            let cartas = dataUser["cartas"];
            for(let i = 0; i < cartas.length; i++){
                if(this.state.title === cartas[i].title){
                    cartas.splice(i, 1);
                    let setDataUser = await DS.settDataUser(cartas, "users/"+ this.state.userRef+"/cartas");
                    if(setDataUser){
                        alert("BORRADO CON EXITO.");
                        this.props.navigation.goBack();
                    }
                    else alert("ERROR AL BORRAR");
                    break;
                }
            }
        }
    }

    renderContent(){
        if(this.state.renderContent){
            return(
                <Swiper loop={false} scrollEnabled={false} showsPagination={false} index={0} ref={(ref) => this._swiper = ref} onIndexChanged={(index) => this.setState({currentIndexSwiper: index})}>
                    <>
                        <View style={[styles.header, {backgroundColor: (this.props.route.params.item.status === "notStarted") ? "red" : (this.props.route.params.item.status  === "started") ? "orange" : "green"}]}>
                            <View style={{flexDirection: "row", justifyContent: "space-between", width: "100%"}}>
                                <Button transparent onPress={()=> this.props.navigation.goBack()}>
                                    <Icon type="MaterialCommunityIcons" style={{color: "white"}} name='arrow-left'/>
                                </Button>
                            </View>
                            <Title style={{height: 40, fontSize: 25}}>{this.state.title}</Title>
                            <Subtitle style={{alignSelf: "flex-start", marginLeft: 15}}>Hace 3 días</Subtitle>
                            {this.renderHeader()}
                        </View>
                        <View style={{padding: 10}}>
                            <Text numberOfLines={4} style={{fontSize: 17}}>
                                {this.props.route.params.item.description }
                            </Text>
                        </View>
                        <View style={{width: "40%", alignSelf: "center", flexDirection:"row", justifyContent: "center", alignItems: "center", padding: 10, marginTop: 20, borderRadius: 10, backgroundColor: "green"}}>
                            <Image
                                source={require("../../Images/sandGlass.png")}
                                style={{width: 50, height: 50}}
                            />
                            <View style={{backgroundColor: "lightgreen", padding: 10, borderRadius: 10}}>
                                <Text>{this.props.route.params.item.duration}</Text>
                            </View>
                        </View>
                        <Button
                            iconRight
                            success
                            style={{justifyContent: "center", alignItems: "center", height: 58, marginTop: 30, width: "50%", alignSelf: "flex-end", borderTopLeftRadius: 10, borderBottomLeftRadius: 10}}
                            onPress={()=> this._swiper.scrollBy(1)}
                        >
                            <Text style={{color: "#FFFFFF", fontSize: 20}}>Fotos</Text>
                            <Icon type="MaterialCommunityIcons"  name='chevron-right' style={{color: "#FFFFFF"}}/>
                        </Button>

                        <Button iconRight  danger style={{bottom: 0,position: "absolute", left: 0, right: 0, justifyContent: "center", alignItems: "center", height: 58}} onPress={()=> this.displayAlertConfirmation()}>
                            <Text style={{color: "#FFFFFF", fontSize: 20}}>Borrar</Text>
                            <Icon type="MaterialCommunityIcons"  name='delete-outline' style={{color: "#FFFFFF"}}/>
                        </Button>
                    </>
                    <View style={{width: "100%", height: "100%"}}>
                        <View style={{position: "absolute", top: 10, left: 7, zIndex: 12}}>
                            <Button transparent onPress={()=> this._swiper.scrollBy(-1)}>
                                <Icon type="MaterialCommunityIcons" style={{color: "white"}} name='arrow-left'/>
                            </Button>
                        </View>
                        <Swiper index={0} loop={false} horizontal={false} showsPagination={false}>
                            {this.state.multimedia.map((item, index)=>{
                                if(item.type === "image"){
                                    return(
                                        <Image source={{uri: item.url}} style={{width: "100%", height: "100%"}}/>
                                    );
                                }else{
                                    return(
                                        <TouchableWithoutFeedback  style={{justifyContent: "center", alignItems: "center", backgroundColor: "black", width: "100%", height: "100%"}} onPress={()=> this.playVideo(item, index)}>
                                            <View style={{width: "100%", height: "100%", backgroundColor: "black", alignItems: "center", justifyContent: "center"}}>
                                                {(item.paused) ?
                                                    <Button transparent style={{position: "absolute", zIndex: 12}}>
                                                        <Icon type="MaterialCommunityIcons" style={{color: "white"}} name='play' size={30}/>
                                                    </Button>
                                                    :
                                                    null
                                                }
                                                <Video source={{uri: item.url}} style={{width: "100%", height: "100%"}} resizeMode="contain" paused={item.paused}/>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    );
                                }
                            })}
                        </Swiper>
                    </View>
                </Swiper>
            );
        }else{
            return(
                <LoadingContent isActive={this.state.loadingContent}/>
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderContent()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: "column",
        width: "100%",
        height: "100%"
    },
    header:{
        alignItems: "center",
        height: "45%",
    },
    backgroundImage: {
        width: WIDTH_D,
        height: HEIGHT_D
    },
    foregroundTextContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },
    foregroundText: {
        fontSize: 34,
        fontWeight: "700",
        letterSpacing: 0.41,
        color: "white"
    }
});
