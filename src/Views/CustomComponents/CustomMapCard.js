import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import MAP_STYLE from './CUSTOM_MAP.json';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class CustomMapCard extends Component {

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        console.log(this.props);
    }

    render() {
        return(
            <View style={styles.container}>
                <View styles={styles.header}>
                    <Text style={{textAlign: "center", padding: 10, fontSize: 35, color: "#FFFFFF"}}>{this.props.title}</Text>
                    <Text style={{textAlign: "center", padding: 10, marginTop: 30, fontSize: 16, color: "#FFFFFF"}} numberOfLines={6}>{this.props.description}</Text>
                    <View style={{flexDirection: "row", justifyContent: "center", alignItems: "center", width: "100%", marginTop: 30}}>
                        <Icon name="map-marker" color={"#FFFFFF"} size={25} style={{alignSelf: "center"}} />
                        <Text style={{fontSize: 13, fontStyle: "italic", color: "#FFFFFF"}} numberOfLines={1}>{this.props.direction}</Text>
                    </View>
                </View>
                <MapView
                    ref={(map)=> this.mapRef = map}
                    style={styles.map} customMapStyle={MAP_STYLE}
                    onMapReady={()=> {
                        this.mapRef.fitToSuppliedMarkers(["mk0"], {
                            edgePadding:{
                                top: 10,
                                right: 10,
                                left: 10,
                                bottom: 10
                            }
                        })
                    }}

                    zoomEnabled={false}
                    zoomTapEnabled={false}
                    scrollEnabled={false}
                    rotateEnabled={false}
                    maxZoomLevel={15}
                >
                    <Marker
                        coordinate={this.props.marker.latLng}
                        icon={require("../../Images/tesoroMarker.png")}
                        identifier="mk0"
                    />
                </MapView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        width: "90%",
        height: "90%",
        borderRadius: 14,
        backgroundColor: "#e74c3c",
        marginTop: 10,
        flexDirection: "column",
        justifyContent: "space-between",
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 7,
    },
    map:{
        width: "100%",
        height: "40%"
    },
    header:{
        width: "90%",
        height: "60%",
        flexDirection: "column",
    }
});
