import {View, StyleSheet} from 'react-native';
import React from 'react';
import {IconButton} from 'react-native-paper';

export function RightSheet(props){
    return(
        <>
            <View style={styles.backdrop}/>
            <View style={styles.card}>
                {props.children}
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    backdrop:{

    },
    card:{
        position: "absolute",
        backgroundColor: "#FFFFFF",
        bottom: 0,
        left: 0,
        right:0,
        top: 0,
        borderBottomLeftRadius: 24,
        borderTopLeftRadius: 24,
        alignItems: "center",
        justifyContent: "center"
    },
    indicator:{
        height: 3,
        width: 80,
        backgroundColor: "grey",
        borderRadius: 3,
    },
    buttonsHeader:{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%"
    }
})
