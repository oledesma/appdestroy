import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, BackHandler,
} from 'react-native';
import {Button} from 'native-base'

export default class FirstScreen extends Component {
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=>{
            BackHandler.exitApp();
            return true;
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={{width: 120, height: 120}} source={require("../Images/exampleLogo.png")}/>
                <Button
                    style={{width: "80%", height: 55, backgroundColor: "#7d5fff", justifyContent: "center", alignItems: "center", borderRadius: 12, marginTop: 80}}
                    onPress={()=> this.props.navigation.navigate("LoginScreen")}
                >
                    <Text style={{color: "#FFFFFF"}}>Iniciar sesión</Text>
                </Button>
                <Button
                    style={{width: "80%", height: 55, backgroundColor: "#7d5fff", justifyContent: "center", alignItems: "center", borderRadius: 12, marginTop: 20}}
                    onPress={()=> this.props.navigation.navigate("RegisterScreen")}
                >
                    <Text style={{color: "#FFFFFF"}}>Registrarse</Text>
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    }
});
