import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
} from 'react-native';
import {TextInput, HelperText} from 'react-native-paper';
import Button2 from 'react-native-paper/src/components/Button';
import IconButton from 'react-native-paper/src/components/IconButton';
import DataSource from '../datasource/DataSource';
import { GoogleSignin } from '@react-native-community/google-signin';
import auth from "@react-native-firebase/auth";
import {Button, Item, Input, Label, Icon, Spinner} from 'native-base';

const DS = new DataSource();

export default class LoginForm extends Component {
    constructor(props){
        super(props);

        this.state = {
            user: "",
            password: "",
            errorUser: false,
            errorUserInput: false,
            errorUserText: "",
            errorGeneral: false,
            errorGeneralInput: false,
            errorGeneralText: "",
            loading: false
        };

    }

    componentDidMount(){
        console.disableYellowBox= true;
        console.reportErrorsAsExceptions = false;
        GoogleSignin.configure({
            webClientId: '950364911881-tn0ao35kqmtu30kek5246ss963bea0lc.apps.googleusercontent.com'
        });
    }



    async _onGoogleButtonPress(){
        //get the user ID token
        const {idToken} = await GoogleSignin.signIn();

        //Create a google credential with the token
        const googleCredential = auth().GoogleAuthProvider.credential(idToken);

        return auth().signInWithCredential(googleCredential);
    }

    _hasErrorsUser = () =>{
        return this.state.errorUser;
    };

    returnToOriginalState(){
        this.setState({
            user: "",
            password: "",
            errorUser: false,
            errorUserInput: false,
            errorUserText: "",
            errorGeneral: false,
            errorGeneralInput: false,
            errorGeneralText: "",
            loading: false
        });
    }

    goTo(routeName) {
        this.returnToOriginalState();
        this.props.navigation.navigate(routeName);
    }

    async onLogin(){
        this.setState({error: false, errorMessage: "", loading: true})
        if(this.state.user !== "" && this.state.password !== ""){
            let userUID = await DS.checkIfUserExist(this.state.user);
            if(userUID){
                let userEmail = await DS.findUserEmail(userUID);
                if(userEmail){
                    let loginResponse = await DS.loginEmailPassword(userEmail, this.state.password, this.state.user);
                    switch (loginResponse) {
                        case true:
                            this.setState({loading: false});
                            this.goTo("MainScreen");
                            break;
                        case "userdisabled":
                            this.setState({error: true, errorMessage: "El usuario ha sido deshabilitado. Pongase en contacto con el soporte para más información.", loading: false})
                            break;
                        case "usernotfound":
                            this.setState({error: true, errorMessage: "No existe una cuenta asociada a este usuario.", loading: false});
                            break;
                        case "wronpassword":
                            this.setState({error: true, errorMessage: "El usuario o la contraseña son incorrectos.", loading: false});
                            break;
                    }
                }else{
                    this.setState({error: true, errorMessage: "Ha ocurrido un error interno. Vuelva a intentarlo mas tarde.", loading: false});
                }
            }else{
                this.setState({error: true, errorMessage: `No existe el usuario ${this.state.user}`, loading: false})
            }
        }else{
            this.setState({error: true, errorMessage: "Debe rellenar los campos.", loading: false})
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Item floatingLabel style={{backgroundColor: (this.state.loading) ? "#d1d1d1" : ""}} error={this.state.error}>
                    <Icon type="AntDesign" name='user' />
                    <Label>Usuario</Label>
                    <Input
                        getRef={username => {
                            this.username = username;
                        }}
                        disabled={this.state.loading}
                        maxLength={30}
                        onChangeText={user => this.setState({user})}
                        autoCapitalize={false}
                        returnKeyType="next"
                        onSubmitEditing={()=> this.password._root.focus()}
                    />
                </Item>
                <Item floatingLabel style={{marginTop: 15, marginBottom: 20, backgroundColor: (this.state.loading) ? "#d1d1d1" : ""}} error={this.state.error}>
                    <Icon type="AntDesign" name='lock' />
                    <Label>Contraseña</Label>
                    <Input
                        getRef={password => {
                            this.password = password;
                        }}
                        disabled={this.state.loading}
                        maxLength={30}
                        secureTextEntry={true}
                        onChangeText={password => this.setState({password})}
                        returnKeyType="done"
                        onEndEditing={()=> this.onLogin()  }
                    />
                </Item>
                {
                    this.state.error ?
                        <Text style={{color: "red"}}>{this.state.errorMessage}</Text>
                        :
                        null
                }
                <Button style={[styles.buttonLogin, {backgroundColor: (this.state.loading) ? "grey" : "#7d5fff"}]} onPress={() => this.onLogin()} disabled={this.state.loading}>
                    <Text style={{color: "white"}}>Iniciar sesión</Text>
                    {
                        this.state.loading ?
                            <Spinner color="#7d5fff" />:null
                    }
                </Button>
                <Text style={{textAlign: "center", marginTop: 20, marginBottom: 20, fontSize: 15}}>-----O conectate con-----</Text>
                <View style={{flexDirection: "row", alignItems: "center", justifyContent: "center", marginBottom: 20}}>
                    <IconButton
                        icon="google-plus"
                        color={"orange"}
                        style={{backgroundColor: "white", marginRight: "15%"}}
                        size={40}
                        onPress={() => this._onGoogleButtonPress()}
                    />
                    <IconButton
                        icon="facebook"
                        color={"blue"}
                        style={{backgroundColor: "white"}}
                        size={40}
                        onPress={() => console.log('Pressed')}
                    />
                </View>
                <View style={{flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
                    <Text style={{textAlign:"center", fontSize: 15}}>¿Todavía no te has registrado? </Text>
                    <TouchableOpacity onPress={()=> this.goTo("RegisterScreen")}><Text style={{color: "#7d5fff", fontSize: 15}}>Registrate aquí.</Text></TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        padding: 15
    },
    input:{
        height: 60,
        marginBottom: 10
    },
    buttonLogin:{
        height: 55,
        alignItems: "center",
        justifyContent: "center",
    }
});
