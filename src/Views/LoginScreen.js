import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    View,
    Text,
    StatusBar,
    KeyboardAvoidingView,
    BackHandler
} from 'react-native';

import {TextInput} from 'react-native-paper';
import Button from 'react-native-paper/src/components/Button';
import IconButton from 'react-native-paper/src/components/IconButton';
import LoginForm from './LoginForm';


export default class LoginScreen extends Component {


    render() {
        return (
            <KeyboardAvoidingView behavior={"padding"} style={styles.container}>
                <StatusBar  backgroundColor={"#FFFFFF"}/>
                <View style={styles.logoContainer}>
                    <Image style={styles.brandImage} source={require("../Images/exampleLogo.png")}/>
                    <Text style={styles.title}>¡Explora tus oportunidades!</Text>
                </View>
                <View style={styles.formContainer}>
                    <LoginForm navigation={this.props.navigation}/>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: "#FFFFFF",
        width: "100%",
        height: "100%"
    },
    logoContainer:{
      alignItems: "center",
      flexGrow: 1 ,
      justifyContent: "center",
        padding: 20
    },
    brandImage:{
        width: 100,
        height: 100
    },
    title:{
        color: "white",
        marginTop: 10,
        width: "90%",
        textAlign: "center",
        opacity: 0.9,
        fontSize: 15
    }
});

