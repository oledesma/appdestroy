import React, {Component, createRef} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, BackHandler, Animated, Dimensions
} from 'react-native';
import Button from 'react-native-paper/src/components/Button';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DataSource from '../datasource/DataSource';
import Profile from './MainScreenComponents/Profile';
import Store from './MainScreenComponents/Store';
import Chats from './MainScreenComponents/Chats';
import Home from './MainScreenComponents/Home';
import AddButton from './CustomComponents/AddButton';

const DS = new DataSource();
const Tab = createBottomTabNavigator();
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const animatedY = new Animated.Value(height);
const animatedX = new Animated.Value(-width);
const actionSheetRef = createRef();
/*

            <Tab.Screen name="New"
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="tooltip-plus-outline" color={color} size={size} />
                    ),
                }}
            />
 */

export default class MainScreen extends Component{

    constructor(props) {
        super(props);
        this.state={

        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount() {
        console.disableYellowBox = true;
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick(e){
        BackHandler.exitApp();
        return true;
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    render() {
        const CreatePlaceholder = () => (
            <View style={{flex: 1, backgroundColor: "white"}}>

            </View>
        );
        return (
            <View style={styles.container}>
                <Tab.Navigator
                    initialRouteName="Home"
                    tabBarOptions={{
                        activeTintColor: "#18dcff",
                        inactiveTintColor: "#FFFFFF",
                        style:{
                            backgroundColor: "#7d5fff"
                        }
                    }}
                >
                    <Tab.Screen name="Home" component={Home}
                                options={{
                                    tabBarLabel: 'Inicio',
                                    tabBarIcon: ({color, size})=>(
                                        <MaterialCommunityIcons name="home" color={color} size={size} />
                                    )
                                }}
                    />
                    <Tab.Screen name="Chats" component={Chats}
                                options={{
                                    tabBarLabel: 'Chat',
                                    tabBarIcon: ({ color, size }) => (
                                        <MaterialCommunityIcons name="message" color={color} size={size} />
                                    ),
                                }}
                    />
                    <Tab.Screen name="New" component={CreatePlaceholder}
                                options={{
                                    tabBarLabel: '',
                                    tabBarIcon: ({ color, size }) => (
                                        <AddButton
                                            onPressButton={(res)=>
                                            {
                                                this.props.navigation.navigate("OptionSelector")
                                            }}
                                        />
                                    ),
                                }}
                                listeners={ ({navigation}) => ({
                                    tabPress: e=>{
                                        e.preventDefault()
                                    }
                                })
                                }
                    />
                    <Tab.Screen name="Store" component={Store}
                                options={{
                                    tabBarLabel: 'Tienda',
                                    tabBarIcon: ({ color, size }) => (
                                        <MaterialCommunityIcons name="store" color={color} size={size} />
                                    ),
                                }}
                    />
                    <Tab.Screen name="Profile" component={Profile}
                                options={{
                                    tabBarLabel: 'Perfil',
                                    tabBarIcon: ({ color, size }) => (
                                        <MaterialCommunityIcons name="account" color={color} size={size} />
                                    ),
                                }}
                    />
                </Tab.Navigator>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        flex: 1,
        zIndex: 100
    },
    backdrop:{

    }
});
