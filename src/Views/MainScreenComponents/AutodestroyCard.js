import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
} from 'react-native';
import Swiper from 'react-native-swiper';

export default class AutodestroyCard extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Swiper
                    ref={(ref)=> this._swiper= ref}
                    index={0}
                >
                    <View style={{width: "100%", height: "100%", justifyContent: "center", alignItems: "center"}}>

                    </View>

                </Swiper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    }
});
