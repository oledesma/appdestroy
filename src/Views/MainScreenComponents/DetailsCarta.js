import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, FlatList,
} from 'react-native';
import {Avatar, IconButton, Paragraph, Subheading, Title, Card, Button} from 'react-native-paper';

export default class DetailsCarta extends Component {
    state={
        creation: "",
        textShow: "Mostrar más",
        numeroLineas: 3
    };

    componentDidMount() {
        this.setTime();
    }

    checkTypeMessage(){
        const type = this.props.route.params.type;

        const description =
            <Card elevation={10} style={{backgroundColor: "#82214e"}}>
                <Card.Content>
                    <Paragraph style={{color: "#FFFFFF"}} numberOfLines={this.state.numeroLineas}>{this.props.route.params.description}</Paragraph>
                    <TouchableOpacity onPress={()=> (this.state.textShow === "Mostrar más") ? this.setState({numeroLineas: null, textShow: "Mostrar menos"}) : this.setState({numeroLineas: 3, textShow: "Mostrar más"})}>
                        <Text style={{color: "#55E6C1"}}>{this.state.textShow}</Text>
                    </TouchableOpacity>
                </Card.Content>
            </Card>;

        const gallery = <View style={{backgroundColor: "#B33771", padding: 10}}>
                            <Card elevation={10} style={{backgroundColor: "#82214e"}}>
                                <Card.Content>
                                    <Paragraph style={{color: "#FFFFFF"}}>galeria</Paragraph>
                                </Card.Content>
                            </Card>
                        </View>;

        const itemAlone = <View style={{backgroundColor: "#B33771", padding: 10}}>
                            <Card elevation={10} style={{backgroundColor: "#82214e"}}>
                                <Card.Content>
                                    <Paragraph style={{color: "#FFFFFF"}}>{this.props.route.params.description}</Paragraph>
                                </Card.Content>
                            </Card>
                        </View>;
        const ubication = <View style={{backgroundColor: "#B33771", padding: 10}}>
                                <Card elevation={10} style={{backgroundColor: "#82214e"}}>
                                    <Card.Content>
                                        <Paragraph style={{color: "#FFFFFF"}}>ubication</Paragraph>
                                    </Card.Content>
                                </Card>
                            </View>;
        let template = "";
        switch (type) {
            case "description":
                template = <View style={{backgroundColor: "#B33771", padding: 10}}>
                    {description}
                </View>;
                break;
            case "gallery":
                template =
                    <View style={{backgroundColor: "#B33771", padding: 10}}>
                        {gallery}
                    </View>;
                break;
            case "description-gallery-location":
                template =
                    <View style={{backgroundColor: "#B33771", padding: 10}}>
                        {description}
                        {gallery}
                        {ubication}
                    </View>;
                break;
            case "description-gallery":
                template =
                    <View style={{backgroundColor: "#B33771", padding: 10}}>
                        {description}
                        {gallery}
                    </View>;
                break;
            case "description-location":
                template =
                    <View style={{backgroundColor: "#B33771", padding: 10}}>
                        {description}
                        {ubication}
                    </View>;
                break;
            case "gallery-location":
                template =
                    <View style={{backgroundColor: "#B33771", padding: 10}}>
                        {gallery}
                        {ubication}
                    </View>;
                break;
        }

        return template;
    }

    setTime(){
        let initialDate = this.props.route.params.createdAt;
        let splitDate = initialDate.split(" ");
        let date = splitDate[0].split("/");
        let time = splitDate[1].split(":");

        let dd = date[0];
        let mm = date[1]-1;
        let yyyy = date[2];
        let hh = time[0];
        let min = time[1];
        let ss = time[2];

        let fecha = new Date(yyyy, mm, dd, hh, min, ss);
        let today = new Date();
        console.log("FECHA: ", fecha.getDate());
        console.log("TODAY: ", today.getDate());

        let diffTime = Math.abs(today - fecha);
        let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        let seconds = diffTime / 1000;
        let minutes = seconds / 60;
        let hours = minutes / 60;
        let days = hours / 24;
        let final = "";
        if(days > 0.99) final = (days === 1) ? `${Math.round(days)} dia`: `${Math.round(days)} dias` ;
        else{
            if(hours > 0.99) final = `${Math.round(hours)} horas.`;
            else {
                if(minutes > 0.99) final = `${Math.round(minutes)} minutos`;
                else final = `${Math.round(seconds)} segundos`;
            }
        }

        this.setState({creation: final});
    }

    render() {
        const {navigation} = this.props;
        return (
            <View style={styles.container}>
                <View style={{flexDirection: "column", justifyContent: "center", alignItems: "center", paddingRight:10, paddingLeft:10, paddingBottom: 10, backgroundColor: "#55E6C1", borderBottomEndRadius: 2}}>
                    <View style={{flexDirection: "row", justifyContent: "flex-end", alignItems: "center", width: "100%"}}>
                        <IconButton
                            icon="share"
                            color={"white"}
                            size={30}
                            onPress={() => console.log('Pressed')}
                        />
                        <IconButton
                            icon="delete"
                            color={"white"}
                            size={30}
                            onPress={() => console.log('Pressed')}
                        />
                    </View>
                    <Avatar.Icon size={64} icon={this.props.route.params.iconName}/>
                </View>
                <View style={{backgroundColor: "#55E6C1" }}>
                    <View style={{flexDirection: "row", backgroundColor: "#B33771", borderTopLeftRadius: 30, borderTopRightRadius: 30, justifyContent: "space-between", alignItems: "center", padding: 20}}>
                        <Title style={{color: "#FFFFFF"}}>{this.props.route.params.title}</Title>
                        <Subheading style={{color: "#FFFFFF"}}>{this.state.creation}</Subheading>
                    </View>
                    {this.checkTypeMessage()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    }
});
