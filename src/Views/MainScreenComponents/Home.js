import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, FlatList,
    TouchableWithoutFeedback, Animated, Vibration, Alert,
} from 'react-native';
import {Avatar} from 'react-native-paper';
import CustomListview from '../CustomComponents/CustomListView';
import CustomRow from '../CustomComponents/CustomRow';
import Card from '../CustomComponents/Card';
import DataSource from '../../datasource/DataSource';
import CardsLoading from '../../Animations/CardsLoading';
import Modal from 'react-native-modal'
import {Button, Icon} from 'native-base';
import Clipboard from '@react-native-community/clipboard';
import Swiper from 'react-native-swiper'
import Video from 'react-native-video';

const DS = new DataSource();

export default class Home extends Component {

    state = {
        data: [],
        loading: false,
        isVisible: false,
        refreshing: false
    }

    _onPressItem(item){
        this.props.navigation.navigate("DetailsCarta", item);
    }

    componentDidMount() {
        this.setState({loading: true}, ()=>{
            this.getCards();
        });

    }

    async getCards(){
        let userRef = await DS.getLocalStorageUserData();
        console.log("USER REF: ", userRef);

        DS.database.ref("users/"+ userRef+"/cartas").on('value', async snapshot=>{
            if(snapshot.exists()){
                let codigos = snapshot.val();
                console.log(codigos)
                let cartas = [];
                let card = null;
                for(let i = 0; i < codigos.length ;i++){
                    card = await DS.getUserCards(codigos[i]);
                    if(card) cartas.push(card);
                    card = null;
                }
                this.sortCards(cartas);
                //cartas.sort((a, b) => parseFloat(a.price) - parseFloat(b.price));

            }else{
                //
                setTimeout(()=>{
                    this.setState({data: [], loading: false, refreshing: false});
                }, 5000)
            }

        })

    }

    sortCards(cartas){
        cartas.sort((a, b)=>{
            let splitDate = a.fechaCreacion.split(" ");
            let date = splitDate[0].split("/");
            let time = splitDate[1].split(":");

            let dd = date[0];
            let mm = date[1]-1;
            let yyyy = date[2];
            let hh = time[0];
            let min = time[1];
            let ss = time[2];

            let splitDate2 = b.fechaCreacion.split(" ");
            let date2 = splitDate2[0].split("/");
            let time2 = splitDate2[1].split(":");

            let dd2 = date2[0];
            let mm2 = date2[1]-1;
            let yyyy2 = date2[2];
            let hh2 = time2[0];
            let min2 = time2[1];
            let ss2 = time2[2];

            let fecha = new Date(yyyy, mm, dd, hh, min, ss);
            let fecha2 = new Date(yyyy2, mm2, dd2, hh2, min2, ss2);

            return fecha2 - fecha;
        })
        setTimeout(()=>{
            this.setState({data: cartas, loading: false, refreshing: false});
        }, 5000)
    }

    goTo(routeName, item) {
        this.props.navigation.navigate(routeName, {item: item, userRef: this.state.userRef});
    }

    displayModal(item){
        this.setState({isVisible: true, currentItem: item});
    }

    video(item, index){
        let tmp = {...this.state.currentItem};

        tmp.multimedia[index].paused = !tmp.multimedia[index].paused;
        this.setState({currentItem: tmp});
    }

    renderMultimedia(){
        if(this.state.currentItem){
            console.log(this.state.currentItem.multimedia)
            return(
                <Swiper
                    index={0}
                    loop={false}
                >
                    {
                        this.state.currentItem.multimedia.map((item, index)=>{
                            if(item.type === "image"){
                                return(
                                    <View Style={{width: "100%", height: "100%"}}>
                                        <Image style={{width: "100%", height: "100%", resizeMode: "contain"}} source={{uri: item.url}}/>
                                    </View>
                                )
                            }else{
                                return(
                                    <TouchableWithoutFeedback
                                        onPress={()=> this.video(item, index)}
                                    >
                                        <View Style={{width: "100%", height: "100%", justifyContent: "center", alignItems: "center"}}>
                                            {
                                                item.paused ?
                                                    <Icon
                                                        style={{
                                                            position: "absolute",
                                                            zIndex: 12,
                                                            color: "#FFFFFF",
                                                            alignSelf: "center",
                                                            top: "50%"
                                                        }}

                                                        name="play"
                                                        type="MaterialCommunityIcons"
                                                    />:null
                                            }
                                            <Video style={{width: "100%", height: "100%"}} resizeMode="contain" repeat={true} source={{uri: item.url}} paused={item.paused}/>
                                        </View>
                                    </TouchableWithoutFeedback>
                                )
                            }
                        })
                    }
                </Swiper>
            )
        }
    }

    renderModalContent(){
        if(this.state.currentItem){
            console.log(this.state.currentItem);
            switch (this.state.currentItem.type) {
                case "cnormal":
                    return(
                        <View style={{width: "100%", height: "100%", padding: 10, justifyContent: "center", alignItems: "center"}}>
                            <View style={{width: "95%", backgroundColor: "#a592f7", flexDirection: "row", height: "15%"}}>
                                <View style={{width: "80%", backgroundColor: "#7d5fff", justifyContent: "center", alignItems: "center"}}>
                                    <Text style={{textAlign: "center", color: "#FFFFFF"}}>
                                        {this.state.currentItem.code}
                                    </Text>
                                </View>
                                <Button
                                    transparent
                                    onPress={()=>{
                                        Clipboard.setString(this.state.currentItem.code);
                                    }}
                                    style={{width: "20%"}}
                                    contentStyle={{width: "100%", justifyContent: "center", alignItems: "center"}}
                                >
                                    <Icon type="MaterialCommunityIcons" name='clipboard-outline' />
                                </Button>
                            </View>
                            <View style={{width: "100%", height: "85%"}}>
                                <ScrollView>
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            width: "95%",
                                            textAlign: "center",
                                            color: "black",

                                        }}
                                        numberOfLines={300}
                                    >
                                        {this.state.currentItem.carta}
                                    </Text>
                                </ScrollView>
                            </View>
                        </View>
                    );
                case "cfotos":
                    return(
                        <View style={{width: "100%", height: "100%", padding: 10, justifyContent: "center", alignItems: "center"}}>
                            <View style={{width: "95%", backgroundColor: "#a592f7", flexDirection: "row", height: "15%"}}>
                                <View style={{width: "80%", backgroundColor: "#7d5fff", justifyContent: "center", alignItems: "center"}}>
                                    <Text style={{textAlign: "center", color: "#FFFFFF"}}>
                                        {this.state.currentItem.code}
                                    </Text>
                                </View>
                                <Button
                                    transparent
                                    onPress={()=>{
                                        Clipboard.setString(this.state.currentItem.code);
                                    }}
                                    style={{width: "20%"}}
                                    contentStyle={{width: "100%", justifyContent: "center", alignItems: "center"}}
                                >
                                    <Icon type="MaterialCommunityIcons" name='clipboard-outline' />
                                </Button>
                            </View>
                            <View style={{width: "100%", height: "85%", paddingTop: 10}}>
                                {this.renderMultimedia()}
                            </View>
                        </View>
                    );
                case "ccode":
                    return(
                        <View style={{width: "100%", height: "100%", padding: 10, justifyContent: "center", alignItems: "center"}}>
                            <View style={{width: "95%", backgroundColor: "#a592f7", flexDirection: "row", height: "15%"}}>
                                <View style={{width: "80%", backgroundColor: "#7d5fff", justifyContent: "center", alignItems: "center"}}>
                                    <Text style={{textAlign: "center", color: "#FFFFFF"}}>
                                        {this.state.currentItem.code}
                                    </Text>
                                </View>
                                <Button
                                    transparent
                                    onPress={()=>{
                                        Clipboard.setString(this.state.currentItem.code);
                                    }}
                                    style={{width: "20%"}}
                                    contentStyle={{width: "100%", justifyContent: "center", alignItems: "center"}}
                                >
                                    <Icon type="MaterialCommunityIcons" name='clipboard-outline' />
                                </Button>
                            </View>
                            <View style={{width: "100%", height: "85%", justifyContent: "center", alignItems: "center"}}>
                                <Text style={{fontWeight: "bold", fontSize: 23}}>CÓDIGO: </Text>
                                <Text style={{fontSize: 20}}>{this.state.currentItem.codigo}</Text>
                            </View>
                        </View>
                    );
                case "ubications":
                    return(
                        <View style={{width: "100%", height: "100%", padding: 10, justifyContent: "center", alignItems: "center"}}>
                            <View style={{width: "95%", backgroundColor: "#a592f7", flexDirection: "row", height: "15%"}}>
                                <View style={{width: "80%", backgroundColor: "#7d5fff", justifyContent: "center", alignItems: "center"}}>
                                    <Text style={{textAlign: "center", color: "#FFFFFF"}}>
                                        {this.state.currentItem.code}
                                    </Text>
                                </View>
                                <Button
                                    transparent
                                    onPress={()=>{
                                        Clipboard.setString(this.state.currentItem.code);
                                    }}
                                    style={{width: "20%"}}
                                    contentStyle={{width: "100%", justifyContent: "center", alignItems: "center"}}
                                >
                                    <Icon type="MaterialCommunityIcons" name='clipboard-outline' />
                                </Button>
                            </View>
                            <View style={{width: "100%", height: "85%"}}>

                            </View>
                        </View>
                    );
            }
        }
    }

    displayAlert(item, index){
        Vibration.vibrate(200);
        Alert.alert(
            "¿Desea borrarlo la carta?",
            "Una vez borrado, tendrá que volver a crearlo.",
            [
                {
                    text: "Cancelar",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "Aceptar", onPress: () => this.deleteCard(item, index) }
            ],
            { cancelable: true }
        );
    }

    handleRefresh(){
        this.setState({
            page: 1,
            refreshing: true,
            seed: this.state.seed + 1,
            loading: true
        }, ()=>{
            this.getCards();
        })
    }

    async deleteCard(item, index){
        let userRef = await DS.getLocalStorageUserData();
        let deleteCodeCard = await DS.database.ref("codigos/" + item.code).remove();
        console.log(deleteCodeCard);
        let userData = await DS.getUserData(userRef);
        let cartas = userData["cartas"];
        cartas.splice(index, 1);
        userData["cartas"] = cartas;

        let pushData = await DS.settDataUser(userData, userRef);
        if(pushData) alert("Borrado con éxito.");

        this.getCards();
    }

    renderContent(){
        if(!this.state.loading){
            if(this.state.data.length > 0){
                return(
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item, index }) =>
                            <TouchableWithoutFeedback
                                onPress={()=> this.displayModal(item)}
                                onLongPress={()=> this.displayAlert(item, index)}
                            >
                                <View>
                                    <Card item={item}/>
                                </View>
                            </TouchableWithoutFeedback>
                        }
                        refreshing={this.state.refreshing}
                        onRefresh={()=> this.handleRefresh()}
                        keyExtractor={item => item.id}
                    />
                )
            }else{
                return(
                    <View style={{width: "100%", height: "100%", justifyContent: "center", alignItems: "center"}}>
                        <Text style={{fontSize: 18, textAlign: "center"}}>Parece que aún no tiene cartas, puede escoger algunas de las opciones para empezar: </Text>
                        <Button
                            style={{
                                height: 60,
                                width: "60%",
                                padding: 10,
                                justifyContent: "center",
                                alignItems: "center",
                                borderRadius: 5,
                                backgroundColor: "#7d5fff",
                                marginTop: 10
                            }}
                            onPress={()=> this.props.navigation.navigate("SelectCardType")}
                        >
                            <Text style={{color: "#FFFFFF", textAlign: "center"}}>Crear carta</Text>
                        </Button>
                        <Button
                            style={{
                                height: 60,
                                width: "60%",
                                padding: 10,
                                justifyContent: "center",
                                alignItems: "center",
                                borderRadius: 5,
                                marginTop: 10
                            }}
                            success
                            onPress={()=> this.props.navigation.navigate("CodeChecker")}
                        >
                            <Text style={{color: "#FFFFFF", textAlign: "center"}}>Ver carta</Text>
                        </Button>
                    </View>
                )
            }
        }
    }
    render() {
        return (
            <View style={styles.container}>
                {this.renderContent()}
                <CardsLoading isActive={this.state.loading}/>
                <Modal
                    isVisible={this.state.isVisible}
                    onBackButtonPress={()=> this.setState({isVisible: false})}
                    onBackdropPress={()=> this.setState({isVisible: false})}
                >
                    <View
                        style={{
                            width: "100%",
                            height: "60%",
                            backgroundColor: "#FFFFFF",
                            borderRadius: 8
                        }}
                    >
                        {this.renderModalContent()}
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center"
    },
    containerRow: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginLeft:10,
        marginRight:10,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
    photo: {
        height: 50,
        width: 50,
    },
});
