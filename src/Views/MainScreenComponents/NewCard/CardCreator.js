import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    Alert,
    BackHandler
} from 'react-native';
import Loading from '../../../Animations/Loading';
import DataSource from '../../../datasource/DataSource';
import Success from '../../../Animations/Success';
import {Button, Icon, Subtitle} from 'native-base';
import Clipboard from '@react-native-community/clipboard';
const DS = new DataSource();

export default class CardCreator extends Component {

    state={
        loading: false,
        params: this.props.route.params,
        success: false,
        renderSuccess: false,
        renderError: false,
        copiedMessage: false
    }

    UNSAFE_componentWillMount() {

    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=> true);
        this.setState({loading: true}, ()=>{
            this.createCard();
        });

    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    async createCard(){
        console.log("CREANDO CARTA");
        let params = {...this.state.params};
        console.log(params);

        let newReference = DS.database.ref("/codigos").push();
        params.code = newReference.key;
        params.fechaCreacion = this.getDate();
        if(params.type === "cfotos"){
            let createStorageCarta = await DS.createStorageCarta(params.multimedia, DS.getCurrentUser().uid, newReference.key);
            if(createStorageCarta){
                let filesUri = await DS.getFilesUri(newReference.key);
                console.log("FILES URI: " , filesUri)
                params.multimedia = filesUri;
                this.creator(newReference, params);
            }
        }
        else if(params.type === "ubications"){
            this.ubicationCreator(newReference, params)
        }
        else {
            this.creator(newReference,params);
        }
        console.log("reference: ", params.code);
        this.setState({params: params});


    }

    getDate(){
        const date = new Date();

        const currentDate =
            date.getDate().toString().padStart(2, "0") + "/" +
            (date.getMonth()+1).toString().padStart(2, "0") + "/" +
            date.getFullYear() + " " +
            date.getHours().toString().padStart(2, "0") + ":" +
            date.getMinutes().toString().padStart(2, "0") + ":" +
            date.getSeconds().toString().padStart(2, "0");

        return currentDate;
    }

    async ubicationCreator(newReference, params){
        let ubic = params.ubications;

        for(let i = 0; i < ubic.length; i++){
            if(ubic[i].type === "cfotos"){
                let createStorageCarta = await DS.createStorageCarta(ubic[i].multimedia, DS.getCurrentUser().uid, newReference.key);
                if(createStorageCarta){
                    let filesUri = await DS.getFilesUri(newReference.key);
                    console.log("FILES URI: " , filesUri)
                    ubic[i].multimedia = filesUri;
                    params.ubications = ubic;
                }
            }
        }

        this.creator(newReference, params);
    }


    creator(newReference, params){
        newReference
            .set(params)
            .then(async ()=>{
                let user = DS.getCurrentAuth().currentUser;
                let userRef = await DS.getLocalStorageData(user.email);
                let currentUserData = await DS.getUserData(userRef);

                let cards = [];

                console.log("DATOS USUARIOS: ", currentUserData);

                if(currentUserData["cartas"]){
                    cards = currentUserData["cartas"];
                    cards.push(params.code);
                }else{
                    cards.push(params.code);
                }
                console.log("SUBIENDO DATOS: ");
                let updateDataUser = await DS.updateCartasUser(cards, "users/" + userRef);
                if(updateDataUser){
                    this.setState({loading: false, success: true}, ()=>{
                        setTimeout(()=>{
                            this.setState({success: false, renderSuccess: true});
                        }, 2000)
                    });
                }else{
                    console.log("ERROR UPDATING USER CARTA");
                }
            })
            .catch(err=>{
                this.setState({loading: false}, ()=>{
                    setTimeout(()=>{
                        this.setState({success: false, renderError: true});
                    }, 1000)
                });
            })
    }

    renderMessageCopied(){
        if(this.state.copiedMessage){
            setTimeout(()=>{
                this.setState({copiedMessage: false})
            }, 6000);
            return(
                <Subtitle>¡Código copiado!</Subtitle>
            )
        }
    }

    renderError(){
        if(this.state.renderError){
            return(
              <View
                  style={{
                  position: "absolute",
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  zIndex: 12,
              }}
              >
                  <Text>Hubo un error</Text>
              </View>
            );
        }
    }

    renderSuccess(){
        if(this.state.renderSuccess){
            return(
              <View
                  style={{
                      position: "absolute",
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      zIndex: 12,
                      backgroundColor: "#7d5fff",
                      justifyContent: "center",
                      alignItems: "center"
                  }}
              >
                  <Text
                      style={{
                          color: "#FFFFFF",
                          fontSize: 24,
                          fontWeight: "bold",
                          textAlign: "center",
                          marginBottom: 35
                      }}
                  >
                      Éste será el código que deberás pasar.
                  </Text>
                  <View style={{width: "95%", backgroundColor: "#a592f7", flexDirection: "row"}}>

                      <View style={{width: "83%", backgroundColor: "#FFFFFF", justifyContent: "center", alignItems: "center"}}>
                          <Text style={{textAlign: "center"}}>
                              {this.state.params.code}
                          </Text>
                      </View>
                      <Button
                          transparent
                          onPress={()=>{
                              Clipboard.setString(this.state.params.code);
                              this.setState({copiedMessage: true});
                          }}
                          style={{width: "18%"}}
                          contentStyle={{width: "100%", justifyContent: "center", alignItems: "center"}}
                      >
                          <Icon type="MaterialCommunityIcons" name='clipboard-outline' />
                      </Button>
                  </View>
                  {this.renderMessageCopied()}
                  <Button
                      style={{backgroundColor: "#cd84f1", borderRadius: 10, padding:10, width: "80%", height: 60, marginTop: 40, justifyContent: "center", alignItems: "center"}}
                      onPress={()=> this.props.navigation.navigate("MainScreen")}
                  >
                      <Text style={{textAlign: "center", fontWeight: "bold", color: "#FFFFFF"}}>Terminar</Text>
                  </Button>
              </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Loading isActive={this.state.loading}/>
                <Success isActive={this.state.success} />
                {this.renderError()}
                {this.renderSuccess()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    }
});
