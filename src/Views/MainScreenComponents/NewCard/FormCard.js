import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    Animated,
    BackHandler
} from 'react-native';

import {Textarea, Button} from 'native-base';

export default class FormCard extends Component {

    state={
        error: false,
        carta: "",
        params: this.props.route.params
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=> {
            this.props.navigation.goBack();
            return true;
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    createCard(){
        if(this.state.carta !== "" && this.state.carta.length > 50){
            if(this.state.params){
                let params = {...this.state.params};
                if(params.ubications === undefined) params.ubications = [];
                params.ubications.push({
                    type: "cnormal",
                    carta: this.state.carta,
                    status: "not complete"
                })
                this.props.navigation.push("Map", params);
            }else{
                this.props.navigation.navigate("CardCreator", {
                    type: "cnormal",
                    carta: this.state.carta,
                    estado: "preparado"
                });
            }
        }else {
            this.setState({error: true})
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize: 25, textAlign: "center", fontWeight: "bold"}}>Tómate tu tiempo</Text>
                <Textarea
                    autoFocus={false}
                    placeholder="Empieza por aquí..."
                    style={styles.card}
                    onChangeText={carta=> this.setState({carta})}
                />
                {
                    this.state.error ?
                        <Text style={{color: "red", width: "95%"}}>No puede estar vacío y con un mínimo de 200 carácteres :(</Text>
                        :
                        null
                }
                <Button
                    style={{borderRadius: 8, backgroundColor: "#7d5fff", padding: 10, width: "80%", marginTop: 30, height: 60, alignItems: "center", justifyContent: "center"}}
                    onPress={()=> this.createCard()}
                >
                    <Text style={{textAlign: "center", color: "#FFFFFF", fontSize: 23}}>Crear</Text>
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    card:{
        borderWidth: 1,
        borderColor: "grey",
        borderRadius: 5,
        width: "95%",
        height: "50%",
        marginTop: 20
    }
});
