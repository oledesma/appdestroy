import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    BackHandler
} from 'react-native';
import {Input, Button} from 'native-base';
import CircularSlider from 'rn-circular-slider';
import Modal from 'react-native-modal';

export default class FormCode extends Component {
    state={
        codigo: "",
        visibleModal: false,
        value: 10,
        params: this.props.route.params
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=>{
            if(this.state.visibleModal){
                this.setState({visibleModal: false});
                return true;
            }else{
                this.props.navigation.goBack();
            }
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    createCard(){
        console.log(this.state.params)
        this.setState({visibleModal: false})

        if(this.state.params){
            let params = {...this.state.params};
            if(params.ubications === undefined) params.ubications = [];
            params.ubications.push({
                type: "ccode",
                status: "not complete",
                codigo: this.state.codigo,
                duration: this.state.value
            })
            this.props.navigation.navigate("Map", params);
        }else{
            this.props.navigation.navigate("CardCreator", {
                type: "ccode",
                estado: "preparado",
                codigo: this.state.codigo,
                duration: this.state.value
            })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={{textAlign: "center", fontWeight: "bold", fontSize: 25}}>Introduce el código</Text>
                <Input
                    value={this.state.codigo}
                    onChangeText={codigo=> this.setState({codigo})}
                    placeholder="Introduce el código aquí..."
                    style={{
                        maxHeight: 60,
                        width: "95%",
                        borderBottomWidth: 2,
                        borderBottomColor: "grey",
                        marginTop: 30
                    }}
                />
                <Button success style={{justifyContent: "center", alignItems: "center", padding: 10, height: 55, width: "95%"}} onPress={()=> this.setState({visibleModal: true})}>
                    <Text style={{color: "#FFFFFF", textAlign: "center"}}>Crear</Text>
                </Button>
                <Modal isVisible={this.state.visibleModal}>
                    <View style={{width: "100%", height: "70%", justifyContent: "center", alignItems: "center", backgroundColor: "white", padding: 20}}>
                        <Text
                            style={{
                                textAlign: "center",
                                fontSize: 25,
                                fontWeight: "bold"
                            }}
                        >
                            Selecciona el tiempo
                        </Text>
                        <Text
                            style={{
                                textAlign: "center"
                            }}
                        >
                            Asigna un tiempo límite para que pueda copiar el código que acabas de introducir.
                        </Text>
                        <CircularSlider
                            step={2}
                            min={10}
                            max={260}
                            value={this.state.value}
                            onChange={value => this.setState({ value })}
                            contentContainerStyle={styles.contentContainerStyle}
                            strokeWidth={10}
                            buttonBorderColor="#3FE3EB"
                            buttonFillColor="#fff"
                            buttonStrokeWidth={10}
                            openingRadian={Math.PI / 4}
                            buttonRadius={10}
                            linearGradient={[{ stop: '0%', color: '#3FE3EB' }, { stop: '100%', color: '#7E84ED' }]}
                        >
                            <Text style={styles.value}>{this.state.value}</Text>
                        </CircularSlider>
                        <View style={{width: "100%", flexDirection: "row", justifyContent: "space-evenly", paddingBottom: 10}}>
                            <Button style={{padding: 10, height: 50, minWidth: "40%", justifyContent: "center", alignItems:"center"}} danger onPress={()=> this.setState({visibleModal: false})}>
                                <Text style={{textAlign: "center", color: "#FFFFFF"}} >Cancelar</Text>
                            </Button>
                            <Button style={{padding: 10, height: 50, minWidth: "40%", justifyContent: "center", alignItems:"center"}} success onPress={()=> this.createCard()}>
                                <Text style={{textAlign: "center", color: "#FFFFFF"}} >Crear</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        justifyContent: "space-evenly",
        alignItems: "center",
    },
    contentContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    value: {
        fontWeight: '500',
        fontSize: 32,
        color: '#3FE3EB'
    }
});
