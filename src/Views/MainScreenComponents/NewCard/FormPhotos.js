import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    PermissionsAndroid,
    Dimensions,
    TouchableWithoutFeedback,
    FlatList, Animated, BackHandler
} from 'react-native';
import Swiper from 'react-native-swiper';
import {RNCamera, FaceDetector} from 'react-native-camera';
import Camera from '../../CustomComponents/Camera';
import {Button, Icon} from 'native-base';
import CameraRoll from '@react-native-community/cameraroll';
import Modal from 'react-native-modal';
import CircularSlider from 'rn-circular-slider';

const numColumns = 3;

export default class FormPhotos extends Component {

    state={
        multimedia: [],
        albums: [],
        uploadMultimedia: [],
        showAlbum: false,
        visibleModal: false,
        value: 10,
        params: this.props.route.params
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=> this.props.navigation.goBack());
        this.askCameraAndStoragePermissions();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    async askCameraAndStoragePermissions(){
        let camera = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
        if(camera){
            let storage = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
            if(!storage){
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                        {
                            title: "Permisos de acceso a tu galería",
                            message:
                                "Necesitamos permisos para acceder a tu galería y puedas seleccionar tus fotos.",
                            buttonNegative: "Cancelar",
                            buttonPositive: "Aceptar"
                        }
                    );
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        this.getAlbums();
                    } else {
                        this.props.navigation.goBack();
                    }
                } catch (err) {
                    console.warn(err);
                }
            }else{
                this.getAlbums();
            }
        }else{
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: "Permisos de acceso a la cámara",
                        message:
                            "Necesitamos acceso a tu cámara para que puedas subir tus fotos.",
                        buttonNegative: "Cancelar",
                        buttonPositive: "Aceptar"
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.askCameraAndStoragePermissions();
                } else {
                    this.props.navigation.goBack();
                }
            } catch (err) {
                console.warn(err);
            }
        }
    }

    goBack(){
        if(this.state.showAlbum){
            this.setState({showAlbum: false, multimedia: [], uploadMultimedia: []})
        }else{
            this.props.navigation.goBack()
        }
    }

    createCard(){
        this.setState({visibleModal: false})
        if(this.state.params){
            let params = {...this.state.params};
            if(params.ubications === undefined) params.ubications = [];
            params.ubications.push({
                type: "cfotos",
                multimedia: this.state.uploadMultimedia,
                status: "not complete",
                duration: this.state.value,
                captura: false,
                currentPhotoPosition: 0
            })
            this.props.navigation.navigate("Map", params);
        }else{
            this.props.navigation.navigate("CardCreator", {
                type: "cfotos",
                duration: this.state.value,
                captura: false,
                estado: "preparado",
                multimedia: this.state.uploadMultimedia,
                currentPhotoPosition: 0
            })
        }
    }

    async getAlbums(){
        let albums = await CameraRoll.getAlbums();
        this.setState({albums: albums});
        console.log(albums)
    }

    setNewPhoto(res){
        console.log(res);
        let tmp = [...this.state.uploadMultimedia];
        console.log(tmp);
        tmp.push({type: "image", path: res})
        this.setState({uploadMultimedia: tmp}, ()=>{
            console.log(this.state.uploadMultimedia);
            this._swiper.scrollBy(-1);
        });
    }

    setNewVideo(res){
        console.log(res);
        let tmp = [...this.state.uploadMultimedia];
        tmp.push({type: "video", path: res})
        this.setState({uploadMultimedia: tmp}, ()=>{
            console.log(this.state.uploadMultimedia);
            this._swiper.scrollBy(-1);
        });
    }

    async showAlbum(album, count){
        let params = {
            first: count,
            assetType: 'All',
            groupTypes: 'All',
            groupName: album
        }

        let  res = await CameraRoll.getPhotos(params);
        this.setState({multimedia: res.edges, showAlbum: true});
    }

    cancelPhoto(){
        this._swiper.scrollBy(-1);
    }

    selectMultimedia(index){
        console.log("pressed select")
        let tmp = [...this.state.multimedia];
        let uplaod = [...this.state.uploadMultimedia];
        if(uplaod.length < 20){
            tmp[index].node.selected = (tmp[index].node.selected) ? false: true;

            if(tmp[index].node.selected){
                uplaod.push({type: (tmp[index].node.type.includes("image")) ? "image" : "video", path: tmp[index].node.image.uri, index: index})
            }else{
                for(let i = 0; i < uplaod.length; i++){
                    if(uplaod[i].index === index) uplaod.splice(i, 1);
                }
            }
            this.setState({multimedia: tmp, uploadMultimedia: uplaod}, ()=>{
                console.log(this.state.uploadMultimedia)
            });
        }else{
            alert("Has llegado a tu límite.")
        }
    }

    renderImages(){
        if(this.state.albums.length > 0 && !this.state.showAlbum){
            return(
                <View style={{width: "100%", height: "90%"}}>
                    <FlatList data={this.state.albums} numColumns={numColumns} renderItem={({item, index}) =>
                        <TouchableWithoutFeedback
                            onPress={()=> this.showAlbum(item.title, item.count)}
                        >

                            <View style={{
                                margin: 10,
                                maxWidth: 100
                            }}>
                                <Image style={{width: 100, height: 100}} source={require("../../../Images/carpeta.png")}/>
                                <Text style={{textAlign: "center"}}>{item.title}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    }/>
                </View>
            )
        }
        if(this.state.showAlbum && this.state.multimedia.length > 0){
            return(
                <View style={{width: "100%", height: "90%"}}>
                    <FlatList data={this.state.multimedia} numColumns={numColumns} renderItem={({item, index}) =>{
                        if(item.node.type.includes("image")){
                            return(
                                <TouchableWithoutFeedback
                                    onPress={()=> this.selectMultimedia(index)}
                                >

                                    <View style={{
                                        margin: 10,
                                        maxWidth: 100
                                    }}>
                                        <View
                                            style={{
                                                borderRadius: 360,
                                                borderWidth: 2,
                                                borderColor: "white",
                                                position: "absolute",
                                                top: 0,
                                                right: 0,
                                                zIndex: 12,
                                                width: 30,
                                                height: 30,
                                            }}
                                        >
                                            {item.node.selected ?
                                                <View style={{width: 25, height: 25, borderRadius: 360, backgroundColor: "#7d5fff"}}/>
                                                :null
                                            }
                                        </View>
                                        <Image style={{width: 100, height: 100}} source={{uri: item.node.image.uri}}/>
                                    </View>
                                </TouchableWithoutFeedback>
                            )
                        }
                        return(
                            <TouchableWithoutFeedback
                                onPress={()=> this.selectMultimedia(index)}
                            >

                                <View style={{
                                    margin: 10,
                                    maxWidth: 100,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}>
                                    <View
                                        style={{
                                            borderRadius: 360,
                                            borderWidth: 2,
                                            borderColor: "white",
                                            position: "absolute",
                                            top: 0,
                                            right: 0,
                                            zIndex: 12,
                                            width: 30,
                                            height: 30,
                                        }}
                                    >
                                        {item.node.selected ?
                                            <View style={{width: 25, height: 25, borderRadius: 360, backgroundColor: "#7d5fff"}}/>
                                            :null
                                        }
                                    </View>
                                    <Icon
                                        style={{
                                            position: "absolute",
                                            zIndex: 12,
                                            color: "#FFFFFF"
                                        }}

                                        name="play"
                                        type="MaterialCommunityIcons"
                                    />
                                    <Image style={{width: 100, height: 100}} source={{uri: item.node.image.uri}}/>
                                </View>
                            </TouchableWithoutFeedback>
                        );
                    }
                    }/>
                </View>
            )
        }
        else{
            return (
                <View style={{height: "90%", justifyContent: "center", alignItems: "center", width: "100%"}}>
                    <Text style={{textAlign: "center"}}>Vacío</Text>
                </View>
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Swiper
                    ref={(swiper) => this._swiper = swiper}
                    index={0}
                    loop={false}
                    horizontal={false}
                    scrollEnabled={false}
                >
                    <View style={{width: "100%", height: "100%"}}>
                        <View style={{width: "100%",height: "10%", backgroundColor: "#7d5fff", flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                            <Button
                                transparent
                                style={{

                                }}
                                onPress={()=> this.goBack()}
                            >
                                <Icon name="arrow-left"  type="MaterialCommunityIcons" style={{color: "#FFFFFF"}}/>
                            </Button>
                            {
                                this.state.uploadMultimedia.length > 0 ?
                                    <Button style={{justifyContent: "center", alignItems: "center", marginRight: 10}} transparent onPress={()=> this.setState({visibleModal: true})}>
                                        <Text style={{color: "#FFFFFF"}}>Crear</Text>
                                    </Button>
                                    :null
                            }
                        </View>

                        {this.renderImages()}

                        <View
                            style={{
                                position: "absolute",
                                bottom: 10,
                                right:10,
                                padding: 0,
                                backgroundColor: "#7d5fff",
                                borderRadius: 360,
                                width: 80,
                                height: 80,
                                justifyContent: "center",
                                alignItems: "center"
                            }}
                        >
                            <Button transparent onPress={()=> this._swiper.scrollBy(1)}>
                                <Icon name="camera"  type="MaterialCommunityIcons" style={{color: "#FFFFFF"}} />
                            </Button>
                        </View>
                        <Modal isVisible={this.state.visibleModal}>
                            <View style={{width: "100%", height: "70%", justifyContent: "center", alignItems: "center", backgroundColor: "white", padding: 20}}>
                                <Text
                                    style={{
                                        textAlign: "center",
                                        fontSize: 25,
                                        fontWeight: "bold"
                                    }}
                                >
                                    Selecciona el tiempo
                                </Text>
                                <Text
                                    style={{
                                        textAlign: "center"
                                    }}
                                >
                                    Este tiempo solo se utilizará para las fotos, por lo que si hay un vídeo, una vez acabado se pasará al siguiente en la carta.
                                </Text>
                                <CircularSlider
                                    step={2}
                                    min={10}
                                    max={260}
                                    value={this.state.value}
                                    onChange={value => this.setState({ value })}
                                    contentContainerStyle={styles.contentContainerStyle}
                                    strokeWidth={10}
                                    buttonBorderColor="#3FE3EB"
                                    buttonFillColor="#fff"
                                    buttonStrokeWidth={10}
                                    openingRadian={Math.PI / 4}
                                    buttonRadius={10}
                                    linearGradient={[{ stop: '0%', color: '#3FE3EB' }, { stop: '100%', color: '#7E84ED' }]}
                                >
                                    <Text style={styles.value}>{this.state.value}</Text>
                                </CircularSlider>
                                <View style={{width: "100%", flexDirection: "row", justifyContent: "space-evenly", paddingBottom: 10}}>
                                    <Button style={{padding: 10, height: 50, minWidth: "40%", justifyContent: "center", alignItems:"center"}} danger onPress={()=> this.setState({visibleModal: false})}>
                                        <Text style={{textAlign: "center", color: "#FFFFFF"}} >Cancelar</Text>
                                    </Button>
                                    <Button style={{padding: 10, height: 50, minWidth: "40%", justifyContent: "center", alignItems:"center"}} success onPress={()=> this.createCard()}>
                                        <Text style={{textAlign: "center", color: "#FFFFFF"}} >Crear</Text>
                                    </Button>
                                </View>
                            </View>
                        </Modal>
                    </View>
                    <Camera enableVideo={true} cancel={()=>this.cancelPhoto()} newPhoto={res=> this.setNewPhoto(res)} newVideo={res => this.setNewVideo(res)}/>
                </Swiper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%"
    },
    itemInvisible:{
        backgroundColor: 'transparent'
    },
    item:{
        backgroundColor: "black",
        alignItems:"center",
        justifyContent: "center",
        margin: 1,
        height: Dimensions.get("window").width / numColumns
    },
    contentContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    value: {
        fontWeight: '500',
        fontSize: 32,
        color: '#3FE3EB'
    }
});
