import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    BackHandler, PermissionsAndroid, FlatList, TouchableWithoutFeedback, Alert
} from 'react-native';
import {Button, Icon, Picker} from 'native-base';
import Modal from 'react-native-modal';

export default class FormUbications extends Component {

    state={
        isVisible: false,
        selected: undefined,
        ubications: this.props.route.params
    }

    componentWillUnmount() {
        this.askLocaionPermissions();
    }

    componentDidMount() {
        console.log(this.state.ubications);
        if(this.state.ubications !== undefined){
            this.setState({isVisible: false, ubications: this.state.ubications.ubications})
        }else this.setState({isVisible: true, ubications: []})
        BackHandler.addEventListener("hardwareBackPress", ()=> {
            this.setState({isVisible: false});
            this.props.navigation.navigate("SelectCardType");
            return true;
        })
    }

    async askLocaionPermissions(){
        let location = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if(location){

        }else{
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                    {
                        title: "Permisos de acceso a tu ubicación",
                        message:
                            "Necesitamos permisos para acceder a tu ubicación para que puedas crear tu carta.",
                        buttonNegative: "Cancelar",
                        buttonPositive: "Aceptar"
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.askLocaionPermissions();
                } else {
                    this.props.navigation.goBack();
                }
            } catch (err) {
                console.warn(err);
            }
        }
    }

    displayAlert(item, index){
        Alert.alert(
            "¿Desea borrarlo la pista?",
            "Una vez borrado, tendrá que volver a crearlo.",
            [
                {
                    text: "Cancelar",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "Aceptar", onPress: () => this.deletePista(item, index) }
            ],
            { cancelable: false }
        );
    }

    onCreate(){
        /*

        * */

        this.props.navigation.push("CardCreator",{
            type: "ubications",
            ubications: this.state.ubications,
            estado: "preparado"
        })
    }

    deletePista(item, index){
        let tmp = [...this.state.ubications];
        tmp.splice(index, 1);

        this.setState({ubications: tmp});
    }

    onPressOption(){
        if(this.state.selected !== "key0"){
            switch (this.state.selected) {
                case "key1":
                    this.setState({isVisible: false})
                    this.props.navigation.push("FormCard", {
                        fromUbication: true,
                        ubications: this.state.ubications
                    })
                    break;
                case "key2":
                    this.setState({isVisible: false})
                    this.props.navigation.push("FormPhotos", {
                        fromUbication: true,
                        ubications: this.state.ubications
                    })
                    break;
                case "key3":
                    this.setState({isVisible: false})
                    this.props.navigation.push("FormCode", {
                        fromUbication: true,
                        ubications: this.state.ubications
                    })
                    break;
            }
        }
    }

    renderButton(){
        if(this.state.ubications !== undefined){
            if(this.state.ubications.length > 0){
                return(
                    <Button style={{justifyContent: "center", alignItems: "center", marginRight: 10}} transparent onPress={()=> this.onCreate()}>
                        <Text style={{color: "#FFFFFF"}}>Crear</Text>
                    </Button>
                )
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{width: "100%",height: "10%", backgroundColor: "#7d5fff", flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                    <Button
                        transparent
                        style={{

                        }}
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Icon name="arrow-left"  type="MaterialCommunityIcons" style={{color: "#FFFFFF"}}/>
                    </Button>
                    {this.renderButton()}
                </View>
                <View
                    style={{
                        width: "100%",
                        height: "90%"
                    }}
                >
                    <FlatList data={this.state.ubications} renderItem={({item, index}) =>
                        <TouchableWithoutFeedback
                            onLongPress={()=> this.displayAlert(item, index)}
                        >

                            <View style={{
                                width: "95%",
                                padding: 10,
                                flexDirection: "row",
                                justifyContent: "space-evenly",
                                alignItems: "center",
                                borderWidth: 1,
                                alignSelf: "center",
                                marginTop: 5,
                                borderRadius: 8
                            }}>
                                <Image style={{width: 50, height: 50}} source={item.type === "cnormal" ? require("../../../Images/carta.png") : item.type === "cfotos" ? require("../../../Images/foto.png") : require("../../../Images/codigo.png")}/>
                                <View style={{width: 2, backgroundColor: "grey", height: "60%"}}/>
                                <View
                                    style={{
                                        flexDirection: "column",
                                        width: "70%"
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontWeight: "bold",
                                            fontSize: 23
                                        }}
                                    >
                                        {`Pista #${index}`}
                                    </Text>
                                    <Text
                                        style={{

                                        }}
                                    >
                                        {item.direction}
                                    </Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    }/>
                </View>
                <View
                    style={{
                        position: "absolute",
                        bottom: 10,
                        right:10,
                        padding: 0,
                        backgroundColor: "#7d5fff",
                        borderRadius: 360,
                        width: 80,
                        height: 80,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    <Button
                        transparent
                        onPress={()=> this.setState({isVisible: true})}
                    >
                        <Icon type="MaterialCommunityIcons" name="plus" style={{color: "#FFFFFF"}}/>
                    </Button>
                </View>
                <Modal
                    isVisible={this.state.isVisible}
                    dismissable={true}
                >
                    <View
                        style={{
                            backgroundColor: "#FFFFFF",
                            width: "100%",
                            height: "50%",
                            borderRadius: 8,
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <Text style={{textAlign: "center", fontSize: 23, fontWeight: "bold", marginTop: 20}}>Selecciona el tipo: </Text>
                        <Picker
                            note
                            mode="dropdown"
                            style={{ width: "95%" }}
                            selectedValue={this.state.selected}
                            placeholder="Selecciona un tipo.."
                            onValueChange={selected => this.setState({selected})}
                        >
                            <Picker.Item label="Selecciones el tipo..." value="key0" />
                            <Picker.Item label="Carta" value="key1" />
                            <Picker.Item label="Fotos" value="key2" />
                            <Picker.Item label="Código" value="key3" />
                        </Picker>

                        <View
                            style={{
                                width: "90%",
                                flexDirection: "row",
                                justifyContent: "space-between",
                                alignItems: "center"
                            }}
                        >
                            <Button
                                danger
                                style={{
                                    padding: 8,
                                    width: "40%",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginBottom: 10
                                }}
                                onPress={()=> this.setState({isVisible: false})}
                            >
                                <Text
                                    style={{
                                        textAlign:"center",
                                        color: "#FFFFFF"
                                    }}
                                >
                                    Cancelar
                                </Text>
                            </Button>
                            <Button
                                style={{
                                    backgroundColor: "#7d5fff",
                                    padding: 8,
                                    width: "40%",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginBottom: 10
                                }}
                                onPress={()=> this.onPressOption()}
                            >
                                <Text
                                    style={{
                                        textAlign:"center",
                                        color: "#FFFFFF"
                                    }}
                                >
                                    Empezar
                                </Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%"
    }
});
