import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, Dimensions, FlatList, TouchableHighlight, BackHandler
} from 'react-native';
import CUSTOM_MAP from '../../CustomComponents/CUSTOM_MAP.json';
import MapView, {Marker} from 'react-native-maps';
import {Searchbar} from 'react-native-paper';
import Geocoder from "react-native-geocoding";
import Geolaction from '@react-native-community/geolocation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import RNGooglePlaces from "react-native-google-places";

const WIDTH_DIMENSION = Dimensions.get("window").width;

export default class Map extends Component {
    state = {
        params: this.props.route.params,
        region: {
            latitude: 52.3015493,
            longitude: 4.6939769,
            latitudeDelta: 0.0021,
            longitudeDelta: 0.0028,
        },
        loadingEnabled: true,
        zoomLevel: 18,
        point: []
    }

    componentDidMount() {
        console.log(this.state.params);
        BackHandler.addEventListener("hardwareBackPress", ()=>{
            let tmp = {...this.state.params};
            tmp.ubications.splice(tmp.length - 1, 1);
            this.props.navigation.push("FormUbications", tmp);
            return true;
        })
        Geocoder.init("AIzaSyDNMtKFKhgDTWfvf5rCsU8Xfh1VC2UgJKQ", {language: "es"});
        this.getCurrentPosition();
    }

    async onCreate(marker){
        let params = {...this.state.params};
        params.ubications[params.ubications.length - 1].point = {
            lat: marker.latLng.latitude,
            lng: marker.latLng.longitude
        }
        params.ubications[params.ubications.length - 1].direction = await this.getAddressFromLatLng(marker.latLng);
        this.props.navigation.push("FormUbications", params)
    }

    addMarker(e){
        let marker = {latLng: e.nativeEvent.coordinate};
        this.onCreate(marker)
    }

    selectedDirection(item){
        this.setState({searchQuery: item.fullText, ubications: []}, async ()=>{
            let query = await this.getLatLngFromAddress(this.state.searchQuery);
            console.log("RESULT QUERY", query.lat);
            let regionTmp = {...this.state.region};
            regionTmp.longitude = query.lng;
            regionTmp.latitude = query.lat;
            this.setState({region: regionTmp});
        });
    }

    _onChangeSearch(text){
        this.setState({searchQuery: text});
        this.getAddressPredictions(text);
    }

    markers(){
    }

    getCurrentPosition(){
        this.setState({loadingEnabled: true});
        Geolaction.getCurrentPosition(
            (position) =>{
                const currentLongitude = position.coords.longitude;
                const currentLatitude = position.coords.latitude;
                const currentAccuracy = position.coords.accuracy;
                let regionTmp = {...this.state.region};
                regionTmp.latitude = currentLatitude;
                regionTmp.longitude = currentLongitude;
                this.setState({region: regionTmp})

            },
            (error)=> alert(error.message),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    }

    getAddressPredictions(userInput){
        RNGooglePlaces.getAutocompletePredictions(userInput,{
            country: "ES"
        }).then((results)=>{
            this.setState({ubications: results})
        }).catch((error) => console.error("ERROR GOOGLE PREDICTIONS: ", error))
    }

    getAddressFromLatLng(obj){

        return new Promise((resolve, reject)=>{
            Geocoder.from(obj)
                .then(json =>{
                    let response = json.results[0].formatted_address;
                    resolve(response);
                })
                .catch(error => {
                    resolve("");
                    console.error("ERROR FROM ADDRESS LATLNG: ", error);
                })
        })
    }

    getLatLngFromAddress(address){

        return new Promise((resolve, reject)=>{
            Geocoder.from(address)
                .then(json =>{
                    let response = json.results[0].geometry.location;
                    resolve(response);
                })
                .catch(error => {
                    resolve("");
                    console.error("ERROR FROM GETTING LATLNG FROM ADDRESS: ", error);
                })
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Searchbar
                    placeholder="Buscar dirección"
                    onChangeText={(text) => this._onChangeSearch(text)}
                    value={this.state.searchQuery}
                    style={styles.searchBoxContainer}
                    ref={(input) => this.searchBarRef = input}
                />
                <View style={styles.recomendation}>
                    <FlatList data={this.state.ubications} renderItem={({item, index}) =>
                        <TouchableHighlight underlayColor="#b3b3b3" activeOpacity={0.8}  onPress={()=> this.selectedDirection(item)}>
                            <View style={styles.cardRecommendation}>
                                <Icon size={25} style={styles.iconRecommendation} name="map-marker" color="grey"/>
                                <View style={styles.directionContent}>
                                    <Text>{item.primaryText}</Text>
                                    <Text>{item.secondaryText}</Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    }/>
                </View>
                <MapView
                    region={this.state.region}
                    style={styles.map} customMapStyle={CUSTOM_MAP}
                    onPress={(e)=> this.addMarker(e)}
                    loadingEnabled={this.state.loadingEnabled}
                    loadingIndicatorColor={"green"}
                    loadingBackgroundColor={"rgba(133, 133, 133, 0.7)"}
                    maxZoomLevel={this.state.zoomLevel}
                >
                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    map:{
      width: "100%",
      height: "100%"
    },
    searchBoxContainer:{
        width: WIDTH_DIMENSION-30,
        top: 10,
        position: "absolute",
        backgroundColor: 'white',
        alignItems: "center",
        flexDirection: "row",
        height: 60,
        borderRadius: 2,
        shadowColor: "#000000",
        elevation: 7,
        shadowRadius: 5,
        shadowOpacity: 1.0,
        zIndex: 12
    },
    recomendation:{
        top: 70,
        backgroundColor: "grey",
        position: "absolute",
        zIndex: 11,
        width: WIDTH_DIMENSION-30
    },
    cardRecommendation:{
        flexDirection: "row",
        backgroundColor: "#FFFFFF",
        width: "100%",
        paddingTop: 17
    },
    directionContent:{
        borderBottomWidth: 1,
        borderColor: "#d4d4d4",
        width: "100%",
        paddingBottom: 10,
    }
});
