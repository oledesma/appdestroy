import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    Animated,
    Keyboard
} from 'react-native';
import {HelperText, TextInput, Button, Title, Headline, Subheading, Appbar, IconButton} from 'react-native-paper';
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import DataSource from '../../datasource/DataSource';
const DS= new DataSource();

let RESET = null;

export default class NewCarta extends Component {
    constructor(props){
        super(props);

        this.state={
            title: "",
            errorInput: false,
            errorHelper: false,
            helperText: "Es necesario poner un título :(",
            animation: new Animated.Value(0),
            disabled: false
        };
        this._onChangeInput = this._onChangeInput.bind(this);
    }

    _onChangeInput(name, value){
        this.setState({[name]: value});
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if(RESET === null && this.props.closeOption) {
            console.log(this.props.closeOption)
            RESET = this.props.closeOption;
            this.setState({title: "", errorInput: false, loading: false, disabled: false}, ()=>{
                RESET = null;
            });
        }
    }

    async _onNext(){
        Keyboard.dismiss();
        this.setState({errorInput: false, errorHelper: false, loading: true, helperText: "Es necesario poner un título :(", disabled: true});
        if(this.state.title !== ""){
            let user = DS.getCurrentAuth().currentUser;
            console.log(user)
            let userRef = await DS.getLocalStorageData(user.email);
            console.log(userRef)
            if(userRef){
                let currentUserData = await DS.getUserData(userRef);
                if(currentUserData){
                    console.log("HAY DATOS")
                    if(currentUserData["cartas"]){
                        console.log("HAY CARTAS")
                        for(let i in currentUserData["cartas"]){
                            if(currentUserData["cartas"][i].title == this.state.title){
                                this.setState({errorInput: true, errorHelper: true, helperText: "Parece que ya existe tu carta. Prueba con otro nombre.", disabled: false});
                                this.props.onCheckingName(false);
                                break;
                            }
                        }

                        if(!this.state.errorInput){
                            this.setState({errorInput: false, errorHelper: false, helperText: "", disabled: false});
                            this.props.callBack(this.state.title);
                            this.props.onNext();
                        }
                    }else{
                        console.log("GO NEXT")
                        this.setState({errorInput: false, errorHelper: false, helperText: "", disabled: false});
                        this.props.callBack(this.state.title);
                        this.props.onNext();
                    }
                }
            }else{
                console.log("NO SE HA ENCONTRADO EL REF")
            }
        }else this.setState({errorInput: true, errorHelper: true, loading: false});
    }

    _hasErrors = () =>{
        return this.state.errorHelper;
    };

    goTo(routeName){
        this.props.navigation.navigate(routeName, {title: this.state.title});
    }

    render() {
        return (
            <>
                <View style={{flexDirection: "row", width: "100%", justifyContent: "flex-end", paddingRight: 10, backgroundColor: "#FFFFFF"}}>
                    <IconButton
                        icon="close"
                        size={25}
                        onPress={()=> this.props.onClose()}
                    />
                </View>
                <View style={styles.form}>
                    <Headline>Empieza añandiendo un título. </Headline>
                    <View style={{flexDirection: "row"}}>
                        <Subheading style={{color: "grey", marginBottom: 50}}>Esto ayudará a distinguir tus cartas </Subheading>
                        <IconMaterial name="emoticon-cool-outline" color="#58B19F" size={30}></IconMaterial>
                    </View>

                    <TextInput
                        style={{height: 60, width: "100%"}}
                        label='Título*'
                        value={this.state.title }
                        error={this.state.errorInput}
                        disabled={this.state.disabled}
                        maxLength={30}
                        returnKeyType={"go"}
                        onChangeText={(title) => this._onChangeInput("title", title)}
                        onSubmitEditing={()=> this._onNext()}
                    />
                    <HelperText
                        type="error"
                        visible={this._hasErrors()}
                    >
                        {this.state.helperText}
                    </HelperText>
                    <Button style={styles.button} disabled={this.state.disabled} loading={this.state.disabled} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onNext()}>
                        Siguiente <IconMaterial name="chevron-right" size={15}></IconMaterial>
                    </Button>
                </View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        width: "90%",
        height: 60,
    },
    contentButton: {
        width: "100%",
        height: 60
    },
    form:{
        justifyContent: "center",
        alignItems: "center",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 50,
        width: "100%",
        backgroundColor: "#FFFFFF",
    },
    overlay:{
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        zIndex: 11,
        justifyContent: "center",
        alignItems: "center",
    },
    box:{
        width: 150,
        height: 150,
        bottom: 370,
        opacity: 1,
        zIndex: 13
    }
});
