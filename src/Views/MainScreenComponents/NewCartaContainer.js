import React, {Component, createRef} from 'react';
import {View, StyleSheet, Animated, BackHandler, Dimensions} from 'react-native';
import NewCarta from './NewCarta';
import Description from './NewCartaStepsOld/Description';
import MediaFiles from './NewCartaStepsOld/MediaFiles';
import Ubication from './NewCartaStepsOld/Ubication';
import LastStep from './NewCartaStepsOld/LastStep';
import Swiper from 'react-native-swiper';
import Loading from '../../Animations/Loading';
import Success from '../../Animations/Success';
import {RightSheet} from '../CustomComponents/RightSheet';
import AddPistasMap from './NewCartaStepsOld/AddPistasMap';
import DataSource from '../../datasource/DataSource';

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const animatedY = new Animated.Value(height);
const animatedX = new Animated.Value(-width);
const actionSheetRef = createRef();
const DS = new DataSource();

export default class NewCartaContainer extends Component{

    constructor(props) {
        super(props);

        this.state={
            title: "",
            backButtonState: false,
            creatingCard: false,
            success: false,
            ubications: [],
            closeOption: false
        }

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick(e){
        if(this.state.currentIndexSwiper === 0 || this.state.currentIndexSwiper === undefined){
            this.onClose();
            return true;
        }
        else if(this.state.creatingCard || this.state.success) return true;
        else{
            this._swiper.scrollBy(-1);
            return true;
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async _onLogOut(){
        let result = await DS.logOut();
        if(result === true){
            this.goTo("LoginScreen");
        }else{
            console.log("ERROR LOG OUT.")
        }
    }

    goNext(){
        this._swiper.scrollBy(1);
    }

    goBack(){
        this._swiper.scrollBy(-1);
    }

    onClose(){
        this.props.navigation.goBack();
    }
    getCurrentDate(){
        const date = new Date();

        let dd, m, yyyy, hh, mm, ss;

        dd = date.getDate();
        m = date.getMonth();
        yyyy = date.getFullYear();
        hh = date.getHours();
        mm = date.getMinutes();
        ss = date.getSeconds();

        return `${dd.toString().padStart(2, "0")}/${m.toString().padStart(2, "0")}/${yyyy} ${hh.toString().padStart(2, "0")}:${mm.toString().padStart(2, "0")}:${ss.toString().padStart(2, "0")}`;
    }

    async createCard(){
        let cartas = [];

        try{
            let user = DS.getCurrentAuth().currentUser;
            let userRef = await DS.getLocalStorageData(user.email);
            let currentUserData = await DS.getUserData(userRef);
            let currentDate = this.getCurrentDate();
            console.log(currentDate.toString());
            let newCarta = {
                title: this.state.title,
                description: this.state.description,
                ubications: this.state.reports,
                duration: this.state.time.toString().trim(),
                createdAt: currentDate.toString().trim(),
                currentStatus: "notOpened"
            };

            if(currentUserData["cartas"]){
                cartas = currentUserData["cartas"];
                cartas.push(newCarta);
            }else {
                cartas.push(newCarta);
            }

            let updateDataUser = await DS.updateCartasUser(cartas, "users/" + userRef);
            if(updateDataUser){
                let createStorageCarta = await DS.createStorageCarta(this.state.multimedia, user.uid , this.state.title);
                if(createStorageCarta){
                    this.setState({creatingCard: false, success: true}, ()=>{
                        setTimeout(()=>{
                            this.setState({success: false}, ()=>{
                                this.onClose();
                            })
                        }, 2000);
                    })
                }else {
                    console.log("ERROR EN LA SUBIDA DEL BUCKET")
                }

            }else{
                console.log("ERROR UPDATING USER CARTA");
            }
        }catch (e) {
            console.log("ERROR GENERAL: ", e);
            console.log("ERROR GENERAL: ", e);
            this.setState({creatingCard: false});
        }
    }

    onSaveUbications(ubications){
        console.log(ubications)
        Animated.timing(animatedX,{toValue: -width, duration: 500, useNativeDriver: false}).start();
        this.setState({ubications: ubications});
    }

    onUbicationCallback(response){
        console.log("ubications reponse")
        if(response === "openMapRightSheet") Animated.timing(animatedX,{toValue: 0, duration: 500, useNativeDriver: false}).start();
        else if(response === "goNext") this._swiper.scrollBy(1);
        else if(typeof response === 'number') {
            console.log("borrando el.. ", response);
            let tmp = this.state.ubications;
            tmp.splice(response, 1);
            console.log(tmp);
            this.setState({ubications: tmp})
        }
        else this.onSaveUbications(response);
    }

    loadingState(entry){
        this.setState({creatingCard: entry});
    }

    render() {
        return(
            <View style={styles.container}>
                <Swiper ref={(ref) => this._swiper = ref} index={0} scrollEnabled={false} showsPagination={false} loop={false} onIndexChanged={(index)=> {this.setState({currentIndexSwiper: index, backButtonState: (index === 0) ? false : true})}}>
                    <View style={{width: "100%", height: "100%", marginTop: 10}}>
                        <NewCarta closeOption={this.state.closeOption} onCheckingName={entry => this.loadingState(entry)} onClose={()=> this.onClose()} onNext={()=> this.goNext()} callBack={title=> this.setState({title})}/>
                    </View>
                    <View style={{width: "100%", height: "100%", marginTop: 10}}>
                        <Description closeOption={this.state.closeOption} onNext={()=> this.goNext()} onBack={()=> this.goBack()} onClose={()=> this.onClose()} callBack={description=> this.setState({description})}/>
                    </View>
                    <View style={{width: "100%", height: "100%", marginTop: 10, justifyContent: "space-between", alignItems: "center"}}>
                        <MediaFiles closeOption={this.state.closeOption} onNext={()=> this.goNext()} onBack={()=> this.goBack()} onClose={()=> this.onClose()} callBack={multimedia=> this.setState({multimedia})}/>
                    </View>
                    <View style={{width: "100%", height: "100%", marginTop: 10, justifyContent: "space-between", alignItems: "center"}}>
                        <Ubication thereIsUbications={this.state.ubications} onNext={()=> this.goNext()} onBack={()=> this.goBack()} onClose={()=> this.onClose()} callBack={reponse => this.onUbicationCallback(reponse)}/>
                    </View>
                    <View style={{width: "100%", height: "100%", marginTop: 10, justifyContent: "space-between", alignItems: "center"}}>
                        <LastStep closeOption={this.state.closeOption} onCreate={()=> this.setState({creatingCard: true}, ()=> this.createCard())} onBack={()=> this.goBack()} onClose={()=> this.onClose()} callBack={time=> this.setState({time})}/>
                    </View>
                </Swiper>
                <Loading isActive={this.state.creatingCard} />
                <Success isActive={this.state.success} />
                <Animated.View
                    style={{
                        position: "absolute",
                        top: 0,
                        right: animatedX,
                        backgroundColor: "rgba(0, 0, 0)",
                        width: "100%", height: "100%"
                    }}
                >
                    <RightSheet>
                        <AddPistasMap callBack={(ubications)=> this.onUbicationCallback(ubications)}/>
                    </RightSheet>
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        height: "100%",
        backgroundColor: "#FFFFFF"
    },

})
