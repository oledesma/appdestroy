import React, {Component} from 'react';
import {View, StyleSheet, FlatList, TouchableHighlight, Text, Dimensions, Alert, Image} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import CUSTOM_MAP from '../../CustomComponents/CUSTOM_MAP.json'
import {Button, HelperText, IconButton, Searchbar, TextInput, Title} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Geolaction from '@react-native-community/geolocation';
import Modal from 'react-native-modal';
import Geocoder from "react-native-geocoding";
import AnimationDisplay from '../../../Animations/AnimationDisplay';
import Loading from '../../../Animations/Loading';
import RNGooglePlaces from "react-native-google-places";

const WIDTH_DIMENSION = Dimensions.get("window").width;
const HEIGHT_DIMENSION = Dimensions.get("window").height;

export default class AddPistasMap extends Component {
    constructor(props) {
        super(props);
        this.state={
            region: {
                latitude: 52.3015493,
                longitude: 4.6939769,
                latitudeDelta: 0.0021,
                longitudeDelta: 0.0028,
            },
            loadingEnabled: false,
            zoomLevel: 18,
            points: [],
            showModal: false,
            pistaTitle: "",
            errorPistaTitle: false,
            descriptionPista: "",
            loadingState: false
        }
    }

    componentDidMount() {
        Geocoder.init("AIzaSyDNMtKFKhgDTWfvf5rCsU8Xfh1VC2UgJKQ", {language: "es"});
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        console.log("ADD PISTA...");
        console.log(prevState);
        console.log(prevProps);
    }

    async addMarker(e){
        let marker = {latLng: e.nativeEvent.coordinate};
        this.showModal(marker);
    }

    showModal(marker){
        this.setState({showModal: !this.state.showModal}, ()=>{
            this.setState({currentMarker: (this.state.showModal) ? marker : undefined });
        });
    }

    onAccept(){
        this.setState({loadingState: true}, async ()=>{
            if(this.state.pistaTitle !== ""){
                console.log(this.state.currentMarker);
                let getDirection = await this.getAddressFromLatLng(this.state.currentMarker.latLng);
                this.setState({
                    points: this.state.points.concat({title: this.state.pistaTitle, description: this.state.descriptionPista, marker: this.state.currentMarker, direction: getDirection}),
                    errorPistaTitle: false,
                    pistaTitle: "",
                    descriptionPista: "",
                    loadingState: false}, ()=>this.showModal());
            } else this.setState({errorPistaTitle: true, loadingState: false});
        })
    }

    onSave(){
        console.log("on save ubication");
        this.props.callBack(this.state.points)
    }

    getAddressFromLatLng(obj){

        return new Promise((resolve, reject)=>{
            Geocoder.from(obj)
                .then(json =>{
                    let response = json.results[0].formatted_address;
                    resolve(response);
                })
                .catch(error => {
                    resolve("");
                    console.error("ERROR FROM ADDRESS LATLNG: ", error);
                })
        })
    }

    getLatLngFromAddress(address){

        return new Promise((resolve, reject)=>{
            Geocoder.from(address)
                .then(json =>{
                    let response = json.results[0].geometry.location;
                    resolve(response);
                })
                .catch(error => {
                    resolve("");
                    console.error("ERROR FROM GETTING LATLNG FROM ADDRESS: ", error);
                })
        })
    }

    markers(){
        return this.state.points.map((point, index)=>
            <Marker
                key={index}
                coordinate={point.marker.latLng}
                title={point.title}
                icon={require("../../../Images/tesoroMarker.png")}
                identifier={`mk${index}`}
            />
        )
    }

    getCurrentPosition(){
        this.setState({loadingEnabled: true});
        Geolaction.getCurrentPosition(
            (position) =>{
                const currentLongitude = position.coords.longitude;
                const currentLatitude = position.coords.latitude;
                const currentAccuracy = position.coords.accuracy;
                this.setPostion(currentLatitude, currentLongitude, currentAccuracy);

            },
            (error)=> alert(error.message),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    }

    setPostion(lat, lon, accuracy, querySearch){
        try{
            console.log("entrando set position")
            let angularDistance, circumference, oneDegreeOfLatitudeInMeters = "";
            if(accuracy){
                accuracy = accuracy / 2;
                angularDistance = accuracy / circumference;
                circumference = 40075;
                oneDegreeOfLatitudeInMeters = 111.32 * 1000;
            }
            const latitudeDelta = (accuracy) ? (accuracy / oneDegreeOfLatitudeInMeters) : 0.1;
            const longitudeDelta = 0.09;
            console.log("error a partir de aqui")
            let tmpRegion = {...this.state.region};
            tmpRegion.longitude = lon;
            tmpRegion.latitude = lat;
            this.setState({region: tmpRegion}, ()=> console.log("tmp region;", this.state.region));
        }catch (e) {
            console.log("ERROR; ", e)
        }
    }

    selectedDirection(item){
        this.setState({searchQuery: item.fullText, ubications: []}, async ()=>{
            let query = await this.getLatLngFromAddress(this.state.searchQuery);
            console.log("RESULT QUERY", query.lat);
            this.setPostion(query.lat, query.lng, undefined, true);
        });
    }

    _onChangeSearch(text){
        this.setState({searchQuery: text});
        this.getAddressPredictions(text);
    }
    getAddressPredictions(userInput){
        RNGooglePlaces.getAutocompletePredictions(userInput,{
            country: "ES"
        }).then((results)=>{
            this.setState({ubications: results})
        }).catch((error) => console.error("ERROR GOOGLE PREDICTIONS: ", error))/*()=> this._swiper.scrollBy(1)
        */
    }

    _hasErrorsTitle(){
        return this.state.errorPistaTitle;
    }

    onRegionChange(region){
        console.log("Changed region: ", region);
    }

    onCancel(){
        this.setState({pistaTitle: "", descriptionPista: ""})
        this.showModal();
    }

    render() {
        return (
            <View style={styles.container}>
                <Searchbar
                    placeholder="Buscar dirección"
                    onChangeText={(text) => this._onChangeSearch(text)}
                    value={this.state.searchQuery}
                    style={styles.searchBoxContainer}
                    ref={(input) => this.searchBarRef = input}
                />
                <View style={styles.recomendation}>
                    <FlatList data={this.state.ubications} renderItem={({item, index}) =>
                        <TouchableHighlight underlayColor="#b3b3b3" activeOpacity={0.8}  onPress={()=> this.selectedDirection(item)}>
                            <View style={styles.cardRecommendation}>
                                <Icon size={25} style={styles.iconRecommendation} name="map-marker" color="grey"/>
                                <View style={styles.directionContent}>
                                    <Text>{item.primaryText}</Text>
                                    <Text>{item.secondaryText}</Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    }/>
                </View>
                <MapView
                    region={this.state.region}
                    onRegionChange={this.onRegionChange}
                    style={styles.map} customMapStyle={CUSTOM_MAP}
                    onPress={(e)=> this.addMarker(e)}
                    loadingEnabled={this.state.loadingEnabled}
                    loadingIndicatorColor={"green"}
                    loadingBackgroundColor={"rgba(133, 133, 133, 0.7)"}
                    maxZoomLevel={this.state.zoomLevel}
                >
                    {this.markers()}
                </MapView>
                <Modal
                    isVisible={this.state.showModal}
                    avoiKeyboard={true}
                    backdropOpacity={0.4}
                    deviceWidth={WIDTH_DIMENSION}
                    style={{width: "100%", justifyContent: "center"}}
                >
                    <View style={styles.centeredView}>
                        <Text style={{fontWeight: "bold", fontSize: 20}}>¡Estás por crear una pista!</Text>
                        <Image source={require("../../../Images/rastro.png")} style={{width: 100, height: 100, marginTop: 5, marginBottom: 5}}/>
                        <TextInput
                            style={{height: 60, width: "100%"}}
                            label='Pista*'
                            value={this.state.pistaTitle }
                            error={this.state.errorPistaTitle}
                            maxLength={14}
                            returnKeyType={"next"}
                            onChangeText={pistaTitle => this.setState({pistaTitle, errorPistaTitle: false})}
                            onSubmitEditing={()=> this.descriptionRef.focus()}
                        />
                        <HelperText
                            type="error"
                            visible={this._hasErrorsTitle()}
                        >
                            Es obligatorio poner el nombre de la pista :(
                        </HelperText>
                        <TextInput
                            style={{minHeight: 130, width: "100%"}}
                            ref={(ref) => this.descriptionRef = ref}
                            label='Descripción'
                            value={this.state.descriptionPista }
                            multiline={true}
                            returnKeyType={"go"}
                            onSubmitEditing={()=> this.onAccept()}
                            maxLength={5000}
                            onChangeText={descriptionPista => this.setState({descriptionPista})}
                        />
                        <View style={{flexDirection: "row", justifyContent: "space-between", marginTop: 10, width: "100%"}}>
                            <Button style={styles.buttonConfirm} contentStyle={styles.contentButton} mode="contained" labelStyle={{color: "white"}} color={"#ff7070"} onPress={()=> this.onCancel()}>
                                Cancelar
                            </Button>
                            <Button style={styles.buttonConfirm} contentStyle={styles.contentButton} mode="contained" labelStyle={{color: "white"}} color={"#58B19F"} onPress={()=> this.onAccept()}>
                                Crear
                            </Button>
                        </View>
                    </View>
                    <Loading isActive={this.state.loadingState}/>
                </Modal>
                <IconButton
                    icon="check"
                    size={40}
                    color="#FFFFFF"
                    style={{backgroundColor: "#58B19F", position: "absolute", bottom: 30, right: 30}}
                    onPress={()=> this.onSave()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    map:{
        width: "100%",
        height: "100%",
        position: "absolute"
    },
    container:{
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%"
    },
    searchBoxContainer:{
        width: WIDTH_DIMENSION-30,
        top: 10,
        position: "absolute",
        backgroundColor: 'white',
        alignItems: "center",
        flexDirection: "row",
        height: 60,
        borderRadius: 2,
        shadowColor: "#000000",
        elevation: 7,
        shadowRadius: 5,
        shadowOpacity: 1.0,
        zIndex: 12
    },
    recomendation:{
        top: 70,
        backgroundColor: "grey",
        position: "absolute",
        zIndex: 11,
        width: WIDTH_DIMENSION-30
    },
    cardRecommendation:{
        flexDirection: "row",
        backgroundColor: "#FFFFFF",
        width: "100%",
        paddingTop: 17
    },
    iconRecommendation:{
        paddingRight: 10,
        alignSelf: "center"
    },
    directionContent:{
        borderBottomWidth: 1,
        borderColor: "#d4d4d4",
        width: "100%",
        paddingBottom: 10,
    },
    centeredView:{
        justifyContent: "center",
        alignItems:"center",
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        padding: 35,
        marginRight: 30
    },
    modalText: {
        textAlign: "center"
    },
    buttonConfirm:{
        width: "47%",
        height: 60,
    },
    contentButton: {
        width: "100%",
        height: 60
    },
})
