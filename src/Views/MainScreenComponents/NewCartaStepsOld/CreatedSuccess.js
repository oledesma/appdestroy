import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
} from 'react-native';
import auth from '@react-native-firebase/auth';

export default class CreatedSuccess extends Component {
    goTo(routeName) {
        this.props.navigation.navigate(routeName);
    }

    componentDidMount() {
        setTimeout(() => {
            this.goTo('MainScreen');
        }, 4000);

    }

    render() {
        return (
            <View>
                <Text>Success creation</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({});
