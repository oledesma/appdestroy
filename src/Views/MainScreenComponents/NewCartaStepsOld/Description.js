import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
} from 'react-native';
import {Appbar, Button, Headline, HelperText, IconButton, Subheading, TextInput} from 'react-native-paper';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

let RESET = null;

export default class Description extends Component {
    constructor(props) {
        super(props);
        this.state = {
            description: "",
            textButton: "Saltar",
            disabled: false
        }
    }

    componentDidMount(){
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if(RESET === null && this.props.closeOption) {
            console.log(this.props.closeOption)
            RESET = this.props.closeOption;
            this.setState({description: ""}, ()=>{
                RESET = null;
            });
        }
    }

    validateInput(){
        if(this.state.description !== "") this.setState({textButton: "siguiente"});
        else this.setState({textButton: "saltar"})
    }

    _onChangeInput(name, value){
        this.setState({[name]: value}, this.validateInput);
    }

    _onNext(){
        this.setState({disabled: true}, ()=>{
            this.props.onNext();
            this.props.callBack(this.state.description);
            this.setState({disabled: false});
        })

    }

    goTo(routeName){
        this.props.navigation.navigate(routeName, {title: this.props.route.params.title, description: this.state.description});
    }

    _onBackButton(){
        this.props.navigation.goBack();
    }

    render() {
        return (
            <>
                <View style={{flexDirection: "row", width: "100%", justifyContent: "space-between", paddingRight: 10, alignItems: "center", backgroundColor: "#FFFFFF"}}>
                    <IconButton
                        icon="arrow-left"
                        size={25}
                        onPress={()=> this.props.onBack()}
                    />
                    <IconButton
                        icon="close"
                        size={25}
                        onPress={()=> this.props.onClose()}
                    />
                </View>
                <View style={styles.form}>
                    <Headline>Ahora un mensaje</Headline>
                    <View style={{flexDirection: "row"}}>
                        <Subheading style={{color: "grey", marginBottom: 50}}>Esto es opcional </Subheading>
                        <IconMaterial name="emoticon-wink-outline" color="#58B19F" size={30}></IconMaterial>
                    </View>

                    <TextInput
                        style={{minHeight: 130, width: "100%"}}
                        label='Descripción'
                        value={this.state.title }
                        multiline={true}
                        disabled={this.state.disabled}
                        returnKeyType={"go"}
                        maxLength={5000}
                        onChangeText={(description) => this._onChangeInput("description", description)}
                        onSubmitEditing={()=> this._onNext()}
                    />
                    <Button style={styles.button} loading={this.state.disabled} disabled={this.state.disabled} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onNext()}>
                        {this.state.textButton} <IconMaterial name="chevron-right" size={15}></IconMaterial>
                    </Button>
                </View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    content:{
        flex: 1,
    },
    button: {
        width: "90%",
        height: 60,
        marginTop: 50
    },
    contentButton: {
        width: "100%",
        height: 60
    },
    form:{
        justifyContent: "center",
        alignItems: "center",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 50,
        width: "100%",
        backgroundColor: "#FFFFFF",
    }
});
