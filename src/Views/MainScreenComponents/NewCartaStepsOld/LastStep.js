import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, TouchableWithoutFeedback, FlatList, Alert, BackHandler
} from 'react-native';
import {Button, IconButton, Title, Headline, Subheading, Paragraph, Badge, TextInput} from 'react-native-paper';
import Swiper from 'react-native-swiper';
import Video from 'react-native-video';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';
import MapView, {Marker} from 'react-native-maps';
import MAP_STYLE from '../../CustomComponents/CUSTOM_MAP';
import InView from 'react-native-component-inview';
import Slider from 'react-native-slider'
import TextInputMask from 'react-native-text-input-mask';
import DataSource from '../../../datasource/DataSource';
//import CircleSlider from 'react-native-circle-slider';
//import CircularSlider from 'react-native-circular-slider';
//import Svg, { G, Path } from 'react-native-svg';
const DS = new DataSource();
let RESET = null;

export default class LastStep extends Component {
    constructor(props){
        super(props);
        this.state= {
            titulo: "",
            descripcion: "",
            multimedia: [],
            ubicaciones: [],
            loading: false,
            currentSwiperIndex: 0,
            currentSwiperMultimediaIndex: 0,
            markers: [],
            videoRefs: [],
            duration: 5,
            time: "00:05"
        }
    }

    componentDidMount(){
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if(RESET === null && this.props.closeOption) {
            console.log(this.props.closeOption)
            RESET = this.props.closeOption;
            this.setState({time: "00:05"}, ()=>{
                RESET = null;
            });
        }
    }

    goTo(routeName){
        this.props.navigation.navigate(routeName)
    }

    componentWillUnmount(): void {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    _onCreate(){
        this.props.callBack(this.state.time);
        this.props.onCreate();
    }

    render() {
        return (
                <>
                    <View style={{flexDirection: "row", width: "100%", justifyContent: "space-between", paddingRight: 10, alignItems: "center", backgroundColor: "#FFFFFF"}}>
                        <IconButton
                            icon="arrow-left"
                            size={25}
                            onPress={()=> this.props.onBack()}
                        />
                        <IconButton
                            icon="close"
                            size={25}
                            onPress={()=> this.props.onClose()}
                        />
                    </View>
                    <View style={{flexDirection: "column"}}>
                        <Headline style={{textAlign: "center"}}>Por último, añade el tiempo de autodestrucción</Headline>
                        <Subheading style={{color: "grey", textAlign: "center"}}>Por defecto son 5 segundos, tu decides. </Subheading>
                    </View>
                    <Image source={require("../../../Images/sandGlass.png")} style={{width: 80, height: 80}}/>
                    <TextInput
                        style={{minHeight: 60, width: "94%"}}
                        label='Duración'
                        value={this.state.time }
                        returnKeyType={"next"}
                        maxLength={5000}
                        onChangeText={time => this.setState({time})}
                        render={props =>
                            <TextInputMask
                                {...props}
                                mask="[00]:[00]"
                            />
                        }
                    />
                    <Button style={styles.buttonConfirm} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onCreate()}>
                        Crear carta
                    </Button>
                </>
            );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    buttonConfirm:{
        width: "95%",
        height: 60,
        alignSelf: "center",
        marginBottom: 45,

    },
    button:{
        width: "48%",
        height: 60,
    },
    contentButton: {
        width: "100%",
        height: 60,
        alignItems: "center"
    },
    form:{
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        flexDirection:"column"
    }
});
