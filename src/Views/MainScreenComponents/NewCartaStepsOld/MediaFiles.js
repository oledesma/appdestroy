import React, {Component, createRef} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, FlatList, TouchableWithoutFeedback,
    Animated, Dimensions,
    Vibration,
    Alert, PermissionsAndroid,
} from 'react-native';
import {Button, Avatar, Title, IconButton} from 'react-native-paper';
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/index';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';
import DataSource from '../../../datasource/DataSource';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actions-sheet';
import Video from 'react-native-video';

const DS = new DataSource();
const actionSheetRef = createRef();
const ITEM_WIDTH = Dimensions.get("window").width;
let PERMISSION_READ_EXTERNAL_STORAGE = false;
let PERMISSION_CAMERA = false;
let RESET =  null;

export default class MediaFiles extends Component {
    constructor(props){
        super(props);
        this.state={
            contentState: "initial",
            multimedia: [],
            animatePress: new Animated.Value(1),
            columns: 2,
            deleteStatus: false,
            headerStatus: "normal",
            buttonText: "Saltar",
            selectedItems: []
        }
    }
    componentDidMount() {
        console.disableYellowBox = true;
        this.checkPermissions();
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if(RESET === null && this.props.closeOption) {
            console.log(this.props.closeOption)
            RESET = this.props.closeOption;
            this.setState({multimedia: [], contentState: "initial", deleteStatus: false, headerStatus: "normal"}, ()=>{
                RESET = null;
            });
        }
    }

    async checkPermissions(){
        PERMISSION_READ_EXTERNAL_STORAGE = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
        PERMISSION_CAMERA = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
    }

    async requestReadExternalStoragePermission(){
        try{
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
                    'title': "Acceso a su almacenamiento",
                    'message': "La aplicación necesita permisos para poder subir sus fotos."
                }
            );

            if(granted === PermissionsAndroid.RESULTS.GRANTED){
                PERMISSION_READ_EXTERNAL_STORAGE =true;
            }else  PERMISSION_READ_EXTERNAL_STORAGE = false;
        }catch (e) {

        }
    }

    async requestCameraPermission(){
        try{
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA, {
                    'title': "Acceso a su almacenamiento",
                    'message': "La aplicación necesita permisos para poder subir sus fotos."
                }
            );

            if(granted === PermissionsAndroid.RESULTS.GRANTED){
                PERMISSION_CAMERA =true;
            }else  PERMISSION_CAMERA = false;
        }catch (e) {

        }
    }

    cleanImagesTmp(){
        ImagePicker.clean().then(() => {
            console.log('removed all tmp images from tmp directory');
        }).catch(e => {
            console.error("ERROR CLEAN UP TMP IMAGES: ", e)
        });
    }

    async pushFiles(file, type, name){
        let res = await DS.pushFile(file, type, name);
    }

    onPressImage(item, index){
        if(this.state.deleteStatus){
            let items = this.state.multimedia;
            let selected = this.state.selectedItems;
            item.borderWidth = (item.borderWidth === 6) ? 0 : 6;
            item.borderColor = (item.borderColor === "#58B19F") ? "" : "#58B19F";
            item.selected = (item.selected == true) ? false : true;
            items[index] = item;
            this.setState({deleteStatus: true, multimedia: items, selectedItems: selected});
            let seleccionados = false;

            items.map((item, index)=>{
                if(item.selected == true) seleccionados = true;
            })

            if(!seleccionados) this.cancelSelection();
        }
    }

    selectItem(item, index){
        let items = this.state.multimedia;
        let selected = [];
        item.borderWidth = 6;
        item.borderColor = "#58B19F";
        item.selected = true;
        selected.push(index);
        Vibration.vibrate(400);
        this.setState({deleteStatus: true, multimedia: items, headerStatus: "delete", buttonText: "cancelar", selectedItems: selected})
    }

    animateOut(item, index){
        if(!this.state.deleteStatus){
            Animated.timing(this.state.animatePress, {
                toValue: 1,
                duration: 200
            }).start();
        }
    }

    _onUpload(){
        if(PERMISSION_READ_EXTERNAL_STORAGE){
            actionSheetRef.current?.setModalVisible();
        }else{
            this.requestReadExternalStoragePermission();
        }

        //this.setState({contentState: "not"});
    }

    contentShow(command){
        let template = "";

        if(this.state.contentState === "initial") template = <Avatar.Icon icon="cloud-upload" size={100}/>;
        else template =
            <FlatList data={this.state.multimedia} numColumns={this.state.columns} renderItem={({item, index}) =>
                <TouchableWithoutFeedback
                    onPress={()=> this.onPressImage(item, index)}
                    onLongPress={()=> this.selectItem(item, index)}
                >
                    <Animated.View style={{
                        margin: 5,
                        transform: [
                            {
                                scale: this.state.animatePress
                            }
                        ]
                    }}>
                        {this.templateVideoImage(item)}
                        <View style={(item.selected)? styles.overlay : {}} />
                    </Animated.View>
                </TouchableWithoutFeedback>
            }/>
        ;

        return template;
    }

    templateVideoImage(item){
        if(item.type === "image") return (<Image style={{width: ((ITEM_WIDTH-(20*this.state.columns))/this.state.columns), height: 150}} source={{uri: item.path}}/>);
        else return (<Video source={{uri: item.path}} repeat={true} muted={true} style={{width: ((ITEM_WIDTH-(20*this.state.columns))/this.state.columns), height: 150 }}/>);
    }

    putMultimedia(path, type){
        if(type === "image" || type === "video"){
            let multimediaContent = [];

            path.map((item, index)=>{
                multimediaContent.push({
                    path: item.path,
                    type: type,
                    paused: false,
                    selected: false,
                    muted: false,
                    volume: 1,
                    rate: 1,
                    repeat: false,
                    resizeMode: "cover",
                    duration: 0.0,
                    currentTime: 0.0,
                    rateText: "1.0",
                    pausedText: "Reproducir",
                    hideControls: false,
                    index: ""
                })
            });

            this.setState({multimedia: this.state.multimedia.concat(multimediaContent), contentState: "notInitial", buttonText: "Siguiente"});
        }else{
            let typeFormatted = (type === "cameraPhoto") ? "image" : "video";
            this.setState({
                multimedia: this.state.multimedia.concat({path: path, type: typeFormatted, borderWidth: 0, borderColor: "",paused: false, selected: false}),
                contentState: "notInitial",
                buttonText: "Siguiente",
                muted: false,
                volume: 1,
                rate: 1,
                repeat: false,
                resizeMode: "cover",
                duration: 0.0,
                currentTime: 0.0,
                rateText: "1.0",
                pausedText: "Reproducir",
                hideControls: false,
                index: ""
            });
        }

        console.log(this.state.multimedia);
    }

    cancelSelection(){
        let x, multimedia = this.state.multimedia;
        for(x in multimedia){
            multimedia[x].selected = false;
        }

        this.setState({multimedia: multimedia, deleteStatus: false, headerStatus: "normal", contentState: "initial", buttonText: (multimedia.length === 0) ? "Saltar": "Siguiente"});
    }

    deleteMultimedia(){
        console.log("ENTRANDO BORRAR MULTIMEDIA");
        let items = this.state.multimedia;

        let items2  = items.filter(function (item) {
            return item.selected === false;
        });

        this.setState({multimedia: items2, selectedItems: []});
        console.log("MULTIMEDIA2: ", items2);
        this.cancelSelection();
    };

    displayAlertConfirmation(){
        Alert.alert(
            "¿Está seguro que desea borrarlos?",
            "Una vez se borre, tendrá que volver a subirlo.",
            [
                {
                    text: "Cancelar",
                    onPress: ()=> this.cancelSelection(),
                    style: "cancel"
                },
                {
                    text: "Borrar",
                    onPress: ()=> this.deleteMultimedia(),
                }
            ],
            {cancelable: true}
        );
    }

    renderHeader(){
        const renderNormal =
            <View style={{flexDirection: "row", justifyContent: "space-between", width: "100%", alignItems: "center"}}>
                <IconButton icon="arrow-left" size={25}/>
                <Button onPress={()=> this.goTo("Ubication")}>
                    {this.state.buttonText}
                </Button>
            </View>;
        const renderDelete =
            <View style={{flexDirection: "row", justifyContent: "space-between",  width: "100%", alignItems: "center"}}>
                <Button onPress={()=> this.cancelSelection()}>
                    {this.state.buttonText}
                </Button>
                <IconButton onPress={()=> this.displayAlertConfirmation() } icon="delete" color="red" size={25}/>
            </View>;
        if(this.state.headerStatus === "normal"){
            return renderNormal;
        }else return renderDelete;
    }

    _onOption(option){
        switch (option) {
            case "pickImage":
                ImagePicker.openPicker({
                    multiple: true,
                    mediaType: "photo"
                }).then( gallery=> {
                    this.putMultimedia(gallery, "image");
                });
                break;
            case "pickVideo":
                ImagePicker.openPicker({
                    mediaType: "video",
                    multiple: true
                }).then(gallery=> {
                    this.putMultimedia(gallery, "video");
                });
                break;
            case "takePhoto":
                if(PERMISSION_CAMERA){
                    ImagePicker.openCamera({
                        mediaType: "photo",
                        cropping: false
                    }).then(image => {
                        this.putMultimedia(image.path, "cameraPhoto");
                    });
                }else{
                    this.requestCameraPermission();
                }
                break;
            case "takeVideo":
                if(PERMISSION_CAMERA){
                    ImagePicker.openCamera({
                        mediaType: 'video',
                    }).then(image => {
                        this.putMultimedia(image.path, "cameraVideo");
                    });
                }else {
                    this.requestCameraPermission();
                }
                break;
        }
    }

    _onNext(){
        this.props.callBack(this.state.multimedia);
        this.props.onNext();
    }

    renderFooter(){
        if(this.state.headerStatus === "normal"){
            return(
                <View style={{flexDirection: "row", justifyContent: "space-evenly", width: "100%", paddingBottom:10}}>
                    <Button style={styles.button} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#3498db"} mode="contained" onPress={() => this._onUpload()}>
                        Subir <IconMaterial name="upload" size={15}></IconMaterial>
                    </Button>
                    <Button style={styles.button} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onNext()}>
                        Siguiente <IconMaterial name="chevron-right" size={15}></IconMaterial>
                    </Button>
                </View>
            )
        }else return(
            <View style={{flexDirection: "row", justifyContent: "space-evenly", width: "100%", paddingBottom:10}}>
                <Button style={{width: "97%", height: 60}} contentStyle={{width: "100%", height: 60}}   labelStyle={{color: "white"}} color={"#ff7070"} mode="contained" onPress={() => this.displayAlertConfirmation()}>
                    Borrar seleccionados <IconMaterial name="delete" size={15}></IconMaterial>
                </Button>
            </View>
        )
    }

    render() {
        return (
            <>
                <View style={{flexDirection: "row", width: "100%", justifyContent: "space-between", paddingRight: 10, alignItems: "center", backgroundColor: "#FFFFFF"}}>
                    <IconButton
                        icon="arrow-left"
                        size={25}
                        onPress={()=> this.props.onBack()}
                    />
                    <IconButton
                        icon="close"
                        size={25}
                        onPress={()=> this.props.onClose()}
                    />
                </View>
                {this.contentShow()}
                {this.renderFooter()}
                <ActionSheet CustomHeaderComponent={()=> <Title>Selecciona una opción</Title> } ref={actionSheetRef}>
                    <View style={styles.containerOptions}>
                        <View style={{borderBottomWidth: 0.2, marginBottom: 10}}>
                            <Title>Selecciona una opción</Title>
                        </View>
                        <Button style={styles.buttonOptions} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onOption("pickImage")}>
                            Seleccionar imagen de galería <IconMaterial name="image" size={15}></IconMaterial>
                        </Button>
                        <Button style={styles.buttonOptions} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onOption("pickVideo")}>
                            Seleccionar video de galería <IconMaterial name="video" size={15}></IconMaterial>
                        </Button>
                        <Button style={styles.buttonOptions} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onOption("takePhoto")}>
                            Tomar foto <IconMaterial name="camera" size={15}></IconMaterial>
                        </Button>
                        <Button style={styles.buttonOptions} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this._onOption("takeVideo")}>
                            Tomar video <IconMaterial name="video-plus" size={15}></IconMaterial>
                        </Button>
                    </View>
                </ActionSheet>
            </>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center"
    },
    button: {
        width: "47%",
        height: 60,
    },
    buttonOptions:{
        width: "100%",
        height: 60,
        marginBottom: 10
    },
    contentButton: {
        width: "100%",
        height: 60
    },
    options:{
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        backgroundColor: "grey",
        marginBottom: 10,
        borderRadius: 8
    },
    options1:{
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        marginTop: 10,
        backgroundColor: "grey",
        marginBottom: 10,
        borderRadius: 8
    },
    containerOptions:{
        padding: 10
    },
    textOptions:{

    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: "'rgba(108, 230, 205,0.6)'",
        borderWidth: 2,
        borderColor: "#32695e"
    }
});
