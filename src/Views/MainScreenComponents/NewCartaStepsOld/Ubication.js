import React, {Component, createRef, useRef} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, PermissionsAndroid,
    Dimensions,
    NativeModules,
    Modal,
    Animated, PanResponder, TouchableWithoutFeedback, FlatList, TouchableHighlight, Alert, BackHandler
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {
    Appbar,
    Button,
    Headline,
    IconButton,
    Searchbar,
    Subheading,
    TextInput,
    Title,
    FAB,
    Paragraph, HelperText
} from 'react-native-paper';
import {Container, Content} from 'native-base';
import Geolaction from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CUSTOM_MAP from '../../CustomComponents/CUSTOM_MAP';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import ActionSheet from 'react-native-actions-sheet';
import RNGooglePlaces from 'react-native-google-places';
import Swiper from 'react-native-swiper'
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomMapCard from '../../CustomComponents/CustomMapCard';


const MAP_STYLE = CUSTOM_MAP;
const actionSheetRef = createRef();
const WIDTH_DIMENSION = Dimensions.get("window").width;
const HEIGHT_DIMENSION = Dimensions.get("window").height;
let PERMISSION_GRANTED = false;

export default class Ubication extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ubications: []
        };
    }

    componentDidMount(){
        this.checkPermissions();
        BackHandler.addEventListener("hardwareBackPress", ()=> {

        })
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
        if(prevProps.thereIsUbications !== this.props.thereIsUbications){
            console.log("NO son iguales:");
            console.log("prevProps: ", prevProps );
            console.log("prevState: ", prevState);
            console.log("prop: ", this.props.thereIsUbications);
            this.setState({ubications: this.props.thereIsUbications}, ()=> console.log(this.state.ubications.length))

        }else{
            console.log("SON IGUALES!!")
        }
    }

    async checkPermissions(){
        if(this.state.card.type === "ubications"){
            let PERMISSION_GRANTED = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
            if(!PERMISSION_GRANTED) this.requestLocationPermission();
            else{

            }
        }
    }

    async requestLocationPermission(){
        try{
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                    'title': "Acceso a su ubicación requerida ",
                    'message': "La aplicación necesita permisos para acceder a su ubicación"
                }
            );

            if(granted === PermissionsAndroid.RESULTS.GRANTED){
                PERMISSION_GRANTED =true;
            }else  PERMISSION_GRANTED = false;
        }catch (e) {

        }
    }

    failedToRequestPermission(){
        alert("ERROR PERMISSION")
    }



    componentWillUnmount(){
        console.log("will unmount")
    }

    addPoints(){
        if(PERMISSION_GRANTED){
            console.log("permission is granted");
            this.props.callBack("openMapRightSheet");
        }else this.requestLocationPermission();
    }

    renderMenu(){
        let empty =
            <View style={styles.menu}>
                <View style={styles.form}>
                    <Headline>¡En busca del objetivo!</Headline>
                    <Subheading style={{color: "grey", textAlign: "center"}}>Añade unos puntos en el mapa que tu aventurero debe seguir. Con cada punto en que llegue, te llegará un aviso y el debe mandarte una foto para confirmarlo.</Subheading>
                    <Image style={{width: 100, height: 100}} source={require("../../../Images/aventuras.png")}/>

                    <View style={{flexDirection: "column", justifyContent: "center"}}>
                        <Button style={styles.button} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={() => this.addPoints()}>
                            Agregar puntos <IconMaterial name="map-marker-plus" size={15}></IconMaterial>
                        </Button>
                        <Button style={styles.button} contentStyle={styles.contentButton}   labelStyle={{color: "white"}} color={"#ff7070"} mode="contained" onPress={() => this.props.callBack("goNext")}>
                            No quiero pistas <IconMaterial name="emoticon-sad" size={15}></IconMaterial>
                        </Button>
                    </View>
                </View>
            </View>;

        let points =
            <View style={styles.menu}>
                <Swiper ref={(swiper) => this._swiperCards = swiper} onIndexChanged={(index)=> this.setState({currentSwiperCards: index})} horizontal={true}  loop={false} showsPagination={false} index={0} >
                    {this.state.ubications.map((item, index)=>{
                        return(
                            <View style={{alignItems: "center", justifyContent: "center"}}>
                                <CustomMapCard {...item}/>
                            </View>
                        )
                    })}
                </Swiper>

                <View style={{flexDirection: "row", width: "100%", justifyContent: "space-between", alignItems: "center", paddingBottom: 10}}>
                    <IconButton
                        icon="delete-outline"
                        size={44}
                        color="#FFFFFF"
                        style={{backgroundColor: "#ff7070"}}
                        onPress={()=> this.displayAlertConfirmation()}
                    />
                    <IconButton
                        icon="plus"
                        size={44}
                        color="#FFFFFF"
                        style={{backgroundColor: "#58B19F"}}
                        onPress={()=> this.addPoints()}
                    />

                    <IconButton
                        icon="content-save"
                        size={44}
                        color="#FFFFFF"
                        style={{backgroundColor: "#58B19F"}}
                        onPress={()=> this.props.callBack("goNext")}
                    />
                </View>
            </View>;
        if(this.state.ubications.length > 0) return points;
        else return empty;
    }

    deletePista(){
        this.props.callBack((this.state.currentSwiperCards) ? this.state.currentSwiperCards : 0);
    }

    displayAlertConfirmation(){
        Alert.alert(
            "¿Está seguro que desea borrar la pista?",
            "Una vez se borre, tendrá que volver a crearlo.",
            [
                {
                    text: "Cancelar",
                    onPress: ()=> console.log("CANCELAR PERRO"),
                    style: "cancel"
                },
                {
                    text: "Borrar",
                    onPress: ()=> this.deletePista(),
                }
            ],
            {cancelable: true}
        );
    }
    render() {
        return (
            <>
                <View style={{flexDirection: "row", width: "100%", justifyContent: "space-between", paddingRight: 10, alignItems: "center", backgroundColor: "#FFFFFF"}}>
                    <IconButton
                        icon="arrow-left"
                        size={25}
                        onPress={()=> this.props.onBack()}
                    />
                    <IconButton
                        icon="close"
                        size={25}
                        onPress={()=> this.props.onClose()}
                    />
                </View>
                {this.renderMenu()}
            </>

        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    modalContainer:{
      backgroundColor: "#FFFFFF",
      paddingTop: 12,
      borderTopRightRadius: 12,
      borderTopLeftRadius: 12
    },
    overlay:{
      backgroundColor: 'rgba(0,0,0,0.2)',
      flex: 1,
      justifyContent: "flex-end"
    },
    map:{
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: "absolute"
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
    },
    cardPointer:{
        width: "100%",
        flexDirection: "row",
        padding: 10,
        alignItems: "center",
        borderRadius: 20,
        backgroundColor: "#dedede",
        elevation: 7
    },
    cardPointerDescription:{
      flexDirection: "column",
        width: "60%",
        paddingLeft: 5
    },
    menu:{
        flex: 1,
        alignItems: "center",
        width: "100%",
        height: "100%"
    },
    radius:{
        height: 50,
        width: 50,
        borderRadius: 50/2,
        overflow: 'hidden',
        backgroundColor: 'rgba(0, 122, 255, 0.1)',
        borderWidth: 1,
        borderColor: 'rgba(0, 112, 255, 0.3)',
        alignItems: "center",
        justifyContent: "center"
    },
    marker:{
        height: 20,
        width: 20,
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: 20/2,
        overflow: "hidden",
        backgroundColor: "#007AFF"
    },
    headerContainer:{
        zIndex: 11,
        width: WIDTH_DIMENSION-30,
        top: 10,
        position: "absolute",
        backgroundColor: 'white',
        alignItems: "center",
        flexDirection: "row"
    },
    searchBoxContainer:{
        zIndex: 11,
        width: WIDTH_DIMENSION-30,
        top: 10,
        position: "absolute",
        backgroundColor: 'white',
        alignItems: "center",
        flexDirection: "row",
        height: 60,
        borderRadius: 2,
        shadowColor: "#000000",
        elevation: 7,
        shadowRadius: 5,
        shadowOpacity: 1.0
    },
    recomendation:{
      top: 70,
      backgroundColor: "grey",
        position: "absolute",
        zIndex: 11,
        width: WIDTH_DIMENSION-30
    },
    cardRecommendation:{
        flexDirection: "row",
        backgroundColor: "#FFFFFF",
        width: "100%",
        paddingTop: 17
    },
    iconRecommendation:{
      paddingRight: 10,
        alignSelf: "center"
    },
    directionContent:{
        borderBottomWidth: 1,
        borderColor: "#d4d4d4",
        width: "100%",
        paddingBottom: 10,
    },
    bottomContainer:{
        marginTop: 10,
        flexDirection: "column",
    },
    button: {
        width: "90%",
        height: 60,
        marginTop: 10
    },
    buttonConfirm:{
        width: "47%",
        height: 60,
        marginTop: 50
    },
    contentButton: {
        width: "100%",
        height: 60
    },
    form:{
        justifyContent: "center",
        alignItems: "center",
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 310,
        paddingTop: 50,
        width: "100%",
        backgroundColor: "#FFFFFF",
    },
    formConfirm:{
        justifyContent: "center",
        alignItems: "center",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 50,
        width: "100%",
        backgroundColor: "#FFFFFF",
    },
    containerConfirm:{
        width: "100%",
        height: "100%"
    }
});
