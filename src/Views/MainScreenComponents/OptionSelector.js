import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    BackHandler
} from 'react-native';
import {Button} from 'native-base';

export default class OptionSelector extends Component {

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=>{
            this.props.navigation.goBack();
            return true;
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    render() {
        return (
            <View style={styles.container}>
                <Button
                    success
                    style={[styles.button]}
                    onPress={()=> this.props.navigation.navigate("SelectCardType")}
                >
                    <Text style={styles.text}>Crear carta</Text>
                </Button>
                <Button
                    style={[styles.button, {backgroundColor: "#7d5fff"}]}
                    onPress={()=> this.props.navigation.navigate("CodeChecker")}
                >
                    <Text style={styles.text}>Código de carta</Text>
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    button:{
        justifyContent: "center",
        width: "80%",
        height: 60,
        borderRadius: 8
    },
    text: {
        textAlign: "center",
        color: "#FFFFFF"
    }
});
