import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    TouchableWithoutFeedback,
    BackHandler,
    Alert
} from 'react-native';
import {Title, Button, Icon} from 'native-base';

export default class SelectCardType extends Component {

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", ()=>{
            this.props.navigation.navigate("MainScreen");
            return true;
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    goTo(routeName){
        this.props.navigation.navigate(routeName);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{width: "97%"}}>
                    <Text style={{fontSize: 26, textAlign: "center"}}>Selecciona el tipo de carta</Text>
                    <Text style={{fontSize: 18, textAlign: "center", marginTop: 10}}>Tienes 4 tipos, asi que tomate tu tiempo ;)</Text>
                </View>
                <View style={{width: "95%", justifyContent: "space-between", alignItems: "center", flexDirection: "row"}}>
                    <TouchableWithoutFeedback
                        onPress={()=> this.goTo("FormCard")}
                    >
                        <View style={styles.buttonContainer}>
                            <Image source={require("../../Images/carta.png")} style={styles.images}/>
                            <Text style={styles.title}>Carta</Text>
                            <Text style={styles.textButton}>Escribe una carta con algo explosivo por dentro.</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={()=> this.goTo("FormPhotos")}
                    >
                        <View style={styles.buttonContainer}>
                            <Image source={require("../../Images/foto.png")} style={styles.images}/>
                            <Text style={styles.title}>Fotos</Text>
                            <Text style={styles.textButton}>Envía fotos y escoge el tiempo que sea visible.</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={{width: "95%", justifyContent: "space-between", alignItems: "center", flexDirection: "row"}}>
                    <TouchableWithoutFeedback onPress={()=> this.goTo("FormCode")}>
                        <View style={styles.buttonContainer}>
                            <Image source={require("../../Images/codigo.png")} style={styles.images}/>
                            <Text style={styles.title}>Código</Text>
                            <Text style={styles.textButton}>Ideal para concursos y entregar un código al más rápido.</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={()=> this.props.navigation.push("FormUbications")}>
                        <View style={styles.buttonContainer}>
                            <Image source={require("../../Images/ubicacion.png")} style={styles.images}/>
                            <Text style={styles.title}>Ubicaciones</Text>
                            <Text style={styles.textButton}>Una combinación de los anteriores, pero con ubicaciones.</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        width: "100%",
        height: "100%",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    buttonContainer:{
        backgroundColor: "#e6e6e6",
        padding: 10,
        borderRadius: 5,
        width: "45%",
        justifyContent: "center",
        alignItems: "center",
        elevation: 5
    },
    images:{
        width: 50,
        height: 50,
        marginBottom: 10
    },
    textButton:{
        textAlign: "center"
    },
    title:{
        fontWeight: "bold",
        fontSize: 17
    }
});
