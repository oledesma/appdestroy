import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView, KeyboardAvoidingView, BackHandler
} from 'react-native';
import {TextInput, Avatar} from 'react-native-paper';
import Button from 'react-native-paper/src/components/Button';
import DataSource from '../datasource/DataSource';
import HelperText from 'react-native-paper/src/components/HelperText';
import {Icon, Input, Item, Label} from 'native-base';

const DS = new DataSource();
const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0;
let helperText, type = "error", user, email, password, passwordConfirm = "", errores = false, regex = /^[0-9a-zA-Z]+$/;

export default class RegisterScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            user: "",
            email: "",
            password: "",
            passwordConfirm: "",
            errorUser: false,
            errorUserText: "",
            errorEmail: false,
            errorEmailText: "",
            errorPassword: false,
            errorPasswordText: "",
            errorGeneral: false,
            errorGeneralText: "",
            loadingRegisterButton: false
        };
        this._onChangeInput = this._onChangeInput.bind(this);
    }
    returnToOriginalState(){
        this.setState({
            user: "",
            email: "",
            password: "",
            passwordConfirm: "",
            errorUser: false,
            errorUserText: "",
            errorEmail: false,
            errorEmailText: "",
            errorPassword: false,
            errorPasswordText: "",
            errorGeneral: false,
            errorGeneralText: "",
            loadingRegisterButton: false
        });
    }

    goTo(routeName) {
        this.returnToOriginalState();
        this.props.navigation.navigate(routeName);
    }

    _onRegister = async ()=>{
        console.reportErrorsAsExceptions = false;
        if(!this.state.errorEmail && !this.state.errorUser && !this.state.errorPassword){
            this.setState({loadingRegisterButton: true});
            let checkIfUserExist = await this.checkIfUserExist();
            console.log("RESPONSE USER EXIST: ", checkIfUserExist);
            if(!checkIfUserExist){
                let res = await DS.register(this.state.email, this.state.password, this.state.user);
                if(res === true) {

                    this.goTo("RegistrationSuccessScreen");
                }
                else if(res === "mailalreadyinuse")
                    this.setState({loadingRegisterButton: false, errorGeneral: true, errorGeneralText: `El email ya está en uso.`, errorEmail: true, errorEmailText: false});
                else
                    this.setState({loadingRegisterButton: false, errorGeneral: true, errorGeneralText: `Hubo un error al hacer el registro.`});
            }else{
                this.setState({loadingRegisterButton: false, errorGeneral: true, errorGeneralText: `El usuario ${this.state.user} ya existe.`, errorUser: true, errorUserText: false});
            }
        }
    };

    validateInput(){
        switch (this.state.name) {
            case "user":
                if(this.state.user.match(/^[0-9a-zA-Z]+$/)) this.setState({errorUser: false, errorUserText: ""});
                else this.setState({errorUser: true, errorUserText: "Solo puede contener números y letras sin espacios."});
                break;
            case "email":
                if(this.state.email.includes("@")) this.setState({errorEmail: false, errorEmailText: ""});
                else this.setState({errorEmail: true, errorEmailText: "Email inválido."});
                break;
            case "passwordConfirm":
                if(this.state.password === this.state.passwordConfirm) this.setState({errorPassword: false, errorPasswordText: ""});
                else this.setState({errorPassword: true, errorPasswordText: "Las contraseñas deben coincidir."});
                break;
        }
    }

    _onChangeInput (name, value){
        console.log(name);
        this.setState({[name]: value, name: name}, this.validateInput);
    };

    async checkIfUserExist(){
        return await DS.checkIfUserExist(this.state.user);
    }

    render() {
        return (
            <View  style={styles.container}>
                <ScrollView>
                    <Text style={{fontWeight: "bold", fontSize: 30, marginBottom: 30}}>Registrarse</Text>

                    <Item floatingLabel style={{backgroundColor: (this.state.loading) ? "#d1d1d1" : ""}} error={this.state.error}>
                        <Icon type="AntDesign" name='user' />
                        <Label>Usuario</Label>
                        <Input
                            getRef={username => {
                                this.username = username;
                            }}
                            disabled={this.state.loading}
                            maxLength={30}
                            onChangeText={user => this.setState({user})}
                            autoCapitalize={false}
                            returnKeyType="next"
                            onSubmitEditing={()=> this.password._root.focus()}
                        />
                    </Item>
                    <Button style={styles.buttonRegister} loading={this.state.loadingRegisterButton} labelStyle={{color: "white"}} color={"#58B19F"} mode="contained" onPress={()=> this._onRegister()}>
                        Registrarse
                    </Button>
                    <View style={{flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
                        <Text style={{textAlign:"center", fontSize: 15}}>¿Ya tienes una cuenta? </Text>
                        <TouchableOpacity onPress={()=> this.goTo("LoginScreen")}><Text style={{color: "#58B19F", fontSize: 15}}>Inicia sesión.</Text></TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        padding: 10,
    },
    registerForm:{

    },
    buttonRegister:{
        height: 50,
        width: 170,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        marginBottom: 20
    },
    input:{
        height: 60,
        marginBottom: 20
    },
    inputUser:{
        height: 60
    }
});
