import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
    Animated
} from 'react-native';
import AnimatedText from 'react-native-paper/src/components/Typography/AnimatedText';

export default class RegistrationSuccessScreen extends Component {
    state = {
        animate: new Animated.Value(30),
        animateXY: new Animated.ValueXY({x: 0, y: 0}),
        animateText: new Animated.Value(0.0),
        text: "¡Registrado con éxito!"
    };

    goTo(routeName) {
        this.props.navigation.navigate(routeName);
    }

    componentDidMount() {
        Animated.timing(this.state.animate, {
            toValue: 150,
            duration: 3000
        }).start();

        setTimeout(() => {
            this.goTo('MainScreen');
        }, 6000);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize: 30, fontWeight: "bold", marginBottom: 20}}> {this.state.text} </Text>
                <Animated.Image
                    style={{width: this.state.animate, height: this.state.animate}}
                    source={require("../Images/success.png")}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    img:{
        width: 130,
        height: 130,
    }
});
