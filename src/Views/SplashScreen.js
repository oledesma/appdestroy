import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView,
} from 'react-native';
import auth from '@react-native-firebase/auth';

export default class Menu extends Component {
    goTo(routeName) {
        this.props.navigation.navigate(routeName);
    }

    componentDidMount() {
        auth().onAuthStateChanged((user)=>{
           if(user){
               console.log("USER IS ALREADY LOGGED: ", user);
               setTimeout(() => {
                   this.goTo('MainScreen');
                   //this.goTo('CodeChecker');
               }, 4000);
           }else {
               setTimeout(() => {
                   this.goTo('FirstScreen');
               }, 4000);
           }
        });

    }

    render() {
        return (
            <View>
                <Text>SplashScreen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({});
