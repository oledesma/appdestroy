import database from '@react-native-firebase/database'
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import AsyncStorage from '@react-native-community/async-storage';
import User from '../Objects/User';

export default class DataSource {
    constructor(){
        this.auth = auth();
        this.database = database();
        this.storage = storage();
        this.rootRef = this.database;
        this.user = new User("", "", "", "");
    }

    checkIfUserExist(username){
        console.log("CHECK IF USER EXIST: ",username);
        return new Promise((resolve, reject)=>{
            this.rootRef.ref("usernames/" + username)
                .once("value", (snapshot)=>{
                    console.log("CHECK IF USER EXISTE SNAPSHOT: ", snapshot);
                    if(snapshot.exists()){
                        resolve(snapshot.val());
                    }else{
                        resolve(false);
                    }
                }).catch((error) =>{
                console.log("ERROR CHECK USER EXIST: ", error)
            })
        })
    }

    getUserData(userRef){
        return new Promise((resolve, reject)=>{
            this.rootRef.ref("users/" + userRef)
                .once("value", (snapshot)=>{
                    console.log("GETTING USER DATA SNAPSHOT: ", snapshot);
                    if(snapshot.exists()){
                        resolve(snapshot.val());
                    }else{
                        resolve(false);
                    }
                }).catch((error) =>{
                console.log("ERROR CHECK USER EXIST: ", error)
            })
        })
    }

    getUserCards(codigo){
        return new Promise((resolve, reject)=>{
            this.rootRef.ref("codigos/" + codigo )
                .once("value", (snapshot)=>{
                    console.log("GETTING CARDS OF USER ", snapshot);
                    if(snapshot.exists()){
                        resolve(snapshot.val());
                    }else{
                        resolve(false);
                    }
                }).catch((error) =>{
                console.log("ERROR GETTING CARDS OF USER : ", error)
            })
        })
    }

    logOut(){
        return new Promise((resolve, reject)=>{
            this.auth.signOut()
                .then(()=> resolve(true))
                .catch((error)=> {console.log("ERROR SIGN OUT: ", error); resolve(false)})
        })
    }

    pushFile(file, type, name){
        this.storage.ref(`${type}/${this.auth.currentUser.uid}/${name}`).put(file).then(()=>{
            console.log("SUBIDA CON ÉXITO");
        }).catch(error =>{
            console.error("ERROR: ", error);
        })
    }

    getFilesUri(title){
        return new Promise(async (resolve)=>{
            const images = this.storage.ref(`Images/${this.auth.currentUser.uid}/${title}`);
            const videos = this.storage.ref(`Videos/${this.auth.currentUser.uid}/${title}`);

            const imagesList = await this.listFilesAndDirectories(images);
            const videosList = await this.listFilesAndDirectories(videos);

            let urlMultimedia = [];

            if(imagesList.length > 0){
                let imageUrl = null;
                for(let i = 0; i < imagesList.length; i++){
                    imageUrl = await this.storage.ref(imagesList[i]).getDownloadURL();
                    urlMultimedia.push({type: "image", url: imageUrl, isPlaying: false});
                }
            }

            if(videosList.length > 0){
                let videoUrl = null;
                for(let i = 0; i < videosList.length; i++){
                    videoUrl = await this.storage.ref(videosList[i]).getDownloadURL();
                    urlMultimedia.push({type: "video", url: videoUrl, paused: true, end: false});
                }
            }

            resolve(urlMultimedia);
        })
    }

    listFilesAndDirectories(reference, pageToken){
        return new Promise((resolve) => {
            reference.list({pageToken}).then(result=>{
                let res = [];
                result.items.forEach(ref => {
                    res.push(ref.fullPath);
                })

                if(result.nextPageToken){
                    return this.listFilesAndDirectories(reference, pageToken);
                }

                resolve(res);
            })
        })
    }

    findUserEmail(uid){
        console.log("EMAIL CHECK USER UID: ", uid);
        return new Promise((resolve, reject)=>{
            this.rootRef.ref("users")
                .once("value", (snapshot)=>{
                    let found = false, email = "";
                    if(snapshot.exists()){
                        if(snapshot.val()[uid] !== undefined){
                            resolve(snapshot.val()[uid].email);
                        }else resolve(false);
                    }else{
                        resolve(false);
                    }
                }).catch((error) =>{
                console.log("ERROR CHECK USER EMAIL ERROR: ", error)
            })
        })
    }

    getCurrentUser(){
        return this.user;
    }

    getCurrentAuth(){
        return this.auth;
    }

    getLocalStorageData(useremail){
        return new Promise(async (resolve, reject)=>{
            try {
                const keys = await AsyncStorage.getAllKeys();
                const result = await AsyncStorage.multiGet(keys);

                let found = false, ref = "";
                for(let i in result){
                    let object= JSON.parse(result[i][1]);
                    if(object["useremail"] == useremail) {
                        console.log(object)
                        found = true;
                        ref = object["userref"];
                        break;
                    }
                }

                console.log(ref)
                resolve(ref);
            } catch(e) {
                console.log("ERROR GETTING LOCAL STORAGE: ", e);
                resolve("error");
            }
        });
    }

    getLocalStorageUserData(){
        return new Promise(async (resolve, reject)=>{
            try {
                const keys = await AsyncStorage.getAllKeys();
                const result = await AsyncStorage.multiGet(keys);

                let found = false, ref = "";
                for(let i in result){
                    let object= JSON.parse(result[i][1]);
                    console.log("USER DATA FIRST LOAD: " , object["userUID"])
                    if(object["userUID"] == this.auth.currentUser.uid) {
                        found = true;
                        ref = object["userref"];
                        break;
                    }
                }

                resolve(ref);
            } catch(e) {
                console.log("ERROR GETTING LOCAL STORAGE: ", e);
                resolve("error");
            }
        });
    }

    setLocalStorageData(data){
        return new Promise(async (resolve, reject)=>{
            try{
                const keys = await AsyncStorage.getAllKeys();
                const result = await AsyncStorage.multiGet(keys);

                if(result.length > 0){
                    await AsyncStorage.setItem(`userKey${result.length}`, JSON.stringify(data));
                }else{
                    await AsyncStorage.setItem("userKey0", JSON.stringify(data));
                }
                //await AsyncStorage.setItem(storageKey, data);
                resolve(true);
            }catch (e) {
                resolve(false);
            }
        })
    }

    register(email, password, username){
        console.log("USERNAME REGISTER: ", username);
        return new Promise((resolve, reject)=>{
            this.auth.createUserWithEmailAndPassword(email, password)
                .then(async ()=>{
                    console.log('REGISTER SUCCESS!!');
                    console.log("LOGIN THE NEW USER...");
                    let loginResult = await this.loginEmailPassword(email, password, username);
                    if(loginResult){
                        console.log("CURRENT LOGGED USER: ", this.auth.currentUser.uid);
                        console.log("PUSHING NEW USER DATA...");
                        let _uid = this.auth.currentUser.uid;
                        let datapush = await this.pushDataUser({
                            username: username,
                            email: email
                        }, "users");

                        if(datapush) {
                            let newUserName = {};
                            newUserName[`${username}`] = datapush;
                            let setNewUser = await this.settDataUser(datapush, "usernames", username);
                            if(setNewUser) resolve(true);
                            else resolve(false);
                        }
                    }else {
                        resolve(false);
                    }
                })
                .catch((error) => {
                    console.error("ERROR REGISTRO: ", error);
                    if (error.code === "auth/email-already-in-use") {
                        resolve("mailalreadyinuse");
                    } else {
                        resolve(false);
                    }
                });
        });
    }

    settDataUser(data, ref, key){
        return new Promise((resolve, reject)=>{
            this.database.child(ref).child(key).setValue(data).then(()=>{
                console.log("DATA SET SUCCESS");
                resolve(true);
            }).catch((error)=>{
                console.error("ERROR DATA SET: ", error);
                resolve(false);
            });
        })
    }

    pushDataUser(data, ref){
        return new Promise((resolve, reject)=>{
            let reference = this.database.ref(ref).push();
            console.log("REFERENCE KEY: ", reference.key);
            reference.set(data).then(()=>{
                console.log("DATA PUSH SUCCESS");
                resolve(reference.key);
            }).catch((error)=>{
                console.error("ERROR DATA PUSH: ", error);
                resolve(false);
            });
        })
    }

    updateCartasUser(data, ref){
        return new Promise((resolve, reject)=>{
            this.database.ref(ref).update({cartas: data}).then(()=>{
                console.log("DATA UPDATE SUCCESS");
                resolve(true);
            }).catch((error)=>{
                console.error("ERROR DATA UPDATE: ", error);
                resolve(false);
            });
        })
    }

    createStorageCarta(multimedia, username, key){
        console.log(username);
        return new Promise(async (resolve, reject)=>{
            let reference = "", refs = [];

            let contador = 1;

            for(let i in multimedia){
                let item = multimedia[i];

                let filename = item.path.split("/");
                if(item.type === "video"){
                    reference = this.storage.ref("Videos/" + this.auth.currentUser.uid + "/" + key + "/" + filename[filename.length-1]);
                }else{
                    reference = this.storage.ref("Images/" + this.auth.currentUser.uid + "/" + key + "/" + filename[filename.length-1]);

                }

                let pushBucket = await this.pushStorage(reference, item.path);
                if(pushBucket) contador++;
            }

            resolve(true);
        });
    }

    pushStorage(ref, file){
        return new Promise((resolve, reject)=>{
            const task = ref.putFile(file);

            task.on('state_changed', taskSnapshot => {
                console.log(`${taskSnapshot.bytesTransferred} transferred out of ${task.totalBytes}`);
            });

            task.then(() => {
                console.log('Image uploaded to the bucket!');
                resolve(true);
            });

            task.catch(e=>{
                console.error("ERROR SUBIDA DE MULTIMEDIA: ", e);
                resolve(false);
            })
        });
    }

    loginEmailPassword(email, password, username){
        return new Promise((resolve, reject)=>{
            this.auth.signInWithEmailAndPassword(email, password)
                .then(async ()=>{
                    console.log('LOGED IN');
                    setTimeout(async ()=>{
                        let userRef = await this.checkIfUserExist(username);
                        console.log("GETTING USER REF: ", userRef);
                        let setRef = await this.setLocalStorageData( {username: username, userref: userRef, useremail: email, userUID: this.auth.currentUser.uid});
                        console.log("USER SET LOCAL STORAGE: ", setRef);
                        if(setRef) resolve(true);
                        else resolve(false);
                    }, 4000);

                })
                .catch(error => {
                    switch (error.code) {
                        case "auth/user-disabled":
                            resolve("userdisabled");
                            break;
                        case "auth/user-not-found":
                            resolve("usernotfound");
                            break;
                        case "auth/wrong-password":
                            resolve("wronpassword");
                            break;
                    }
                });
        });
    }

    checkIfCodeExist(code){
        return new Promise((resolve, reject)=>{
            this.rootRef.ref("codigos/" + code)
                .once("value", (snapshot)=>{
                    console.log("GETTING CODE CHECK RESPONSE: ", snapshot);
                    if(snapshot.exists()){
                        resolve(snapshot.val());
                    }else{
                        resolve(false);
                    }
                }).catch((error) =>{
                console.log("ERROR CODE CHECK: ", error)
            })
        })
    }
}
